@extends('layouts.app-one-col')

@section('css')
  <link href="../assets/libs/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/libs/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
  <link href="https://vjs.zencdn.net/7.8.3/video-js.css" rel="stylesheet" />

  <style type="text/css">
    #image-upload{
      display: none;
    }
    #toggleImage{
      background: #f3f7f9;
      border-color: #f3f7f9;
    }
    #toggleImage.active{
      background: #d1e0e8;
    }
    .card.bg-primary{
            background-color: #eff0f2 !important;
        }
    .card-avatar{
      width: 40px;
      position: absolute;
    margin-top: -40px;
    right:0;
    }
    /* ---- grid ---- */

    .grid {

      max-width: 1300px;
    }

    /* clearfix */
    .grid:after {
      content: '';
      display: block;
      clear: both;
    }

    /* ---- grid-item ---- */

    .grid-item--width2 { width:  46%; }
    #discover-feed div:nth-of-type(2){
      z-index: 100;
    }
    .grid-item--width3 { width:  60%; }

    .grid-item--height2 { height: 200px; }
    .grid-item--height3 { height: 260px; }
    .grid-item--height4 { height: 360px; }
    .swal2-image{
      border-radius: 50% !important;
    }
    .liked{
      pointer-events: none !important;
    }
    .icebreakanswer{
      margin-top:0;
      margin-bottom: 20px;
    }
    .swal2-popup .swal2-styled.swal2-confirm{
      background: #f75964;


    }
    .swal2-popup .swal2-styled.swal2-confirm:hover{
      background: #f52837!important;
    }
    img.swal2-avi{
      position: absolute;
      left:20px;
      top:20px;
      width: 50px;
    }
    .grid-item .delete{
            position: absolute;
            top: 5px;
            right: 6px;
            width: 40px;
            height: 40px;
            text-align: center;
            padding-top: 11px;
            border-radius: 50%;
            font-size: 20px;
            cursor: pointer;
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        }
    .grid-item img{
      cursor: pointer;
    }
    #addPostButton{
      display: none;
    }
    @media (min-width: 992px){
      #discover-maker{
        position: fixed; right: 20px;
        display: inline-block !important;
        z-index: 102;
      }
      .order-lg-first{
        min-height: 500px;
      }
      .loadingoverlay{
        margin-left:70px;
      }
      .loadingoverlay_element{
        margin-left:-70px;
      }
    }
    @media (max-width: 600px){
    .grid-item--width2{
      width: 100%;
    }
    #discover-maker{
      display: none;
    }
    #addPostButton{
      display: block;
      position: fixed;
      bottom:10px;
      right: 10px;
      z-index: 200;
      box-shadow: 0 2px 6px 0 rgba(0,0,0,.5);
    }
    .font-36{
      font-size: 36px;
    }
    #discover-maker{
      position: fixed;
      z-index: 101;
      top: 120px;
      margin-left: 0;
      padding-left: 0;
      left: 6px;
    }
    .content-popped:before{
      position: absolute;
      content: '';
      background-image: linear-gradient(to left, rgba(50, 100, 245, 0.55), rgba(74, 84, 232, 0.53), rgba(91, 66, 219, 0.50), rgba(104, 44, 203, 0.53), rgba(114, 2, 187, 0.55));
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      z-index: 101;
    }
  }
  .loadingoverlay{
    z-index: 101 !important;
  }
  </style>
@endsection

@section('content')
  <div class="row">
    <div class="col-lg-8 col-md-12 col-sm-12 order-last order-sm-last order-md-last order-lg-first order-xl-first text-center">

      <div id="discover-feed" class="grid">
        <div class="grid-sizer"></div>

      </div>
    </div> <!-- end card -->

    <div id="discover-maker" class="col-md-12 col-lg-4 col-sm-12 order-first order-sm-first order-md-first order-lg-last order-xl-last">
      <div class="card-box">
        <h4 class="header-title">Get the conversation started!</h4>
        <form method="POST" action="{{ route('add-post') }}" role="form" enctype="multipart/form-data" id="icebreaker-form">
          @CSRF
          <ul class="nav nav-tabs nav-bordered nav-justified" role="tablist">
              <li class="nav-item">
                  <a class="nav-link py-2 active" data-toggle="tab" href="#icebreaker-tab" role="tab" id="icebreaker-tab-nav">
                      <i class="mdi mdi-comment d-block font-22 my-1"></i>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link py-2" data-toggle="tab" href="#media-tab" role="tab" id="media-tab-nav">
                      <i class="mdi mdi-image-plus d-block font-22 my-1"></i>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link py-2" data-toggle="tab" href="#song-tab" role="tab" id="song-tab-nav">
                      <i class="mdi mdi-music d-block font-22 my-1"></i>
                  </a>
              </li>
          </ul>
          <div class="tab-content pt-0">
            <div class="tab-pane active" id="icebreaker-tab" role="tabpanel" style="padding-top:15px;">
              @include('layouts.posts.icebreaker-form')
              <div class="mt-2">
                <button type="submit" class="ladda-button btn btn-blue " name="icebreaker" value="Post Media" dir="ltr" data-style="expand-left">Post Answer</button>
              </div>
            </div>
            <div class="tab-pane" id="media-tab" role="tabpanel" style="padding-top:15px;">
              <input style="margin-top:20px;" type="file"  data-height="150" name="photo" data-allowed-file-extensions="png jpg jpeg GIF gif PNG JPG JPEG" data-max-file-size-preview="3M" class="dropify"/>
              <div class="mt-2">
                <button type="submit" class="ladda-button btn btn-blue" name="postmedia" value="Post Media" dir="ltr" data-style="expand-left">Post Media</button>
              </div>
            </div>
            <div class="tab-pane" id="song-tab" role="tabpanel" style="padding-top:15px;">
              <label class="form-label">Copy a Spotify song URL and paste it below</label>
              <div class="input-group">
                  <input type="text" class="form-control" placeholder="example: https://open.spotify.com/embed/track/5FEXPoPnzueFJQCPRIrC3c" name="song" aria-label="Spotify Song URL" id="song-input">
                  <div class="input-group-append">
                      <button class="btn btn-dark waves-effect waves-light" type="button" id="song-verify"><i class="fa fa-search"></i></button>
                  </div>
              </div>
              <br>
              <div id="song-preview-container"></div>
              <div class="mt-2">
                <small id="song-helper-text">Verify url with the <i class="fa fa-search"></i> button to post.</small><br>
                <button type="submit" class="ladda-button btn btn-blue disabled" name="postsong" value="Post Media" dir="ltr" data-style="expand-left" id="post-song-button">Post Song</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

  </div>
  <div class="avatar-lg rounded-circle bg-blue border-blue border" id="addPostButton">
      <i class="fe-plus font-36 avatar-title text-white"></i>
  </div>
@endsection

@section('js')
  <script src="../assets/libs/dropify/js/dropify.min.js"></script>
  <script src="../assets/libs/dropzone/min/dropzone.min.js"></script>
  <script src="../assets/js/pages/form-fileuploads.init.js"></script>
  <script src="https://masonry.desandro.com/masonry.pkgd.js"></script>
  
  <!-- Loading buttons js -->
  <script src="../assets/libs/ladda/spin.min.js"></script>
  <script src="../assets/libs/ladda/ladda.min.js"></script>
  <!-- Buttons init js-->
  <script src="../assets/js/pages/loading-btn.init.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
  <script src="/js/discover.js"></script>
  @endsection

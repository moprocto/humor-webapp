<!-- Begin Mailchimp Signup Form -->
                    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                        /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                           We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                    </style>
                    <div id="mc_embed_signup" class="card popular-price" style="border-top:none;">
                        <form action="https://humorapp.us4.list-manage.com/subscribe/post?u=e228efa131d9b6bb27ccb2a2d&amp;id=90616c88d5" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <input type="hidden" name="MMERGE4" value='{{ $results["contextual"] }}' id="mce-MMERGE4">
                            <input type="hidden" name="MMERGE5" value='{{ $results["dark"] }}' id="mce-MMERGE5">
                            <input type="hidden" name="MMERGE8" value='{{ $results["physical"] }}' id="mce-MMERGE8">
                            <input type="hidden" name="MMERGE18" value='{{ $results["sexual"] }}' id="mce-MMERGE18">
                            <input type="hidden" name="MMERGE20" value='{{ $results["weird"] }}' id="mce-MMERGE20">
                            <input type="hidden" name="MMERGE21" value='{{ $results["witty"] }}' id="mce-MMERGE21">
                            <input type="hidden" name="MMERGE19" value="{{ $quizID }}"  id="mce-MMERGE19">
                            <div id="mc_embed_signup_scroll">
                                <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                                <div class="mc-field-group">
                                    <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span></label>
                                    <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL">
                                </div>
                                <div class="mc-field-group">
                                    <label>We have a strict no-spam policy.</label>
                                </div>
                                <div id="mce-responses" class="clear" style="margin:0; padding:0;">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response alert alert-success" id="mce-success-response" style="margin:25px 0; display:none; padding:10px;"></div>
                                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                    <input type="text" name="b_e228efa131d9b6bb27ccb2a2d_90616c88d5" tabindex="-1" value="">
                                </div>
                                <div class="clear">
                                    <input type="submit" value="Get Launch Notification     " name="subscribe" id="mc-embedded-subscribe" class="btn btn-success" style="margin:20px 0; display: inline;"> <img src="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/160/apple/96/rocket_1f680.png" width="20" style="display: inline; margin-left:-35px;">
                                </div>
                            </div>
                             <br>
                        Questions? Email us at <a href="mailto:hello@humorapp.co">hello@humorapp.co</a> :)
                        </form>
                       
                    </div>
                    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email'}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
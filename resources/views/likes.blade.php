@extends('layouts.app-one-col')

@section('css')
  <link href="../assets/libs/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/libs/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
  <link href="https://vjs.zencdn.net/7.8.3/video-js.css" rel="stylesheet" />

  <style type="text/css">
    #image-upload{
      display: none;
    }
    #toggleImage{
      background: #f3f7f9;
      border-color: #f3f7f9;
    }
    #toggleImage.active{
      background: #d1e0e8;
    }
    .card.bg-primary{
      background-color: #eff0f2 !important;
    }
    .card-avatar{
      width: 40px;
      position: absolute;
      margin-top: -40px;
      right:0;
    }

    /* ---- grid-item ---- */
    .swal2-image{
      border-radius: 50% !important;
    }
    .liked{
      pointer-events: none !important;
    }
    .icebreakanswer{
      margin-top:0;
      margin-bottom: 20px;
    }
    .swal2-popup .swal2-styled.swal2-confirm{
      background: #f75964;


    }
    .swal2-popup .swal2-styled.swal2-confirm:hover{
      background: #f52837!important;
    }
    img.swal2-avi{
      position: absolute;
      left:20px;
      top:20px;
      width: 50px;
    }
    #addPostButton{
      display: none;
    }
    @media (min-width: 992px){
      #discover-maker{
        position: fixed; right: 20px;
        display: inline-block !important;
        z-index: 102;
      }

    }
    @media (max-width: 600px){
    .grid-item--width2{
      width: 100%;
    }
    #discover-maker{
      display: none;
    }
    #addPostButton{
      display: block;
      position: fixed;
      bottom:10px;
      right: 10px;
      z-index: 200;
      box-shadow: 0 2px 6px 0 rgba(0,0,0,.5);
    }
    .font-36{
      font-size: 36px;
    }
  }
  .loadingoverlay{
    z-index: 101 !important;
  }
  </style>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-5 col-sm-12 offset-md-3 mypost">
    @if(empty($likes))
      <h4 class="text-center" style="width: 100%;">No likes, yet! They'll be here faster than you can count the alphabet backwards!</h4> <br><h5 class="text-center" style="width: 100%;">Keep posting in <a href="/home">Discover</a></h5>
    @endif
    @foreach($likes as $like)
      @php
        $admirer = $like->profile;
        $post = $like->post;
        $message = $like->message;
      @endphp
      
        @if($post->type < 3)
          <div class="card">
            <div class="card-body">
              <div class="text-left">
                <h5 class="card-title"><i class="mdi mdi-format-quote-open font-20"></i> The most famous person I ever met was...</h5>
              </div>
              <div class="col-md-12 text-center">
                <h3 class="card-text">John Lewis</h3>
              </div>
              <div class="col-md-12 col-sm-12 text-left">
                <h5 class="card-title">{{ $admirer->displayName }}</h5>
              </div>
              <div class="col-md-12 col-sm-12">
                <img class="card-img-top img-fluid card-avatar rounded-circle lazy" data-src="{{ $admirer->avatar }}" alt="{{ $admirer->displayName }}" data-featherlight="{{ $admirer->avatar }}">  
              </div>
              @if(isset($message))
              <div class="col-md-12 col-sm-12">
                <h4><i class="mdi mdi-paw text-danger"></i> {{ $message->message }}</h4>
              </div>
              @endif
            </div>
          </div>
        @endif
        @if($post->type == 3)
          <div class="card">
              <img class="card-img-top img-fluid load-image lazy" data-featherlight="{{ $post->mediaLink }}" data-src="{{ $post->mediaLink }}" alt="Card image cap">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12 col-sm-12 text-left">
                    <h5 class="card-title">{{ $admirer->displayName }}</h5>
                  </div>
                  <div class="col-md-12 col-sm-12">
                    <img class="card-img-top img-fluid card-avatar rounded-circle lazy" data-src="{{ $admirer->avatar }}" alt="{{ $admirer->displayName }}" data-featherlight="{{ $admirer->avatar }}">  
                  </div>
                  @if(isset($message))
                    <div class="col-md-12 col-sm-12">
                      <h4><i class="mdi mdi-paw text-danger"></i> {{ $message->message }}</h4>
                    </div>
                  @endif
                </div>
              </div>
          </div>
          @endif
          @if($post->type == 4)
          <div class="card">
              <iframe data-src="{{ $post->songURL }}" style="width: 100%;" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media" class="lazy"></iframe>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12 col-sm-12 text-left">
                    <h5 class="card-title">{{ $admirer->displayName }}</h5>
                  </div>
                  <div class="col-md-12 col-sm-12">
                    <img class="card-img-top img-fluid card-avatar rounded-circle lazy" data-src="{{ $admirer->avatar }}" alt="{{ $admirer->displayName }}" data-featherlight="">  
                  </div>
                  @if(isset($message))
                    <div class="col-md-12 col-sm-12">
                      <h4><i class="mdi mdi-paw text-danger"></i> {{ $message->message }}</h4>
                    </div>
                  @endif
                </div>
            </div>
          </div>
          @endif
        @endforeach
      </div>
    </div>
  </div>
@endsection

@section('js')
  
  <!-- Loading buttons js -->
  <script src="../assets/libs/ladda/spin.min.js"></script>
  <script src="../assets/libs/ladda/ladda.min.js"></script>
  <!-- Buttons init js-->
  <script src="../assets/js/pages/loading-btn.init.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
  <script>
    $(".lazy").lazyload()
  </script>
  @endsection

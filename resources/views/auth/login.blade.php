@extends('layouts.auth')

@section('css')
<style>
    .auth-fluid{
        background-image: url('/images/hero-bg-1.jpg');
    }
</style>
@endsection

@section('content')
    <!-- title-->
    <h4 class="mt-0">Sign In</h4>
    <p class="text-muted mb-4"></p>

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            @foreach ($errors->all() as $error)
                {{ $error }}
            @endforeach
        </div>
    @endif

    <!-- form -->
    <form method="POST" action="{{ route('firebase-login') }}">
        @CSRF
        <div class="form-group">
            <label for="emailaddress">Email address</label>
            <input class="form-control" type="email" id="emailaddress" name="email" required="" placeholder="Enter your email">
        </div>
        <div class="form-group">
            <a href="{{ route('firebase-reset-get') }}" class="text-muted float-right"><small>Forgot your password?</small></a>
            <label for="password">Password</label>
            <div class="input-group input-group-merge">
                <input type="password" id="password" name="password" class="form-control" placeholder="Enter your password">
                <div class="input-group-append" data-password="false">
                    <div class="input-group-text">
                        <span class="password-eye"></span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="form-group mb-3">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="checkbox-signin">
                <label class="custom-control-label" for="checkbox-signin">Remember me</label>
            </div>
        </div>
        <div class="form-group mb-0 text-center">
            <button class="btn btn-primary btn-block" type="submit">Log In </button>
        </div>
        <!-- social-->
    </form>
    <!-- end form-->

    <!-- Footer-->
    <footer class="footer footer-alt">
        <p class="text-muted">Don't have an account? <a href="{{ route('register') }}" class="text-muted ml-1"><b>Sign Up</b></a></p>

        <div class="">
            <a href="https://apps.apple.com/us/app/id1527176305" target="_blank"><img src="/images/download-app-ios.png" style="width: 150px;"></a>
        </div>
    </footer>

@endsection

@section('testimonial')
    <!-- Auth fluid right content -->
            <div class="auth-fluid-right text-center">
                <div class="auth-user-testimonial">
                    <h2 class="mb-3 text-white">For whatever you're looking for</h2>
                    <p class="lead">you'll find it here :)
                    </p>
                </div> <!-- end auth-user-testimonial-->
            </div>
            <!-- end Auth fluid right content -->
@endsection()

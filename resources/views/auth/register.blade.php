@extends('layouts.auth')

@section('css')
<style>
    .auth-fluid{
        background-image: url('/images/hero-bg-3b.jpg');
    }
</style>
@endsection

@section('content')

<!-- title-->
    <h4 class="mt-0">Sign Up</h4>
    <p class="text-muted mb-4">Welcome to the dark side</p>

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            @foreach ($errors->all() as $error)
                {{ $error }}
            @endforeach
        </div>
    @endif

    <!-- form -->
    <form method="POST" action="{{ route('firebase-register') }}">
        @csrf
        <input type="hidden" value="" id="timezone" name="timezone">
        <div style="overflow: hidden; height: 0px;background: transparent;" data-description="dummyPanel for Chrome auto-fill issue">
            <input type="text" style="height:0;background: transparent; color: transparent;border: none;" data-description="dummyUsername"> </input>
            <input type="email" style="height:0;background: transparent; color: transparent;border: none;" data-description="dummyEmail"> </input>
            <input type="password" style="height:0;background: transparent; color: transparent;border: none;" data-description="dummyPassword"></input>
        </div>
        <div class="form-group">
            <label for="fullname">Your First Name</label>
            <input class="form-control" type="text" id="fullname" name="name" placeholder="Saitama" value="{{ old('name') }}" required>
        </div>

        <div class="form-group">
            <label>Email address</label>
            <input class="form-control" type="email" name="email" value="{{ old('email') }}" required placeholder="Enter your email">
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <div class="input-group input-group-merge">
                <input type="password" id="password" class="form-control" name="password" placeholder="Enter your password" required="">
                <div class="input-group-append" data-password="false">
                    <div class="input-group-text">
                        <span class="password-eye"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="password">Confirm Password</label>
            <div class="input-group input-group-merge">
                <input type="password" id="password_confirmation" class="form-control" name="password_confirmation" placeholder="Enter your password" required="">
                <div class="input-group-append" data-password="false">
                    <div class="input-group-text">
                        <span class="password-eye"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="checkbox-signup" required>
                <label class="custom-control-label" for="checkbox-signup" >I accept <a href="{{ route('policy') }}" target="_blank" class="text-dark">Terms and Conditions</a></label>
            </div>
        </div>
        <div class="form-group mb-0 text-center">
            <button class="btn btn-primary waves-effect waves-light btn-block" type="submit"> Sign Up </button>
        </div>
    </form>
    <!-- end form-->

    <!-- Footer-->
    <footer class="footer footer-alt">
        <p class="text-muted">Already have account? <a href="{{ route('login') }}" class="text-muted ml-1"><b>Log In</b></a></p>

        <div class="">
            <a href="https://apps.apple.com/us/app/id1527176305" target="_blank"><img src="/images/download-app-ios.png" style="width: 150px;"></a>
        </div>
    </footer>

@endsection

@section('testimonial')
    <!-- Auth fluid right content -->
            <div class="auth-fluid-right text-center">
                <div class="auth-user-testimonial">
                    <h2 class="mb-3 text-white">All the best relationships</h2>
                    <p class="lead">start with a good laugh
                    </p>
                </div> <!-- end auth-user-testimonial-->
            </div>
            <!-- end Auth fluid right content -->
@endsection()

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.7/jstz.js"></script>
    <script>
        var tz = jstz.determine();
        $("#timezone").val(tz.name());
    </script>
@endsection
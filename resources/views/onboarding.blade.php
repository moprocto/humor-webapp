@extends('layouts.app')

@section('css')
<link href="/assets/libs/toggle-switch/switch--radio-buttons.css" rel="stylesheet" type="text/css" />
    <style>
        body{
            background-color: #2892fd;
            background-size: cover;
            background-position: center;
        }
        #simple-dragula > div:first-child .card{
            border: 3px solid gold !important;
        }
        .card.bg-primary{
            background-color: #eff0f2 !important;
        }
        .table thead th{
            border-top:none !important;
        }
        @media (max-width: 600px){
            div .account-pages{
                margin-top: 0 !important;
            }
            body{
                 background: whitesmoke;
            }
            main{
                box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            }
            .w-75{
                width: 100% !important;
            }
        }
        ul.wizard > li:first-child{
            display: none;
        }
        .h1{
            font-size:48px;
        }
        #lab .btn-label{
            float:left;
        }
        #lab .button-list{
            margin-bottom:10px;
        }
        button[disabled]{
            cursor: inherit !important;
        }
            .input_hidden {
                position: absolute;
                left: -9999px;
            }

            .selected {
                background-color: rgba(0,255,0, 0.7);
            }

            .buttons  label {
                display: inline-block;
                cursor: pointer;
                text-align: center;
            }
            .buttons label:hover {
                background-color: rgba(0,255,0, 0.2);
            }

            .buttons  label img {
                padding: 3px;
                width: 40px;
            }
            .content-page{
                margin-left:0 !important;
            }
            @media only screen and (max-width: 500px) {

                .col-sm-4{
                    width: 32% !important;
                }
                .content-page{
                    padding-top:0 !important;
                    margin-top:0 !important;
                }
                .page-title img{
                    max-width: 100px !important;
                }
                .col-xs-4 img{
                    max-width: 40px;
                }
                .quiz-image{
                    width: 100%;
                    min-height: auto !important;
                }
                .mb-150{
                    margin-bottom:150px !important;
                }
            }
            li.disabled a{
                display: none;
            }
    </style>
    <link href="../assets/libs/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="account-pages mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-8">
                <div class="card bg-pattern">
                    <div class="card-body p-4">
                        <form method="POST" action="{{ route('add-onboarding') }}" role="form" enctype="multipart/form-data">
                            @CSRF
                            
                            <div id="basicwizard">
                                <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-4">
                                    <li class="nav-item">
                                        <a href="#basictab1" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2 active">
                                            <i class="mdi mdi-account-circle mr-1"></i>
                                            <span class="d-none d-sm-inline">Base</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#basictab2" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-cow"></i>
                                            <span class="d-none d-sm-inline">Protein</span>
                                        </a>
                                    </li>
                                    <!--
                                    <li class="nav-item">
                                        <a href="#basictab3" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-one-up"></i>
                                            <span class="d-none d-sm-inline">Toppings</span>
                                        </a>
                                    </li>
                                -->
                                    <li class="nav-item">
                                        <a href="#basictab4" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="fas fa-laugh-squint mr-1"></i>
                                            <span class="d-none d-sm-inline">Humor</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#basictab5" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-cup mr-1"></i>
                                            <span class="d-none d-sm-inline">Beverage</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#basictab6" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-checkbox-marked-circle-outline mr-1"></i>
                                            <span class="d-none d-sm-inline">Finish</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content b-0 mb-0 pt-0">
                                    <div class="tab-pane active" id="basictab1">
                                        <div class="text-center w-75 m-auto mb-15">
                                            <h4 class="text-dark-50 text-center mt-3"><span class="hidden-sm-up">Your Base: </span>A little bit about me</h4>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="card mt-3 border">
                                                    <div class="card-body">
                                                        <input type="file" data-height="100" name="photos[]" data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" data-max-file-size-preview="4M" class="dropify"/>
                                                    </div> <!-- end card-body-->
                                                </div> <!-- end card-->
                                            </div> <!-- end col-->
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row mb-3">
                                                    <label class="col-md-3 col-form-label" for="userName">I was born on</label>
                                                    <div class="col-md-9">
                                                        <input type="date" class="form-control" id="birthday" name="birthday" value="{{ Date('1999-m-d') }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-3">
                                                    <label class="col-md-3 col-form-label" for="password"> I identify as a</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="gender">
                                                            <option value="x">Please Select</option>
                                                            <option value="man">Man</option>
                                                            <option value="woman">Woman</option>
                                                            <option value="neither">Neither</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--
                                                <div class="form-group row mb-3">
                                                    <label class="col-md-3 col-form-label" for="zipcode"> My zip code is</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="zipcode" name="zipcode" class="form-control" value="" placeholder="99999">
                                                    </div>
                                                </div>
                                            -->
                                                <div class="form-group row mb-3">
                                                    <label class="col-md-3 col-form-label" for="bio"> What others should know about me</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control" id="bio" name="bio" placeholder="I'm a water sign so I like to keep things wavy"></textarea>
                                                    </div>
                                                </div>
                                            </div> <!-- end col -->
                                        </div> <!-- end row -->
                                    </div>

                                    <div class="tab-pane" id="basictab2">
                                        <div class="text-center w-75 m-auto mb-15">
                                                <h4 class="text-dark-50 text-center mt-3"><span class="hidden-sm-up">Your Protein: </span>The heavy stuff</h4>
                                                <br>
                                            </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row mb-3">
                                                    <label class="col-md-3 col-form-label" for="school"> I attended</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="school" name="school" class="form-control" value="" placeholder="Hogwarts">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-3">
                                                    <label class="col-md-3 col-form-label" for="jobTitle"> I earn my living as a</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="jobTitle" name="jobTitle" class="form-control" value="" placeholder="Programming Wizard">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-3">
                                                        <label class="col-md-3 col-form-label" for="lookingFor"> I am looking for</label>
                                                        <div class="col-md-9">
                                                            <select class="form-control" name="lookingFor">
                                                                <option value="Unsure">Not sure</option>
                                                                <option value="Relationship">A committed relationship</option>
                                                                <option value="Casual">Something casual</option>
                                                                <option value="Platonic">Platonic friends</option>
                                                                <option value="Marriage">Marriage</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                <!--
                                                <div class="form-group row mb-3">
                                                    <label class="col-md-3 col-form-label" for="showMe"> Show me</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="showMe">
                                                            <option value="women">Women</option>
                                                            <option value="men">Men</option>
                                                            <option value="both" selected="">Everyone</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            -
                                                <div class="form-group row mb-3">
                                                    <label class="col-md-3 col-form-label" for="religion"> My belief system</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control dropdown" id="religion" name="religion">
                                                            <option value="x">Select One</option>
                                                            <option value="Christianity">Christianity</option>
                                                            <option value="Buddhism">Buddhism</option>
                                                            <option value="Hinduism">Hinduism</option>
                                                            <option value="Islam">Islam</option>
                                                            <option value="Catholic">Catholic</option>
                                                            <option value="Judaism">Judaism</option>
                                                            <option value="Spiritual">Spiritual</option>
                                                            <option value="Sikhism">Sikhism</option>
                                                            <option value="Other">Other</option>
                                                            <option value="Agnostic">Agnostic</option>
                                                            <option value="Atheist">Atheist</option>
                                                        </select>
                                                    </div>
                                                </div>
                        
                                                <div class="form-group row mb-3">
                                                    <label class="col-md-3 col-form-label" for="educationLevel">My education level</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control dropdown" id="educationLevel" name="educationLevel">
                                                            <option value="high school">High School</option>
                                                            <option value="GED">GED</option>
                                                            <option value="Vocational">Vocational/Trade</option>
                                                            <option value="College">Some College</option>
                                                            <option value="Bachelors">Bachelor's degree</option>
                                                            <option value="Masters">Master's degree</option>
                                                            <option value="Doctorate">Doctorate</option>
                                                            <option value="Professional">Doctorate in Medical, Law, or Dental</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                -->
                                            </div> <!-- end col -->
                                        </div> <!-- end row -->
                                    </div>

                                    <div class="tab-pane" id="basictab3">
                                        <div class="text-center w-75 m-auto mb-15">
                                            <h4 class="text-dark-50 text-center mt-3"><span class="hidden-sm-up">Your Toppings: </span>Any flavor?</h4>
                                            <br>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row mb-3">
                                                    <div class="w-100">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>When it comes to</th>
                                                                    <th class="text-center">Never</th>
                                                                    <th class="text-center">Socially</th>
                                                                    <th class="text-center">Frequently</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Smoking, I do it</td>
                                                                    <td class="text-center"><input type="radio" name="smoking" value="0" checked=""></td>
                                                                    <td class="text-center"><input type="radio" name="smoking" value="1"></td>
                                                                    <td class="text-center"><input type="radio" name="smoking" value="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Drinking, I do it</td>
                                                                    <td class="text-center"><input type="radio" name="drinking" value="0" ></td>
                                                                    <td class="text-center"><input type="radio" name="drinking" value="1" checked=""></td>
                                                                    <td class="text-center"><input type="radio" name="drinking" value="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Drugs, I do them</td>
                                                                    <td class="text-center"><input type="radio" name="drugs" value="0" checked=""></td>
                                                                    <td class="text-center"><input type="radio" name="drugs" value="1"></td>
                                                                    <td class="text-center"><input type="radio" name="drugs" value="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Marijuana, I partake</td>
                                                                    <td class="text-center"><input type="radio" name="marijuana" value="0" checked=""></td>
                                                                    <td class="text-center"><input type="radio" name="marijuana" value="1"></td>
                                                                    <td class="text-center"><input type="radio" name="marijuana" value="2"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- end col -->
                                        </div> <!-- end row -->
                                    </div>
                                    <div class="tab-pane" id="basictab4">
                                        <div class="text-center w-75 m-auto mb-15 mb-150">
                                            <h4 class="text-dark-50 text-center mt-3" id="quiz-form-title"><span class="hidden-sm-up">Your Humor: </span> How Funny Are These Memes?</h4>
                                            <br>
                                            @include('layouts.quiz-form')
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="basictab5">
                                        <div class="text-center w-75 m-auto mb-15">
                                            <h4 class="text-dark-50 text-center mt-3"><span class="hidden-sm-up">Your Beverage: </span>Start a conversation worth sharing over drinks:</h4>
                                            <br>
                                            @include('layouts.posts.icebreaker-form')
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="basictab6">
                                        <div class="text-center w-75 m-auto mb-15">
                                            <h4 class="text-dark-50 text-center mt-3">Buckle up!</h4>
                                            <br>
                                            <p>You're ready to jump into the fray. All you have to do is press the...</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 text-center">
                                                <div class="form-group row mb-3">
                                                    <div class="col-md-12">
                                                        <input type="submit" name="submit" value="Shiny Green Submit Button" class="btn btn-success">
                                                    </div>
                                                </div>
                                            </div> <!-- end col -->
                                        </div> <!-- end row -->
                                    </div>
                                    <ul class="list-inline wizard mb-0">
                                        <li class="previous list-inline-item">
                                            <a href="javascript: void(0);" class="btn btn-secondary">Previous</a>
                                        </li>
                                        <li class="next list-inline-item float-right">
                                            <a href="javascript: void(0);" class="btn btn-secondary">Next</a>
                                        </li>
                                    </ul>

                                </div> <!-- tab-content -->
                            </div> <!-- end #basicwizard-->
                        </form>
                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end page -->

@endsection

@section('js')
    <script src="/assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <!-- Loading buttons js -->
    <script src="../assets/libs/dragula/dragula.min.js"></script>

    <!-- Buttons init js-->
    <script src="../assets/js/pages/dragula.init.js"></script>
    <script src="../assets/libs/dropify/js/dropify.min.js"></script>

    <script src="../assets/libs/dropzone/min/dropzone.min.js"></script>
    <script src="../assets/js/pages/form-fileuploads.init.js"></script>
    <script src="/js/lab.js"></script>
    <script>
        $(document).ready(function(){
            $("#basicwizard").bootstrapWizard({
                onInit: function (event, current) {
                    $('ul.wizard > li:first-child').attr('style', 'display:none');
                },
                onTabShow: function (event, current, next) {
                    window.scrollTo(0, 0); 
                    if (next > 0) {
                        $('ul.wizard > li:first-child').attr('style', 'display:inline-block');
                        $('ul.wizard > li:last-child').attr('style', 'display:inline-block');
                    } else {
                        $('ul.wizard > li:first-child').attr('style', 'display:none');
                        $('ul.wizard > li:last-child').attr('style', 'display:inline-block');
                    }
                    if(next >= 5){
                        $('ul.wizard > li:last-child').attr('style', 'display:none');
                    }
                }
            });
            
            getIcebreaker();

            $("body").on("click", "#icebreaker-get", function(){
                getIcebreaker();
            });

            function getIcebreaker(){
                var currentBreaker = $("#icebreaker_id").val();
                $.ajax({
                  url: '/api/icebreaker/' + currentBreaker
                }).done(function(data) {
                  $("#icebreaker").html(data);
                });
            }

            $(".dropify").dropify({
                messages:{
                    default:"Upload a profile pic"
                }
            });

            var icon = $(".file-icon");
                icon.removeClass("file-icon");
                icon.addClass("mdi mdi-account-box-outline h1");
            $('.buttons input:radio').each(function(){
                $(this).addClass('input_hidden');
            });

            $('.buttons label').on("click", function() {
                $(this).addClass('selected');
                $(this).siblings().removeClass('selected');
            });

            $('.buttons .reset').on("click", function() {
                var buttons = $(this).parent().parent().find("label");
                buttons.each(function(){
                    $(this).removeClass('selected');
                });
            });

            var image;

            $(".quiz-image").each(function(){
                image = $(this).attr("data-url");
                $(this).delay(500).attr("src", image); 
            });

            $("body").on("click", ".score-meme", function(){
                scoreMeme($(this), 10);
            });
        });
    </script>
@endsection

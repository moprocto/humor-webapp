<div class="card-body">
    <ul class="conversation-list" data-simplebar="init" style="max-height: 460px">
        <div class="simplebar-wrapper" style="margin: 0px -15px;">
            <div class="simplebar-height-auto-observer-wrapper">
                <div class="simplebar-height-auto-observer"></div>
            </div>
            <div class="simplebar-mask">
                <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                    <div class="simplebar-content-wrapper" style="height: auto; overflow: hidden scroll;" id="chat-scroll">
                        <div class="simplebar-content" style="padding: 0px 15px;" id="chat-window" data-to="">
                            
                            <div class="d-flex justify-content-center mt-3">
                                <div class="spinner-border text-blue" role="status"></div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="simplebar-placeholder" style="width: auto; height: 853px;"></div>
        </div>
        <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
            <div class="simplebar-scrollbar" style="width: 0px; display: none;"></div>
        </div>
        <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
            <div class="simplebar-scrollbar" style="height: 248px; transform: translate3d(0px, 0px, 0px); display: block;"></div>
        </div>
    </ul>
    <div class="row keyboard">
        <div class="col">
            <div class="mt-2 bg-light p-3 rounded">
                
                    <div class="row">
                        <div class="col mb-2 mb-sm-0 col-sm-10">
                            <input type="text" class="form-control border-0" placeholder="Enter your text" required="" id="message-body">
                            <div class="invalid-feedback">
                                Please enter your messsage
                            </div>
                        </div>
                        <div class="col-sm-auto col-sm-2">
                            <div class="btn-group">
                                <!-- <a href="#" class="btn btn-light"><i class="fe-paperclip"></i></a> -->
                                <button type="button" id="send-message" class="btn btn-success chat-send btn-block" data-to=""><i class="fe-send"></i></button>
                                <input type="hidden" id="send-heartbeat" value="5000">
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row-->
            </div> 
        </div> <!-- end col-->
    </div>
</div>
<script>
   Swal.fire({
      title: '{{ session("error")["header"] }}',
      text: '{{ session("error")["body"] }}',
      type: "error",
      confirmButtonClass: "btn btn-confirm mt-2"
  });
</script>
<script>
   Swal.fire({
      title: '{{ session("success")["header"] }}',
      text: '{{ session("success")["body"] }}',
      type: "success",
      confirmButtonClass: "btn btn-confirm mt-2"
  });
</script>
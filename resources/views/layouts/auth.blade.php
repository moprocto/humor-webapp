<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>humor</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="humor is where you find something more with the people you connect with" name="description" />
        <meta content="humor" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="/images/logo.png">
        <link rel="apple-touch-icon" href="/images/logo-bw.jpg">
        <link rel="apple-touch-icon" sizes="76x76" href="/images/logo-bw.jpg">
        <link rel="apple-touch-icon" sizes="120x120" href="/images/logo-bw.jpg">
        <link rel="apple-touch-icon" sizes="152x152" href="/images/logo-bw.jpg">

        <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
        <meta property="og:site_name" content="humor"/> <!-- website name -->
        <meta property="og:site" content="https://humorapp.co"/> <!-- website link -->
        <meta property="og:title" content="humor: laughter is in the air"/> <!-- title shown in the actual shared post -->
        <meta property="og:description" content="humor is where you find something more with the people you connect with"/> <!-- description shown in the actual shared post -->
        <meta property="og:image" content="http://humorapp.co/images/hero-bg-3a.jpg"/> <!-- image link, make sure it's jpg -->
        <meta property="og:url" content="https://humorapp.co"/> <!-- where do you want your post to link to -->
        <meta property="og:type" content="website"/>
        <!-- App css -->
        <link href="../assets/css/bootstrap-saas.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
        <link href="../assets/css/app-saas.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

        <link href="../assets/css/bootstrap-saas-dark.min.css" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
        <link href="../assets/css/app-saas-dark.min.css" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />

        <!-- icons -->
        <link href="../assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            .auth-fluid:before{
                position: absolute;
                content: '';
                background-image: linear-gradient(to left, rgba(50, 100, 245, 0.55), rgba(74, 84, 232, 0.53), rgba(91, 66, 219, 0.50), rgba(104, 44, 203, 0.53), rgba(114, 2, 187, 0.55));
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
            }
            @media (max-width: 990px){
                .footer{
                    position: relative;
                }
            }
        </style>
        @yield('css')
    </head>

    <body class="loading auth-fluid-pages pb-0">

        <div class="auth-fluid">
            <!--Auth fluid left content -->
            <div class="auth-fluid-form-box">
                <div class="align-items-center d-flex h-100">
                    <div class="card-body">

                        <!-- Logo -->
                        <div class="auth-brand text-center text-lg-left">
                            <div class="auth-logo">
                                <a href="/" class="logo logo-dark text-center">
                                    <span class="logo-lg">
                                        <img src="/images/logo-black.png" alt="" height="50">
                                    </span>
                                </a>
            
                                <a href="index.html" class="logo logo-light text-center">
                                    <span class="logo-lg">
                                        <img src="/images/logo-white.png" alt="" height="50">
                                    </span>
                                </a>
                            </div>
                        </div>
                        @yield('content')

                    </div> <!-- end .card-body -->
                </div> <!-- end .align-items-center.d-flex.h-100-->
            </div>
            <!-- end auth-fluid-form-box-->

            @yield('testimonial')
            
        </div>
        <!-- end auth-fluid-->

        <!-- Vendor js -->
        <script src="../assets/js/vendor.min.js"></script>

        <!-- App js -->
        <script src="../assets/js/app.min.js"></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163662190-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163662190-1');
</script>
        @yield('js')
    </body>
</html>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>humor</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="humor is where you find something more with the people you connect with" name="description" />
        <meta content="humor" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="/images/logo.png">
        <link rel="apple-touch-icon" href="/images/logo-bw.jpg">
        <link rel="apple-touch-icon" sizes="76x76" href="/images/logo-bw.jpg">
        <link rel="apple-touch-icon" sizes="120x120" href="/images/logo-bw.jpg">
        <link rel="apple-touch-icon" sizes="152x152" href="/images/logo-bw.jpg">
        <meta id="CSRF" content="{{ CSRF_TOKEN() }}">
        <meta id="uid" content="{{ $user->uid }}">

        <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
        <meta property="og:site_name" content="humor"/> <!-- website name -->
        <meta property="og:site" content="https://humorapp.co"/> <!-- website link -->
        <meta property="og:title" content="humor: laughter is in the air"/> <!-- title shown in the actual shared post -->
        <meta property="og:description" content="humor is where you find something more with the people you connect with"/> <!-- description shown in the actual shared post -->
        <meta property="og:image" content="http://humorapp.co/images/hero-bg-3a.jpg"/> <!-- image link, make sure it's jpg -->
        <meta property="og:url" content="https://humorapp.co"/> <!-- where do you want your post to link to -->
        <meta property="og:type" content="website"/>

        
        <!-- Jquery Toast css -->
        <link href="/assets/libs/jquery-toast-plugin/jquery.toast.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <!-- App css -->
        <link href="/assets/css/bootstrap-saas.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
        <link href="/assets/css/app-saas.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />
        <!-- icons -->
        <link href="/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/featherlight/featherlight.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            body[data-layout-mode=two-column] .sidebar-main-menu{
                width: 280px;
            }
            .jq-toast-wrap{
                cursor: pointer;
            }
            @media (min-width: 992px){
                body[data-layout-mode=two-column] .content-page{
                    margin-left: 70px;
                }
                body[data-layout-mode=two-column] .footer {
                    left: 70px;
                }
                .topnav-menu{
                    display: none;
                }
            }
            body[data-layout-mode=two-column] .sidebar-icon-menu {
                /* background-color: #7202bb; */
                background-color: #6332f6;
            }
            notify{
                position: absolute;
                right:15;
                width: 20px;
                height: 20px;
                text-align: center;
                padding-top: 4px;
                
            }
            @media (min-width: 992px){
                .featherlight-content img {
                    min-width: 500px;                
                }
            }
            @media only screen and (max-width: 960px) {
              
              .xs-column-reverse {
                display: flex;
                flex-direction: column-reverse;
              }
              .topnav-menu a.active{
                color: #6332f6 !important;
              }
            }
            @media only screen and (max-width: 990px) {
                .card-avatar{
                    width: 40px;
                    position: absolute;
                    margin-top: -40px !important;
                    top:0;
                    right:0;
                }
                .logo-box{
                    margin-left:20px;
                }
                .logo-box img{
                    height: 52px;
                }
            }
            .jq-toast-single{
                background: white;
                color:#343a40;
                padding:0;
                border: 1px solid rgba(0,0,0,.1);
                box-shadow: 0 0.25rem 0.75rem rgba(0,0,0,.1);
            }
            
            .jq-toast-heading{
                border-bottom: 1px solid rgba(0,0,0,.05);
                padding:5px 10px;
                margin:0;
            }
            .jq-toast-single h2{
                margin:0;
            }
            .jq-toast-single p{
                padding: 6px 10px;
                background: rgb(248 249 249);
            }
        </style>
        @yield('css')
    </head>

    <body class="loading" data-layout-mode="two-column" data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "light", "size": "default", "showuser": false}, "topbar": {"color": "light"}, "showRightSidebarOnPageLoad": false}'>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">

                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="">
                            <a class="nav-link @if($page == 'discover') active @endif" href="/home">
                                <i class="mdi mdi-compass-outline  font-22 my-1 "></i>
                                <!-- <notify class="badge badge-danger rounded-circle">10</notify> -->
                            </a>
                        </li>
                        <li class="">
                            <a class="nav-link @if($page == 'home') active @endif" href="/messages">
                                <i class="mdi mdi-message-text  font-22 my-1"></i>
                            </a>
                        </li>
                        <li class="">
                            <a class="nav-link @if($page == 'likes') active @endif" href="/likes">
                                <i class="mdi mdi-paw  font-22 my-1 "></i>
                                <!-- <notify class="badge badge-danger rounded-circle">10</notify> -->
                            </a>
                        </li>
    
                        <li class="">
                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="@if(isset($profile['avatar'])){{ $profile['avatar'] }}@else /images/logo.png @endif" class="rounded-circle user-avatar">
                                <span class="pro-user-name ml-1">
                                    <i class="mdi mdi-chevron-down"></i> 
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
    
                                <!-- item-->
                                <a href="/profile" class="dropdown-item notify-item">
                                    <i class="fe-user"></i>
                                    <span>My Profile</span>
                                </a>
    
                                <div class="dropdown-divider"></div>
    
                                <!-- item-->
                                <a href="/logout" class="dropdown-item notify-item">
                                    <i class="fe-log-out"></i>
                                    <span>Logout</span>
                                </a>
    
                            </div>
                        </li>
    
                    </ul>
    
                    <!-- LOGO -->
                    <div class="logo-box">
                        <a href="/home" class="logo logo-dark text-center">
                            <span class="logo-sm">
                                <img src="/images/logo-black.png" alt="" height="62">
                                <!-- <span class="logo-lg-text-light">UBold</span> -->
                            </span>
                            <span class="logo-lg">
                                <img src="/images/logo-black.png" alt="" height="60">
                                <!-- <span class="logo-lg-text-light">U</span> -->
                            </span>
                        </a>
    
                        <a href="/home" class="logo logo-light text-center">
                            <span class="logo-sm">
                                <img src="/images/logo-black.png" alt="" height="62">
                            </span>
                            <span class="logo-lg">
                                <img src="/images/logo-black.png" alt="" height="60">
                            </span>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="h-100" >

                    <div class="sidebar-content">
                        <div class="sidebar-icon-menu h-100" data-simplebar>
                            <!-- LOGO -->
                            @include('layouts.nav')
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- End Sidebar -->

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->

            <div class="row">
                <div class="col-md-12"><br>
                </div>
            </div>

            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">

                        @yield('content')
                        
                    </div> <!-- container -->

                </div> <!-- content -->

                @include('layouts.footer')

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="/assets/js/vendor.min.js"></script>

        <!-- Sweet Alerts js -->
        <script src="/assets/libs/sweetalert2/sweetalert2.min.js"></script>
        <!-- Toast -->

        <!-- Tost-->
        <script src="/assets/libs/jquery-toast-plugin/jquery.toast.min.js"></script>
        <!-- toastr init js-->
        <script src="/assets/js/pages/toastr.init.js"></script>

        <!-- App js -->
        <script src="/assets/js/app.min.js"></script>

        <script src="/assets/libs/featherlight/featherlight.js"></script>
        <script src="/assets/libs/lazyload/lazyload.min.js"></script>

        @include('layouts.alerts.manager')
        <script src="/js/scripts.js"></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163662190-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-163662190-1');
        </script>

        @yield('js')
    </body>
</html>
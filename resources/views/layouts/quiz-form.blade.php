<div class="row" style="padding-bottom: 10px; overflow: hidden; margin:0;">
    <div class="col-md-12 text-center" style="margin-bottom:0px; overflow: hidden; padding:0; min-height: 280px;" id="meme-window">
            @if(!empty($memes[0]->prompt))<h3 class="text-center" style="font-size:16px;">{{ $memes[0]->prompt }}</h3>@endif
        <img data-src="{{ env('HUMOR_IMG_URL') . $memes[0]->file }}" width="100%" class="quiz-image lazy" style="max-height: 380px;">
    </div> <!-- end col -->

    <div class="buttons card-body" style="display: contents; margin-bottom: 35px;" id="quiz-buttons">
        <input type="radio" name="item_{{ $memes[0]->id }}" class="" /><label for="{{ $memes[0]->id }}1" class="col-md-4 col-sm-4 col-xs-4 score-meme" data-meme="{{ $memes[0]->id }}" data-score="-1"><img src="/images/lame.png" /><br>Lame!</label>
    
        <input type="radio" name="item_{{ $memes[0]->id }}" /><label for="{{ $memes[0]->id }}2" class="col-md-4 col-sm-4 col-xs-4 score-meme" data-meme="{{ $memes[0]->id }}" data-score="1"><img src="/images/neutral.png" /><br> Okay!</label>
    
        <input type="radio" name="item_{{ $memes[0]->id }}" /><label for="{{ $memes[0]->id }}3" class="col-md-4 col-sm-4 col-xs-4 score-meme" data-meme="{{ $memes[0]->id }}" data-score="2"  ><img src="/images/laughed.png" alt="Super User" /><br> Funny!</label>
        <br>
        <div class="col-md-12 text-center">
            <button type="button" class="reset btn btn-white score-meme" data-meme="{{ $memes[0]->id }}" data-score="0" class="score-meme" ><i class="mdi mdi-refresh"></i> Hmm I don't Get It</button>
        </div>
    </div>
</div> <!-- end row -->
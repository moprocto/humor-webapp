<div class="card bg-primary ">
  <div class="card-body">
    <div class="card-widgets">
        <a href="javascript:;" data-toggle="reload" id="icebreaker-get"><i class="mdi mdi-refresh"></i></a>
    </div>
    <div id="icebreaker">
      <h5 class="card-title mb-0"></h5>
      
      <div class="collapse pt-3 show">
          <input type="hidden" name="icebreaker_id" id="icebreaker_id" value="1">
          <textarea rows="3" class="form-control" name="answer" style="background: transparent; border:none; min-height: 40px;" placeholder="Your Answer..." spellcheck="false"></textarea>
      </div>
    </div>
  </div>
</div>
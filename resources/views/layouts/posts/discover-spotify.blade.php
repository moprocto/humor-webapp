@if(!isset($post["hidden"]))
<div class="grid-item grid-item--width2">
    <div class="card">
        <iframe data-src="{{ $post['songURL'] }}" style="width: 100%;" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media" class="lazy"></iframe>
        <div class="card-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 text-left">
                <h5 class="card-title">{{ $displayName }}</h5>
              </div>
              <div class="col-md-12 col-sm-12">
                <img class="card-img-top img-fluid card-avatar rounded-circle lazy" data-src="{{ $avatar }}"  alt="{{ $displayName }}" data-featherlight="{{ $avatar }}">  
              </div>
            </div>

            <div class="clear-fix mb-2"></div>
            
            <div class="media-body pb-0">
              <div class="float-left like-section" data-post="{{ $key }}" data-type="{{ $post['type'] }}">
                {!! $likeButton !!}
              </div>
              @if($post["user_id"] == $user->uid)
              <div class="float-left" style="margin-right:20px;"><span class="badge badge-danger delete" data-post="{{ $key }}"><i class="mdi mdi-close-thick"></i></span></div> 
              @endif
            </div>
        </div>
    </div>
</div>
@endif
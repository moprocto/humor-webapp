<div class="grid-item grid-item--width2">
  <div class="card">
    <div class="card-body">
        <div class="row">
          <div class="col-md-12 col-sm-12 text-left">
            <h5 class="card-title">{{ $displayName }}</h5>
          </div>
          <div class="col-md-12 col-sm-12">
            <img class="card-img-top img-fluid card-avatar rounded-circle" src="@if(!empty($avatar)){{ $avatar }}@else /images/logo.png @endif" data-src="" alt="{{ $displayName }}" @if(!empty($avatar))data-featherlight="{{ $avatar }}"@endif>  
          </div>
        </div>
        <div class="text-left">
          <h5 class="card-title"><i class="mdi mdi-format-quote-open font-20"></i> {{ $post["icebreaker"] }}...</h5>
        </div>
        <h3 class="card-text">{{ $post["answer"] }}</h3>
        
        <div class="clear-fix mb-2"></div>

        <div class="media-body pb-0">
          <div class="float-left like-section" data-post="{{ $key }}" data-type="{{ $post['type'] }}">
            {!! $likeButton !!}
          </div>
        </div>
    </div>
  </div>
</div>
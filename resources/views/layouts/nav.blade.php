<div class="simplebar-mask">
    <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
        <div class="simplebar-content-wrapper" style="height: 100%; overflow: hidden;">
            <div class="simplebar-content" style="padding: 0px 0px 20px;">
                <a href="/profile" class="logo">
                <span>
                    <img src="@if(isset($profile['avatar'])){{ $profile['avatar'] }}@else /images/logo.png @endif" class="rounded-circle user-avatar" alt="" style="width: 100%; padding:10px;">
                </span>
                </a>
                <nav class="nav flex-column" id="two-col-sidenav-main">
                    <a class="nav-link" href="/home" data-toggle="tooltip" data-placement="right" title="" data-trigger="hover" data-original-title="Discover">
                        <i class="mdi mdi-compass-outline  font-22 my-1 text-white"></i>
                        <!-- <notify class="badge badge-danger rounded-circle">10</notify> -->
                    </a>
                    <a class="nav-link" href="/messages" data-toggle="tooltip" data-placement="right" title="" data-trigger="hover" data-original-title="Direct Messages">
                        <i class="mdi mdi-message-text  font-22 my-1 text-white"></i>
                    </a>
                    <a class="nav-link" href="/likes" data-toggle="tooltip" data-placement="right" title="" data-trigger="hover" data-original-title="Likes">
                        <i class="mdi mdi-paw  font-22 my-1 text-white"></i>
                        <!-- <notify class="badge badge-danger rounded-circle">10</notify> -->
                    </a>
                    <!--
                    <a class="nav-link" href="{{ route('lab') }}" data-toggle="tooltip" data-placement="right" title="" data-trigger="hover" data-original-title="Lab">
                        <i class="mdi mdi-flask  font-22 my-1 text-white"></i>
                        notify class="badge badge-danger rounded-circle">1</notify> 
                    </a>
                -->
                </nav>
            </div>
        </div>
    </div>
</div>
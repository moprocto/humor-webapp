<!-- Footer Start -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                {{ Date('Y') }} <!--  - <script>document.write(new Date().getFullYear())</script> --> &copy; humor
            </div>
            <div class="col-md-6">
                <div class="text-md-right footer-links d-none d-sm-block">
                    <a href="/logout">Logout</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end Footer -->
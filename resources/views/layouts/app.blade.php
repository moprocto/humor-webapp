<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="/images/logo.png">
        <link rel="apple-touch-icon" href="/images/logo.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/images/logo.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/images/logo.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/images/logo.png">
        <!-- CSRF Token -->
        <meta id="CSRF" name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Plugins css -->
            <link href="/assets/libs/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
            <link href="/assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />
            
            <!-- App css -->
            <link href="/assets/css/bootstrap-saas.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
            <link href="/assets/css/app-saas.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

            <link href="/assets/css/bootstrap-saas-dark.min.css" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled />
            <link href="/assets/css/app-saas-dark.min.css" rel="stylesheet" type="text/css" id="app-dark-stylesheet"  disabled />

            <!-- icons -->
            <link href="/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
            <style type="text/css">
                            @media (min-width: 600px){
                    .hidden-sm-up{
                        display: none !important;
                    }
                }
            </style>
        @yield('css')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="/images/logo-black.png" width="75">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ $user->displayName }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('firebase-logout') }}"
                                       onclick="">
                                        {{ __('Logout') }}
                                    </a>
                                </div>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <!-- Vendor js -->
        <script src="/assets/js/vendor.min.js"></script>

        <!-- Plugins js-->
        <script src="/assets/libs/flatpickr/flatpickr.min.js"></script>


        <script src="/assets/libs/selectize/js/standalone/selectize.min.js"></script>


        <!-- App js-->
        <script src="/assets/js/app.min.js"></script>
        <script src="/assets/libs/lazyload/lazyload.min.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163662190-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-163662190-1');
        </script>
                @yield('js')
</body>
</html>

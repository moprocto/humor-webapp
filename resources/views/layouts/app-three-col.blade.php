<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>humor</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="humor is where you find something more with the people you connect with" name="description" />
        <meta content="humor" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="/images/logo.png">
        <link rel="apple-touch-icon" href="/images/logo-bw.jpg">
        <link rel="apple-touch-icon" sizes="76x76" href="/images/logo-bw.jpg">
        <link rel="apple-touch-icon" sizes="120x120" href="/images/logo-bw.jpg">
        <link rel="apple-touch-icon" sizes="152x152" href="/images/logo-bw.jpg">
        <meta id="CSRF" content="{{ CSRF_TOKEN() }}">
        <meta id="uid" content="{{ $user->uid }}">

        <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
        <meta property="og:site_name" content="humor"/> <!-- website name -->
        <meta property="og:site" content="https://humorapp.co"/> <!-- website link -->
        <meta property="og:title" content="humor: laughter is in the air"/> <!-- title shown in the actual shared post -->
        <meta property="og:description" content="humor is where you find something more with the people you connect with"/> <!-- description shown in the actual shared post -->
        <meta property="og:image" content="http://humorapp.co/images/hero-bg-3a.jpg"/> <!-- image link, make sure it's jpg -->
        <meta property="og:url" content="https://humorapp.co"/> <!-- where do you want your post to link to -->
        <meta property="og:type" content="website"/>

        
        <link href="/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/jquery-toast-plugin/jquery.toast.min.css" rel="stylesheet" type="text/css" />
        <!-- App css -->
        <link href="/assets/css/bootstrap-saas.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
        <link href="/assets/css/app-saas.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

        <!-- icons -->
        <link href="/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/featherlight/featherlight.css" rel="stylesheet" type="text/css" />
        
        <style type="text/css">
            body[data-layout-mode=two-column] .sidebar-main-menu{
                width: 280px;
            }
            .jq-toast-wrap{
                cursor: pointer;
            }
            @media (min-width: 992px){
                body[data-layout-mode=two-column] .content-page{
                    margin-left: calc(70px + 280px);
                }
                body[data-layout-mode=two-column] .footer {
                    left: calc(70px + 280px);
                }
                body[data-layout-mode=two-column] .navbar-custom .logo-box {
                    width: 280px;
                }
                body[data-layout-mode=two-column] .sidebar-main-menu .nav .nav-link {
                    padding: 15px;
                }
                .featherlight-image {
                    min-width: 600px;
                }
                body[data-topbar-color=light] .navbar-custom{
                    background-color: #eff0f2 !important;
                    z-index: 0;
                }
                .topnav-menu{
                    display: none;
                }
            }
            body[data-layout-mode=two-column] .sidebar-icon-menu {
                /* background-color: #7202bb; */
                background-color: #6332f6;
            }
            notify{
                position: absolute;
                right:15;
                width: 20px;
                height: 20px;
                text-align: center;
                padding-top: 4px;
                
            }
            @media only screen and (max-width: 960px) {
              .topnav-menu a.active{
                color: #6332f6 !important;
              }
              .logo-box{
                    margin-left:20px;
                }
                .logo-box img{
                    height: 52px;
                }
            }
        </style>
        @yield('css')

    </head>

    <body class="loading" data-layout-mode="two-column" data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "light", "size": "default", "showuser": false}, "topbar": {"color": "light"}, "showRightSidebarOnPageLoad": false}'>
        <!-- Begin page -->
        <div id="wrapper" >
            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <!-- LOGO -->
                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="">
                            <a class="nav-link" href="/home">
                                <i class="mdi mdi-compass-outline  font-22 my-1 "></i>
                                <!-- <notify class="badge badge-danger rounded-circle">10</notify> -->
                            </a>
                        </li>
                        <li class="">
                            <a class="nav-link @if($page == 'home') active @endif" href="/messages">
                                <i class="mdi mdi-message-text  font-22 my-1"></i>
                            </a>
                        </li>
                        <li class="">
                            <a class="nav-link @if($page == 'likes') active @endif" href="/likes">
                                <i class="mdi mdi-paw  font-22 my-1 "></i>
                                <!-- <notify class="badge badge-danger rounded-circle">10</notify> -->
                            </a>
                        </li>
    
                        <li class="">
                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="@if(isset($profile['avatar'])){{ $profile['avatar'] }}@else /images/logo.png @endif" class="rounded-circle user-avatar">
                                <span class="pro-user-name ml-1">
                                    <i class="mdi mdi-chevron-down"></i> 
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
    
                                <!-- item-->
                                <a href="/profile" class="dropdown-item notify-item">
                                    <i class="fe-user"></i>
                                    <span>My Profile</span>
                                </a>
    
                                <div class="dropdown-divider"></div>
    
                                <!-- item-->
                                <a href="/logout" class="dropdown-item notify-item">
                                    <i class="fe-log-out"></i>
                                    <span>Logout</span>
                                </a>
    
                            </div>
                        </li>
    
                    </ul>

                    <div class="logo-box">
                        <a href="/home" class="logo logo-dark text-center">
                            <span class="logo-sm">
                                <img src="/images/logo-black.png" alt="" height="62">
                                <!-- <span class="logo-lg-text-light">UBold</span> -->
                            </span>
                            <span class="logo-lg">
                                <img src="/images/logo-black.png" alt="" height="60">
                                <!-- <span class="logo-lg-text-light">U</span> -->
                            </span>
                        </a>
    
                        <a href="/home" class="logo logo-light text-center">
                            <span class="logo-sm">
                                <img src="/images/logo-black.png" alt="" height="62">
                            </span>
                            <span class="logo-lg">
                                <img src="/images/logo-black.png" alt="" height="60">
                            </span>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="h-100" >

                    <div class="sidebar-content">
                        <div class="sidebar-icon-menu h-100" data-simplebar>
                            @include('layouts.nav')
                        </div>
                        @if($page == "home")
                        <!--- Sidemenu -->

                        <div class="sidebar-main-menu">
                            <h5 class="menu-title" style="color:#6332f6 !important;">Direct Messages</h5>
                            <div id="two-col-menu" class="h-100" data-simplebar>
                                <div class="twocolumn-menu-item d-block" id="dashboard">
                                    <div class="title-box">
                                        <div class="container-fluid" style="padding:10px;" id="matches">
                                            <div class="d-flex justify-content-center">
                                                <div class="spinner-border text-blue" role="status"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <!-- End Sidebar -->

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->

            <!-- ============================================================== -->

            <div class="content-page" style="overflow-y: hidden; padding-bottom: 0;">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">

                        @yield('content')
                        
                    </div> <!-- container -->

                </div> <!-- content -->

                @include('layouts.footer')

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->


        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="/assets/js/vendor.min.js"></script>

        <!-- Sweet Alerts js -->
        <script src="/assets/libs/sweetalert2/sweetalert2.min.js"></script>

        <!-- Tost-->
        <script src="/assets/libs/jquery-toast-plugin/jquery.toast.min.js"></script>
        <!-- toastr init js-->
        <script src="/assets/js/pages/toastr.init.js"></script>

        @include('layouts.alerts.manager')

        <!-- App js -->
        <script src="/assets/js/app.min.js"></script>
        <script src="/assets/libs/lazyload/lazyload.min.js"></script>
        <script src="/assets/libs/featherlight/featherlight.js"></script>

        <script src="/js/scripts.js"></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163662190-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-163662190-1');
        </script>
        @yield('js')
    </body>
</html>
@extends('layouts.app-one-col')

@section('css')
<link href="/assets/libs/toggle-switch/switch--radio-buttons.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		#lab .btn-label{
			float:left;
		}
		#lab .button-list{
			margin-bottom:10px;
		}
		button[disabled]{
			cursor: inherit !important;
		}
            .input_hidden {
                position: absolute;
                left: -9999px;
            }

            .selected {
                background-color: rgba(0,255,0, 0.7);
            }

            .buttons  label {
                display: inline-block;
                cursor: pointer;
                text-align: center;
            }
            .buttons label:hover {
                background-color: rgba(0,255,0, 0.2);
            }

            .buttons  label img {
                padding: 3px;
                width: 40px;
            }
            .content-page{
                margin-left:0 !important;
            }
            @media only screen and (max-width: 500px) {
                .col-sm-4{
                    width: 32% !important;
                }
                .content-page{
                    padding-top:0 !important;
                    margin-top:0 !important;
                }
                .page-title img{
                    max-width: 100px !important;
                }
                .col-xs-4 img{
                    max-width: 40px;
                }
                .quiz-image{
                    width: 100%;
                    min-height: auto !important;
                }
            }
            li.disabled a{
                display: none;
            }
        </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-9">
    	<div class="row">
    		<div class="col-md-6 offset-md-3">
		    	<div class="card">
		    		@if($results->count() < 30)
                        @include('layouts.quiz-form')
                    @endif
		    	</div>
	    	</div>
	    </div>
    </div>
    <div class="col-md-3" id="lab">
    	<div class="card">
    		<div class="card-body text-center">
    			<img src="/images/logo-lab.png" style="width: 100%; max-width: 200px; margin:0 auto;">
    			<h5>Help humor improve your recommendations!</h5>
    			<br>
		        <div class="button-list">
		            <button type="button" class="btn @if($results->isEmpty()) btn-soft-secondary @else btn-success @endif waves-effect waves-light btn-block" onclick="window.location.href='/discover'" @if($results->count() > 30) disabled @endif>
		                <span class="btn-label"><i class="mdi @if($results->count() < 30) mdi-clock-outline @else mdi-check-bold @endif"></i></span>Humor Quiz: <span id="quiz-progress-percent">{{ round($results->count()/30 * 100,0) }}</span>% Complete
		            </button>
		        </div>
		        <div class="button-list">
		            <button type="button" class="btn @if($profile->avatar == '/images/logo.png') btn-soft-secondary @else btn-success @endif  waves-effect waves-light btn-block" @if($profile->avatar != '/images/logo.png') disabled @endif onclick="window.location.href='/profile#photos'">
		                <span class="btn-label"><i class="mdi @if($profile->avatar == '/images/logo.png') mdi-clock-outline @else mdi-check-bold @endif"></i></span>Upload Profile Picture
		            </button>
		        </div>
		        <div class="button-list">
		            <button type="button" class="btn @if($posts->isEmpty()) btn-soft-secondary @else btn-success @endif waves-effect waves-light btn-block" onclick="window.location.href='/discover'" @if($posts->isNotEmpty()) disabled @endif>
		                <span class="btn-label"><i class="mdi @if($posts->isEmpty()) mdi-clock-outline @else mdi-check-bold @endif"></i></span>Post Conversation Starter
		            </button>
		        </div>
	        </div>
    	</div>
    </div>
</div>
@endsection

@section('js')
<script src="/js/lab.js"></script>
<script src="/assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<!-- Init js-->
<script src="/assets/js/pages/form-wizard.init.js"></script>
<script>
	$('.buttons input:radio').each(function(){
	    $(this).addClass('input_hidden');
	});

	$('.buttons label').on("click", function() {
	    $(this).addClass('selected');
	    $(this).siblings().removeClass('selected');
	});

	$('.buttons .reset').on("click", function() {
	    var buttons = $(this).parent().parent().find("label");
	    buttons.each(function(){
	        $(this).removeClass('selected');
	    });
	});

	var image;

	$(".quiz-image").each(function(){
	    
	    image = $(this).attr("data-url");
	    $(this).delay(500).attr("src", image); 
	});

    $("body").on("click", ".score-meme", function(){
        scoreMeme($(this), 30);
    });
</script>
@endsection

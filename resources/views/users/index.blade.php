@extends('layouts.app-one-col')

@section('css')
    <link rel="stylesheet" type="text/css" href="/assets/libs/swiper/swiper.min.css">
    <style type="text/css">
        #dragula .dragula-profile-image:first-of-type .delete{
            display: none;
        }
        .sidebar-main-menu .card{
            padding:10px;
            width: 100%;
            border-radius: 3px;
            background: #ffffff;
            box-shadow: 0 2px 2px rgba(0,0,0,0.13), 0 2px 2px rgba(0,0,0,0.13);
        }
        .sidebar-main-menu .card:hover{
            color: #2892fd;
            background-color: rgba(40,146,253,.07);
            box-shadow: 0 2px 2px rgba(0,0,0,0.05), 0 2px 2px rgba(0,0,0,0.05);
            transform: 1s;
            cursor: pointer;
        }
        .avatar-xxl {
            height: 15rem;
            width: 15rem;
        }
        .pd-10{
            padding:10px;
            padding-top:0;
        }
        .conversation-list .odd .ctext-wrap{
            background: #6332f6;
            color:white;  
        }
        .conversation-list .odd .ctext-wrap:after {
            border-left-color: #6332f6;
            border-top-color: #6332f6;
        }
         #simple-dragula > div:first-child .card{
            border: 3px solid gold !important;
        }

        
        .dragula-profile-image img{
                border-radius: 10px;
            }
        .dragula-profile-image .delete{
            position: absolute;
            top: 5px;
            right: 17px;
            width: 40px;
            height: 40px;
            text-align: center;
            padding-top: 11px;
            border-radius: 50%;
            font-size: 20px;
            cursor: pointer;
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        }

        .dragula-profile-image .delete:hover{
            background: #f52837!important;
        }
        .dragula-profile-image img{
            cursor:move;
            box-shadow: 0 3px 3px rgba(0,0,0,0.16), 0 3px 3px rgba(0,0,0,0.23);
            margin-bottom:20px;
        }
        .cropper-container{
            padding-right:100px;
        }

        @media (max-width: 500px){
            .col-xs-6{
                width: 50% !important;
            }
        }
        @media (max-width: 990px){
            .order-first{
                margin-bottom:20px;
            }
            body.gu-unselectable{
                overflow: hidden !important;
            }
        }

</style>
    <link href="../assets/libs/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" integrity="sha256-jKV9n9bkk/CTP8zbtEtnKaKf+ehRovOYeKoyfthwbC8=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js" integrity="sha256-CgvH7sz3tHhkiVKh05kSUgG97YtzYNnWt6OXcmYzqHY=" crossorigin="anonymous"></script>

@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <form method="POST" action="{{ route('edit-profile') }}" role="form" enctype="multipart/form-data">
            @CSRF
            <input type="hidden" name="timezone" id="timezone">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title text-dark-50 mt-3"><i class="mdi mdi-account-circle mr-1"></i> Base: Tell us about you</h4>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label" for="userName">I was born on</label>
                                <div class="col-md-9">
                                    <input type="date" class="form-control" id="birthday" name="birthday" value="{{ $profile['birthday'] }}" required="">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label" for="password"> I identify as a</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="gender" required="">
                                        <option value="">Please Select</option>
                                        <option value="man" @if($profile->gender == 'man') selected="" @endif>Man</option>
                                        <option value="woman" @if($profile->gender == 'womman') selected="" @endif>Woman</option>
                                        <option value="neither" @if($profile->gender == 'neither') selected="" @endif>Neither</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label" for="zipcode"> My zip code is</label>
                                <div class="col-md-9">
                                    <input type="text" id="zipcode" name="zipcode" class="form-control" value="{{ $profile->zipcode }}" placeholder="99999" required="">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label" for="bio"> What others should know about me</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="bio" name="bio" placeholder="I'm a water sign so I like to keep things wavy" required="">{{ $profile->bio }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label" for="password"> I am looking for</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="lookingFor" required>
                                        <option value="relationship" @if($profile->lookingFor == "relationship") selected @endif>A committed relationship</option>
                                        <option value="casual" @if($profile->lookingFor == "casual") selected @endif>Something casual</option>
                                        <option value="platonic" @if($profile->lookingFor == "platonic") selected @endif>Platonic friends</option>
                                        <option value="unsure" @if($profile->lookingFor == "unsure") selected @endif>Not sure</option>
                                        <option value="marriage" @if($profile->lookingFor == "marriage") selected @endif>Marriage</option>
                                    </select>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title text-dark-50 mt-3"><i class="mdi mdi-cow"></i> Protein: The heavy stuff</h4>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label" for="school"> I attended</label>
                                <div class="col-md-9">
                                    <input type="text" id="school" name="school" class="form-control" value="{{ $profile['school'] }}" placeholder="Hogwarts">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label" for="jobTitle"> I earn my living as a</label>
                                <div class="col-md-9">
                                    <input type="text" id="jobTitle" name="jobTitle" class="form-control" value="{{ $profile->jobTitle }}" placeholder="Programming Wizard">
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label" for="showMe"> Show me</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="showMe">
                                        <option value="women" @if($profile->showMe == 'women') selected="" @endif>Women</option>
                                        <option value="men" @if($profile->showMe == 'men') selected="" @endif>Men</option>
                                        <option value="both" @if($profile->showMe == 'both') selected="" @endif>Everyone</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label" for="religion"> My belief system</label>
                                <div class="col-md-9">
                                    <select class="form-control dropdown" id="religion" name="religion" required="">
                                        <option value="">Select One</option>
                                        <option value="Christianity" @if($profile->religion == 'Christianity') selected="" @endif>Christianity</option>
                                        <option value="Buddhism" @if($profile->religion == 'Buddhism') selected="" @endif>Buddhism</option>
                                        <option value="Hinduism" @if($profile->religion == 'Hinduism') selected="" @endif>Hinduism</option>
                                        <option value="Islam" @if($profile->religion == 'Islam') selected="" @endif>Islam</option>
                                        <option value="Catholic" @if($profile->religion == 'Catholic') selected="" @endif>Catholic</option>
                                        <option value="Judaism" @if($profile->religion == 'Judaism') selected="" @endif>Judaism</option>
                                        <option value="Spiritual" @if($profile->religion == 'Spiritual') selected="" @endif>Spiritual</option>
                                        <option value="Sikhism" @if($profile->religion == 'Sikhism') selected="" @endif>Sikhism</option>
                                        <option value="Other" @if($profile->religion == 'Other') selected="" @endif>Other</option>
                                        <option value="Agnostic" @if($profile->religion == 'Agnostic') selected="" @endif>Agnostic</option>
                                        <option value="Atheist" @if($profile->religion == 'Atheist') selected="" @endif>Atheist</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row mb-3">
                                <label class="col-md-3 col-form-label" for="educationLevel">My education level</label>
                                <div class="col-md-9">
                                    <select class="form-control dropdown" id="educationLevel" name="educationLevel">
                                        <option value="high school" @if($profile->educationLevel == 'high school') selected="" @endif>High School</option>
                                        <option value="GED" @if($profile->educationLevel == 'GED') selected="" @endif>GED</option>
                                        <option value="Vocational" @if($profile->educationLevel == 'Vocational') selected="" @endif>Vocational/Trade</option>
                                        <option value="College" @if($profile->educationLevel == 'College') selected="" @endif>Some College</option>
                                        <option value="Bachelors" @if($profile->educationLevel == 'Bachelors') selected="" @endif>Bachelor's degree</option>
                                        <option value="Masters" @if($profile->educationLevel == 'Masters') selected="" @endif>Master's degree</option>
                                        <option value="Doctorate" @if($profile->educationLevel == 'Doctorate') selected="" @endif>Doctorate</option>
                                        <option value="Professional" @if($profile->educationLevel == 'Professional') selected="" @endif>Doctorate in Medical, Law, or Dental</option>
                                    </select>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title text-dark-50 mt-3"><i class="mdi mdi-one-up"></i> Toppings: Any flavor?</h4>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group row mb-3">
                                <div class="col-md-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>When it comes to</th>
                                                <th class="text-center">Never</th>
                                                <th class="text-center">Socially</th>
                                                <th class="text-center">Frequently</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Smoking, I do it</td>
                                                <td class="text-center"><input type="radio" name="smoking" value="0" @if($profile['smoking'] == 0) checked="" @endif></td>
                                                <td class="text-center"><input type="radio" name="smoking" value="1" @if($profile['smoking'] == 1) checked="" @endif></td>
                                                <td class="text-center"><input type="radio" name="smoking" value="2" @if($profile['smoking'] == 2) checked="" @endif></td>
                                            </tr>
                                            <tr>
                                                <td>Drinking, I do it</td>
                                                <td class="text-center"><input type="radio" name="drinking" value="0" @if($profile['drinking'] == 0) checked="" @endif></td>
                                                <td class="text-center"><input type="radio" name="drinking" value="1" @if($profile['drinking'] == 1) checked="" @endif></td>
                                                <td class="text-center"><input type="radio" name="drinking" value="2" @if($profile['drinking'] == 2) checked="" @endif></td>
                                            </tr>
                                            <tr>
                                                <td>Drugs, I do them</td>
                                                <td class="text-center"><input type="radio" name="drugs" value="0" @if($profile['drugs'] == 0) checked="" @endif></td>
                                                <td class="text-center"><input type="radio" name="drugs" value="1" @if($profile['drugs'] == 1) checked="" @endif></td>
                                                <td class="text-center"><input type="radio" name="drugs" value="2" @if($profile['drugs'] == 2) checked="" @endif></td>
                                            </tr>
                                            <tr>
                                                <td>Marijuana, I partake</td>
                                                <td class="text-center"><input type="radio" name="marijuana" value="0" @if($profile['marijuana'] == 0) checked="" @endif></td>
                                                <td class="text-center"><input type="radio" name="marijuana" value="1" @if($profile['marijuana'] == 1) checked="" @endif></td>
                                                <td class="text-center"><input type="radio" name="marijuana" value="2" @if($profile['marijuana'] == 2) checked="" @endif></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-sm-12 col-md-12 order-last order-sm-first col-md-last order-xl-last order-lg-first">
                    <div class="card">
                        <div class="card-body">
                            <input id="photoUploader" type="file" data-plugins="dropify" data-height="150" name="photos[]" data-allowed-file-extensions="png jpg jpeg" data-max-file-size-preview="3M" />
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-sm-12 col-md-12 order-first order-sm-last order-md-first order-xl-first order-lg-last">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-dark-50 mt-3" id="photos"><i class="mdi mdi-image mr-1"></i> Chips: Select your favorite photos of you. Drag to re-order!</h4>

                            <div class="row" id="dragula">
                                @foreach($photos as $key => $photo)
                                     <div class="col-md-3 col-sm-6 col-xs-6 dragula-profile-image" data-sequence="{{ $key }}" data-photo="{{ $photo['id'] }}"><img src="{{ $photo['mediaLink'] }}" data-featherlight="{{ $photo['mediaLink'] }}" style="width:100%;"><span class="badge badge-danger delete" data-image="{{ $photo['id'] }}"><i class="mdi mdi-close-thick"></i></span></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-success" value="Save Changes">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
 <!-- Center modal content -->
<div class="modal fade" id="cropperModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myCenterModalLabel">Edit Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-0 text-center">
                <img id="dropify-render-preview" style="width: 80%;">
            
                <br>

                <input type="checkbox" name="toDiscover" value="1" id="toDiscover"> Add to Discover
                <br>
                <button type="buttton" id="croppedUpload" class="btn btn-success" style="margin: 20px 0;"><i class="mdi mdi-progress-upload"></i> Upload</button  >
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('js')
<script src="/assets/libs/swiper/swiper.min.js"></script>
<script src="../assets/libs/dragula/dragula.min.js"></script>

<!-- Buttons init js-->
<script src="../assets/js/pages/dragula.init.js"></script>
<script src="../assets/libs/dropify/js/dropify.min.js"></script>

<script src="../assets/libs/dropzone/min/dropzone.min.js"></script>
<script src="../assets/js/pages/form-fileuploads.init.js"></script>

<script src="/js/profile.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.7/jstz.js"></script>
    <script>
        var tz = jstz.determine();
        $("#timezone").val(tz.name());
    </script>
@endsection


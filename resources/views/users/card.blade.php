@php
$avatar = "/images/logo.png";
if(isset($profile->bio)){
        	$bio = $profile["bio"];
        }
        if(isset($profile->jobTitle)){

        	$jobTitle = $profile["jobTitle"];
        }
$p = $photos->count();

$html = '<div class="swiper-container"><div class="swiper-wrapper">';

if($p > 1){
    $swiper = "swiper-pagination";
    
} else {
    $html = str_replace('class="swiper-container"', "style='padding-bottom:50px;'", $html);
    $swiper ="";
}
echo $html;

$p = 0;
if(isset($photos)){

	foreach ($photos as $key => $photo) {
		if($p == 0){
			$avatar = $profile->avatar;
			echo '<div class="swiper-slide"><img src="' . $avatar . '" style="width: 100%;" data-featherlight="' . $avatar . '"></div>';
		} else {

			echo '<div class="swiper-slide"><img src="' . $photos[$key]["mediaLink"] . ' " style="width: 100%;" data-featherlight="' . $photos[$key]["mediaLink"] . '" style="width: 100%;"></div>';
		}
		
		$p++;
	}
} else {
	echo '<div class="swiper-slide"><img src="/images/logo.png" style="width:200px; margin: 0 auto;"></div>';
}






$age = getAge($profile["birthday"]);
if($age == 0){
	$age = "";
} else {
	$age = ", " . getAge($profile["birthday"]);
}

echo '
        </div>
        <!-- Add Pagination -->
        <div class="' . $swiper . '"></div>
      </div>
    <h4 class="mb-0 mt-0">' . $profile->displayName . '' . $age . '</h4>
    <p class="text-muted">' . $jobTitle . '</p>

    <div class="text-left mt-3 pb-15">
        <h4 class="font-13 text-uppercase">About Me :</h4>
        <p class="text-muted font-13 mb-3">
            ' . $bio . '
        </p>
        ' . $details . '
';
    if(isset($profile->school)){

        	echo '<p class="mb-2 font-13"><strong>School :</strong> <span class="ml-2">' . $profile["school"] . '</span></p>';
        }
        
        if(isset($profile->lookingFor)){

        	echo'<p class="mb-2 font-13"><strong>Looking For :</strong><span class="ml-2">' . ucfirst($profile["lookingFor"]) . '</span></p>';
        }
@endphp
	<button type="button" class="btn btn-outline-danger waves-effect waves-light full-width mt-4 unmatch" data-user="{{ $profile->user_id }}" data-avatar="{{ $avatar }}" data-convo="{{ $convo }}" style="width: 100%;">Unmatch</button>
</div>



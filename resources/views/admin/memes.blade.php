@extends('layouts.ubold')

@section('css')
	<link href="/assets/libs/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
	<!-- Plugins css -->
    <link href="/assets/libs/mohithg-switchery/switchery.min.css" rel="stylesheet" type="text/css" />
    <!-- Plugins css -->
        
        <link href="/assets/libs/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
	<!-- Filter -->
    <div class="row">
        <div class="col-12">
            <div class="text-center filter-menu">
                <a href="javascript: void(0);" class="filter-menu-item active" data-rel="all">All ({{ $memes->count() }})</a>
                <a href="javascript: void(0);" class="filter-menu-item" data-rel="dark">Dark ({{ $memes->where('type', 'dark')->count() }})</a>
                <a href="javascript: void(0);" class="filter-menu-item" data-rel="physical">Physical ({{ $memes->where('type', 'physical')->count() }})</a>
                <a href="javascript: void(0);" class="filter-menu-item" data-rel="sexual">Sexual ({{ $memes->where('type', 'sexual')->count() }})</a>
                <a href="javascript: void(0);" class="filter-menu-item" data-rel="dark">Witty ({{ $memes->where('type', 'witty')->count() }})</a>
                <a href="javascript: void(0);" class="filter-menu-item" data-rel="physical">Weird ({{ $memes->where('type', 'weird')->count() }})</a>
                <a href="javascript: void(0);" class="filter-menu-item" data-rel="sexual">Contextual ({{ $memes->where('type', 'contextual')->count() }})</a>
            </div>
        </div>
    </div>
    <!-- end row-->
    <table class="table">
        <thead>
            <tr>
                <th>Meme</th>
                <th>Category</th>
                <th>Views ({{ \App\Result::count() }})</th>
                <th>Score</th>
                <th>Funny %</th>
                <th>Is Active</th>
            </tr>
        </thead>
        <tbody>
            @foreach($memes as $meme)
            
            <tr class="filter-item all {{ $meme->type }}">
                <td><a href="{{ env('HUMOR_IMG_URL') . $meme->file }}" target="_blank" class="image-popup" title="Screenshot-1">
                    <img src="{{ env('HUMOR_IMG_URL') . $meme->file }}"  class="img-fluid" alt="work-thumbnail" width="50px">
                </a></td>
                <td>{{ $meme->type }}</td>
                <td>{{ \App\Result::where('meme_id', $meme->_id)->count() }}</td>
                <td><a href='/memes/{{ str_replace("item_", "", $meme->_id) }}'>{{ memeScore(str_replace("item_", "", $meme->_id)) }}</a></td>
                <td>@if(\App\Result::where('meme_id', $meme->_id)->count() > 0) {{ number_format(memeScore(str_replace("item_", "", $meme->_id)) / (\App\Result::where('meme_id', $meme->_id)->count()) * 100, 1) }}% @endif</td>
                <td>@if($meme->active == 1) Yes @else No @endif</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row filterable-content">
    	
    </div>
    <!-- end row -->
@endsection

@section('js')
	<script src="/assets/libs/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="/assets/js/pages/gallery.init.js"></script>
@endsection
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta description -->
    <meta name="description"
          content="humor is where you find something more with the people you connect with" name="description">
    <meta name="author" content="humor">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
        <meta property="og:site_name" content="humor"/> <!-- website name -->
        <meta property="og:site" content="https://humorapp.co"/> <!-- website link -->
        <meta property="og:title" content="humor: laughter is in the air"/> <!-- title shown in the actual shared post -->
        <meta property="og:description" content="humor is where you find something more with the people you connect with"/> <!-- description shown in the actual shared post -->
        <meta property="og:image" content="http://humorapp.co/images/hero-bg-3a.jpg"/> <!-- image link, make sure it's jpg -->
        <meta property="og:url" content="https://humorapp.co"/> <!-- where do you want your post to link to -->
        <meta property="og:type" content="website"/>

    <!--title-->
    <title>humor: laughter is in the air</title>

    <!--favicon icon-->
    <link rel="icon" href="images/logo.png" type="image/png">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans&display=swap"
          rel="stylesheet">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="stylesheets/bootstrap.min.css">
    <!--Magnific popup css-->
    <link rel="stylesheet" href="stylesheets/magnific-popup.css">
    <!--Themify icon css-->
    <link rel="stylesheet" href="stylesheets/themify-icons.css">
    <!--animated css-->
    <link rel="stylesheet" href="stylesheets/animate.min.css">
    <!--ytplayer css-->
    <link rel="stylesheet" href="stylesheets/jquery.mb.YTPlayer.min.css">
    <!--Owl carousel css-->
    <link rel="stylesheet" href="stylesheets/owl.carousel.min.css">
    <link rel="stylesheet" href="stylesheets/owl.theme.default.min.css">
    <!--custom css-->
    <link rel="stylesheet" href="stylesheets/style.css">
    <!--responsive css-->
    <link rel="stylesheet" href="stylesheets/responsive.css">
    <style>
    .counter-number {
  font-size: 32px;
  font-weight: 700;
  text-align: center;
}
.coming-box{
  float: left;
  width: 18%;
  text-align: center;
}

.counter-number span {
  font-size: 15px;
  font-weight: 400;
  display: block;
  text-align: center;
}
        th tr td{
            border: none;
        }
        th.highlight{
            color:white;
            background: #7200bb;
        }
        td.highlight{
            background: whitesmoke;
        }
        .table thead th,.table td, .table th{
            border:none;
        }
        #top-container{
            transition: 2s;
        }
        #PopupSignupForm_0{
        
        }
       @media only screen and (max-width: 600px) {
        .hero-content-left{
            margin-top:70px !important;
        }
        .navbar-toggler{
        display:none !important;
        }
        }
    </style>
</head>
<body>


<!--loader start-->
<div id="preloader">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!--loader end-->

<!--header section start-->
    
<!--header section end-->


<!--body content wrap start-->
<div class="main">

    <!--client section start-->
    <section  class="client-section ptb-100 gray-light-bg" id="signup">
        <div class="container">
            <!--clients logo start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading text-left mb-5">
                        <h1>Support</h1>
                        <div>
							<p>We are here to help! For all inquiries, please send us an email at <a href="mailto:hello@humorapp.co">hello@humorapp.co</a></p>
						</div>
                    </div>
                </div>
            </div>
            <!--clients logo end-->
        </div>
    </section>
    <!--client section start-->
</div>
<!--body content wrap end-->

<!--footer section end-->

<!--bottom to top button start-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="ti-angle-up"></span>
</button>
<!--bottom to top button end-->


<!--jQuery-->
<script src="scripts/jquery-3.4.1.min.js"></script>
<!--Popper js-->
<script src="scripts/popper.min.js"></script>
<!--Bootstrap js-->
<script src="scripts/bootstrap.min.js"></script>
<!--Magnific popup js-->
<script src="scripts/jquery.magnific-popup.min.js"></script>
<!--jquery easing js-->
<script src="scripts/jquery.easing.min.js"></script>
<!--wow js-->
<script src="scripts/wow.min.js"></script>
<!--owl carousel js-->
<script src="scripts/owl.carousel.min.js"></script>
<!--countdown js-->
<script src="scripts/jquery.countdown.min.js"></script>
<!--custom js-->
<script src="scripts/scripts.js"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163662190-1');
</script>
</body>
</html>
@extends('layouts.app-three-col')

@section('css')
    <link rel="stylesheet" type="text/css" href="/assets/libs/swiper/swiper.min.css">
    <style type="text/css">
      .swal2-popup .swal2-styled.swal2-confirm{
      background: #f75964;


    }
    .swal2-popup .swal2-styled.swal2-confirm:hover{
      background: #f52837!important;
    }
        .sidebar-main-menu .card{
            padding:10px;
            width: 100%;
            border-radius: 3px;
            background: #ffffff;
            box-shadow: 0 2px 2px rgba(0,0,0,0.13), 0 2px 2px rgba(0,0,0,0.13);
        }
        .sidebar-main-menu .card:hover, .sidebar-main-menu .card.active{
            color: #2892fd;
            background-color: rgba(40,146,253,.07);
            box-shadow: 0 2px 2px rgba(0,0,0,0.05), 0 2px 2px rgba(0,0,0,0.05);
            transform: 1s;
            cursor: pointer;
        }
        .avatar-xxl {
            height: 15rem;
            width: 15rem;
        }
        .swiper-container {
          width: 100%;
          height: 100%;
          border-radius: .25rem;
        }

        .swiper-slide {
          text-align: center;
          font-size: 18px;
          background: #fff;

          /* Center slide text vertically */
          display: -webkit-box;
          display: -ms-flexbox;
          display: -webkit-flex;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          -webkit-justify-content: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          -webkit-align-items: center;
          align-items: center;
        }
        .swiper-pagination {
            margin: 0 auto;
            padding-top: 30px;
            margin-bottom: 18px;
            position: relative;
            margin-left: 0;
        }
        .pb-15{
            padding:15px;
            padding-top:0;
        }
        .conversation-list .odd .ctext-wrap{
            background: #6332f6;
            color:white;  
        }
        .conversation-list .odd .ctext-wrap:after {
            border-left-color: #6332f6;
            border-top-color: #6332f6;
        }
        .mt-3{
          margin-top:99px;
        }
        .nav-pills .nav-link.active{
          background-color:rgba(99, 50, 246, 0.1) !important;
          box-shadow: 0 2px 2px rgba(99, 50, 246,0.15), 0 2px 2px rgba(99, 50, 246,0.15);
          
        }
        .nav-pills .nav-link:hover{
          
          box-shadow: 0 2px 2px rgba(99, 50, 246,0.13), 0 2px 2px rgba(99, 50, 246,0.13);
        }

        .nav-pills .nav-link.active *{
          color: #6332f6 !important;
        }
        #main-chat-window, #profile-card, footer{
          display:none;
        }

@media (min-width: 990px){
  #profile-screen{
    display: block !important;
  }
  .tab-content{
    display: flex;
    padding-top:0;
  }
  .tab-pane{
    padding-left:0;
  }
  #profile-screen{
    padding-right:0;
  }
  #user-bar{
    display: none;
  }
}
 @media (max-width: 990px){

  .tab-content{
    padding: 0;
  }
  .tab-pane{
    padding-left:0;
    padding-right:0;
  }

  .flex-column {
      flex-direction: inherit !important;
  }
  .nav-pills{
  flex-direction: none !important
  }
  body .left-side-menu {
    display: block !important;
    position: relative;
    bottom: inherit;
    margin: 0 20px;
    width: calc(100% - 42px) !important;
  }
  .sidebar-icon-menu{
    display: none;
  }
  #wrapper{
    overflow: initial !important;
  }
  body[data-layout-mode=two-column] .sidebar-main-menu {

    position: relative;
    top: inherit;
    left: inherit;
    bottom: inherit;
    padding:0;
  }
  .match-row{
    width: 100%;
    display: block;
    padding:8px !important;
  }
  .match-row .position-relative{
    text-align: center
  }
  .sidebar-main-menu{
    width: 100% !important
  }
  .match-row .media-mobile{
    display: inherit;
  }
  .chat-mobile-menu{
    display: flex;
  }
  .lh-2{
    line-height: 2.4;
    height: 30px;
    overflow-y: hidden;
  }
  .content-page{
    display: none;
    position: absolute;
    top:-5px;
    z-index: 50;
    padding:0;
    width: 100%;
  }
  .keyboard{
    position: fixed;
    bottom:0px;
    left:0;
    width: 100%;
    margin:0;
    z-index: 1000;
  }
  .keyboard, .keyboard .col{
    padding: 0 !important;
  }
  .keyboard .p-3{
    padding: 0 1.5rem !important;
    padding-top: 0.9rem !important;
    margin-top: 0 !important;
  }
  .keyboard .col-sm-10{
    width: 80% !important;
    flex-grow: unset;
    flex-basis: unset;
  }
  .keyboard .col-sm-2{
    width: 20% !important;
    flex-grow: unset;
    flex-basis: unset;
  }
  #chat-user{
    padding-left:0 !important;
  }
  #chat-window{
    padding-bottom: 100px !important;
  }
}
</style>
@endsection

@section('content')
<div class="row justify-content-center" >
  <div class="card col-md-12" style="margin-bottom: 0" id="user-bar">
    <div class="card-body py-2 px-3 border-bottom border-light" id="chat-user">
    
    <div class="media py-1">
        <div class="mobile-back" id="chat-user-header">
          <i class="fe-chevron-left avatar-title text-dark" style="font-size:36px; padding-right: 10px;"></i>
        </div>
        <img src="" class="mr-2 rounded-circle chat-window-avatar" height="36" alt="">
        <div class="media-body">
            <h5 class="mt-2 mb-0 font-15">
                <p class="text-reset text-displayName"></p>
            </h5>
        </div>
        <div>
        <ul class="nav nav-tabs nav-bordered chat-mobile-menu">
            <li class="nav-item">
                <a href="#chat-screen" data-toggle="tab" aria-expanded="false" class="nav-link active" id="chat-screen-tab">
                    Chat
                </a>
            </li>
            <li class="nav-item">
                <a href="#profile-screen" data-toggle="tab" aria-expanded="true" class="nav-link" id="profile-screen-tab">
                    Profile
                </a>
            </li>
        </ul>
        </div>
    </div>
</div>
  </div>
  <div class="tab-content" style="width: 100%;">
    <div class="tab-pane active col-xl-8 col-lg-8" id="chat-screen">
      <div id="main-chat-window">
        <div class="card">
          @include('layouts.chat')           
        </div> <!-- end card-body -->
      </div> <!-- end card -->
    </div>
    <div class="tab-pane col-lg-4 col-xl-4" id="profile-screen">
          <div class="card-box text-center" style="padding:0;" id="profile-card">
              <div class="d-flex justify-content-center" style="padding:200px 0;">
                  <div class="spinner-border text-blue" role="status"></div>
              </div>
          </div> <!-- end card-box -->

    </div>
  </div>
</div>
    
</div>
@endsection

@section('js')
<script src="/assets/libs/swiper/swiper.min.js"></script>
<script src="/js/chat.js"></script>
@endsection

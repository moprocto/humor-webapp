<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta description -->
    <meta name="description"
          content="humor is where you find something more with the people you connect with" name="description">
    <meta name="author" content="humor">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
        <meta property="og:site_name" content="humor"/> <!-- website name -->
        <meta property="og:site" content="https://humorapp.co"/> <!-- website link -->
        <meta property="og:title" content="humor: laughter is in the air"/> <!-- title shown in the actual shared post -->
        <meta property="og:description" content="humor is where you find something more with the people you connect with"/> <!-- description shown in the actual shared post -->
        <meta property="og:image" content="http://humorapp.co/images/hero-bg-3a.jpg"/> <!-- image link, make sure it's jpg -->
        <meta property="og:url" content="https://humorapp.co"/> <!-- where do you want your post to link to -->
        <meta property="og:type" content="website"/>

    <!--title-->
    <title>humor: laughter is in the air</title>

    <!--favicon icon-->
    <link rel="icon" href="images/logo.png" type="image/png">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans&display=swap"
          rel="stylesheet">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="stylesheets/bootstrap.min.css">
    <!--Magnific popup css-->
    <link rel="stylesheet" href="stylesheets/magnific-popup.css">
    <!--Themify icon css-->
    <link rel="stylesheet" href="stylesheets/themify-icons.css">
    <!--animated css-->
    <link rel="stylesheet" href="stylesheets/animate.min.css">
    <!--ytplayer css-->
    <link rel="stylesheet" href="stylesheets/jquery.mb.YTPlayer.min.css">
    <!--Owl carousel css-->
    <link rel="stylesheet" href="stylesheets/owl.carousel.min.css">
    <link rel="stylesheet" href="stylesheets/owl.theme.default.min.css">
    <!--custom css-->
    <link rel="stylesheet" href="stylesheets/style.css">
    <!--responsive css-->
    <link rel="stylesheet" href="stylesheets/responsive.css">
    <style>
    .counter-number {
  font-size: 32px;
  font-weight: 700;
  text-align: center;
}
.coming-box{
  float: left;
  width: 18%;
  text-align: center;
}

.counter-number span {
  font-size: 15px;
  font-weight: 400;
  display: block;
  text-align: center;
}
        th tr td{
            border: none;
        }
        th.highlight{
            color:white;
            background: #7200bb;
        }
        td.highlight{
            background: whitesmoke;
        }
        .table thead th,.table td, .table th{
            border:none;
        }
        #top-container{
            transition: 2s;
        }
        #PopupSignupForm_0{
        
        }
       @media only screen and (max-width: 600px) {
        .hero-content-left{
            margin-top:70px !important;
        }
        .navbar-toggler{
        display:none !important;
        }
        }
    </style>
</head>
<body>


<!--loader start-->
<div id="preloader">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!--loader end-->

<!--header section start-->

<header class="header">
    <nav class="navbar navbar-expand-lg fixed-top bg-transparent">
        <div class="container">
            <a class="navbar-brand" href="index.html">
                <img src="images/logo-white.png" width="160" alt="logo" class="img-fluid"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ti-menu"></span>
            </button>
            <div class="collapse navbar-collapse h-auto" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto menu">
                    <li><a href="#contact" class="page-scroll">Home</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
    
<!--header section end-->


<!--body content wrap start-->
<div class="main">

    <!--client section start-->
    <section  class="client-section ptb-100 gray-light-bg" id="signup">
        <div class="container">
            <!--clients logo start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading text-left mb-5">
                        <h1>Terms of Use</h1>
                        <div>
<p>Effective on 5/23/2019</p>
<ol>
<li><strong>INTRODUCTION</strong></li>
</ol>
<p><strong>By accessing or using humor’s services, you agree to be bound by these terms, including our</strong> <strong>Privacy</strong> <strong>and</strong> Cookie Policies</strong><strong>, so it is important that you read this agreement carefully before you create an account. We may update the terms from time to time, so you should check this page regularly for updates.</strong></p>
<p>By accessing or using our Services on www.humor.co (the “Website”), the humor mobile application (the “App”), or any other platforms or services humor may offer (collectively, the “Service” or our “Services”), you agree to, and are bound by, these Terms of Use (the “Terms” or “Agreement”). This Agreement applies to anyone who accesses or uses our Services, regardless of registration or subscription status.</p>
<p>Your use of our Services is also subject to the Privacy, and any terms disclosed and agreed to by you when you purchase additional features, products, or services from humor (“Additional Terms Upon Purchase”), which are incorporated into this Agreement by reference. If you do not wish to be bound by this Agreement, do not use our Services.</p>
<p>We reserve the right to modify, amend, or change the Terms at any time. Notice of any material change will be posted on this page with an updated effective date. In certain circumstances, we may notify you of a change to the Terms via email or other means, as appropriate under the circumstances; however, you are responsible for regularly checking this page for notice of any changes. We agree that future changes will not be retroactive without your consent. Your continued use of our Services constitutes your acceptance of any change, and you will be legally bound by the updated Terms. If you do not accept a change to the terms, you should stop using our Services immediately.</p>
<ol start="2">
<li><strong>ACCOUNT ELIGIBILITY; YOUR RESPONSIBILITIES</strong></li>
</ol>
<p><strong><em>Before you create an account on humor, make sure you are eligible to use our Services.  This Section also details what you can and can’t do when using the Services, as well as the rights you grant humor.</em></strong></p>
<p><strong>By using our Services, you represent and warrant that:</strong></p>
<ol>
<li>You are at least 18 years old;</li>
<li>You are legally qualified to enter a binding contract with humor;</li>
<li>You are seeking a meaningful relationship.</li>
<li>You are not located in a country that is subject to a U.S. Government embargo, or that has been designated by the U.S. Government as a “terrorist supporting” country;</li>
<li>You are not on any list of individuals prohibited from conducting business with the United States;</li>
<li>You are not prohibited by law from using our services;</li>
<li>You have not have been convicted of or pled no contest to a felony or indictable offense (or crime of similar severity), a sex crime, or any crime involving violence;</li>
<li>You are not required to register as a sex offender with any state, federal or local sex offender registry;</li>
<li>You do not have more than one account on our Services; and</li>
<li>You have not previously been removed from our Services by us, unless you have our express written permission to create a new account.</li>
</ol>
<p>If at any time you cease to meet these requirements, you must immediately delete your account, and we retain the right to remove your access to our services without warning.</p>
<p><strong>You agree to:</strong></p>
<p>•    Comply with these Terms, and check this page from time to time to ensure you are aware of any change;</p>
<p>•    Comply with all applicable laws, including without limitation, privacy laws, intellectual property laws, anti-spam laws, and regulatory requirements;</p>
<p>•    Use the latest version of the Website and/or App;</p>
<p>•    Treat other users in a courteous and respectful manner, both on and off our Services and abide by our membership principles;</p>
<p>•    Be respectful when communicating with any of our customer care representatives or other employees;</p>
<p>•    Maintain a strong password and take reasonable measures to protect the security of your login information.</p>
<p>•    Present yourself respectfully and authentically by adding at least one photo that shows your face.</p>
<p><strong>You agree that you will not:</strong></p>
<p>•    Misrepresent your identity, age, current or previous positions, qualifications, or affiliations with a person or entity;</p>
<p>•    Use the Services in a way that damages the Services or prevents their use by other users;</p>
<p>•    Use our Services in a way to interfere with, disrupt or negatively affect the platform, the servers, or our Services’ networks;</p>
<p>•    Use our Services for any harmful, illegal, or nefarious purpose;</p>
<p>•    Harass, bully, stalk, intimidate, assault, defame, harm or otherwise mistreat any person;</p>
<p>•    Post or share Prohibited Content (see below);</p>
<p>•    Solicit passwords for any purpose, or personal identifying information for commercial or unlawful purposes from other users or disseminate another person’s personal information without his or her permission;</p>
<p>•    Solicit money or other items of value from another user, whether as a gift, loan, or form of compensation;</p>
<p>•    Use another user’s account;</p>
<p>•    Use our Services in relation to fraud, a pyramid scheme, or other similar practice; or</p>
<p>•    Violate the terms of the license granted to you by humor (see Section 6 below).</p>
<p>•    Disclose private or proprietary information that you do not have the right to disclose;</p>
<p>•    Copy, modify, transmit, distribute, or create any derivative works from, any Member Content or Our Content, or any copyrighted material, images, trademarks, trade names, service marks, or other intellectual property, content or proprietary information accessible through our Services without humor’s prior written consent;</p>
<p>•    Express or imply that any statements you make are endorsed by humor;</p>
<p>•    Use any robot, crawler, site search/retrieval application, proxy or other manual or automatic device, method or process to access, retrieve, index, “data mine,” or in any way reproduce or circumvent the navigational structure or presentation of our Services or its contents;</p>
<p>•    Upload viruses or other malicious code or otherwise compromise the security of our Services;</p>
<p>•    Forge headers or otherwise manipulate identifiers to disguise the origin of any information transmitted to or through our Services;</p>
<p>•    “Frame” or “mirror” any part of our Services without humor’s prior written authorization;</p>
<p>•    Use meta tags or code or other devices containing any reference to humor or the platform (or any trademark, trade name, service mark, logo or slogan of humor) to direct any person to any other website for any purpose;</p>
<p>•    Modify, adapt, sublicense, translate, sell, reverse engineer, decipher, decompile or otherwise disassemble any portion of our Services, or cause others to do so;</p>
<p>•    Use or develop any third-party applications that interact with our Services or Member Content or information without our written consent;</p>
<p>•    Use, access, or publish the humor application programming interface without our written consent;</p>
<p>•    Probe, scan or test the vulnerability of our Services or any system or network; or</p>
<p>•    Encourage, promote, or agree to engage in any activity that violates these Terms.</p>
<p><strong>Prohibited Content—humor prohibits uploading or sharing content that:</strong></p>
<p>•    Could reasonably be deemed to be offensive or to harass, upset, embarrass, alarm or annoy any other person;</p>
<p>•    Is obscene, pornographic, violent or otherwise may offend human dignity, or contains nudity;</p>
<p>•    Is abusive, insulting or threatening, discriminatory or that promotes or encourages racism, sexism, hatred or bigotry;</p>
<p>•    Encourages or facilitates any illegal activity including, without limitation, terrorism, inciting racial hatred or the submission of which in itself constitutes committing a criminal offense;</p>
<p>•    Is defamatory, libelous, or untrue;</p>
<p>•    Relates to commercial activities (including, without limitation, sales, competitions, promotions, and advertising, solicitation for services, “sugar daddy” or “sugar baby” relationships, links to other websites or premium line telephone numbers);</p>
<p>•    Involves the transmission of “junk” mail or “spam”;</p>
<p>•    Contains any spyware, adware, viruses, corrupt files, worm programs or other malicious code designed to interrupt, damage or limit the functionality of or disrupt any software, hardware, telecommunications, networks, servers or other equipment, Trojan horse or any other material designed to damage, interfere with, wrongly intercept or expropriate any data or personal information whether from humor or otherwise;</p>
<p>•    Infringes upon any third party’s rights (including, without limitation, intellectual property rights and privacy rights);</p>
<p>•    Was not written by you or was automatically generated, unless expressly authorized by humor;</p>
<p>•    Includes the image or likeness of another person without that person’s consent (or in the case of a minor, the minor’s parent or guardian), or is an image or likeness or a minor unaccompanied by the minor’s parent or guardian;</p>
<p>•    Is inconsistent with the intended use of the Services; or</p>
<p>•    May harm the reputation of humor or its affiliates.</p>
<p>The uploading or sharing of content that violates these terms (“Prohibited Content”) may result in the immediate suspension or termination of your account.</p>
<ol start="3">
<li><strong>CONTENT</strong></li>
</ol>
<p><strong><em>It is important that you understand your rights and responsibilities with regard to the content on our Services, including any content you provide or post.  You are expressly prohibited from posting inappropriate content.</em></strong></p>
<p>While using our Services, you will have access to: (i) content that you upload or provide while using our Services (“Your Content”); (ii) content that other users upload or provide while using our Services (“Member Content”); and (iii) content that humor provides on and through our Services (“Our Content”). In this agreement, “content” includes, without limitation, all text, images, video, audio, or other material on our Services, including information on users’ profiles and in direct messages between users.</p>
<p><strong>3a.  YOUR CONTENT</strong></p>
<p><strong><em>You are responsible for Your Content. Don’t share anything that you wouldn’t want others to see, that would violate this Agreement, or that may expose you or us to legal liability.</em></strong></p>
<p>You are solely responsible and liable for Your Content, and, therefore, you agree to indemnify, defend, release, and hold us harmless from any claims made in connection with Your Content.</p>
<p>You represent and warrant to us that the information you provide to us is accurate, including any information submitted through Facebook or other third-party sources (if applicable), and that you will update your account information as necessary to ensure its accuracy.</p>
<p>The content included on your individual profile should be relevant to the intended use of our Services. You may not display any personal contact or banking information, whether in relation to you or any other person (for example, names, home addresses or postcodes, telephone numbers, email addresses, URLs, credit/debit card or other banking details). If you choose to reveal any personal information about yourself to other users, you do so at your own risk. We encourage you to use caution in disclosing any personal information online.</p>
<p>Your individual profile will be visible to other people around the world, so be sure that you are comfortable sharing Your Content before you post. You acknowledge and agree that Your Content may be viewed by other users, and, notwithstanding these Terms, other users may share Your Content with third parties. By uploading Your Content, you represent and warrant to us that you have all necessary rights and licenses to do so and automatically grant us a license to use Your Content as provided under Section 7 below.</p>
<p>You understand and agree that we may monitor or review Your Content, and we have the right to remove, delete, edit, limit, or block or prevent access to any of Your Content at any time in our sole discretion. Furthermore, you understand agree that we have no obligation to display or review Your Content.</p>
<p><strong>3b.  MEMBER CONTENT</strong></p>
<p><strong><em>While you will have access to Member Content, it is not yours and you may not copy or use Member Content for any purpose except as contemplated by these Terms.</em></strong></p>
<p>Other users will also share content on our Services. Member Content belongs to the user who posted the content and is stored on our servers and displayed at the direction of that user.</p>
<p>You do not have any rights in relation to Member Content, and you may only use Member Content to the extent that your use is consistent with our Services’ purpose of allowing use to communicate with and meet one another. You may not copy the Member Content or use Member Content for commercial purposes, to spam, to harass, or to make unlawful threats. We reserve the right to terminate your account if you misuse Member Content.</p>
<p><strong>3c.  OUR CONTENT</strong></p>
<p><strong><em>humor owns all other content on our Services.</em></strong></p>
<p>Any other text, content, graphics, user interfaces, trademarks, logos, sounds, artwork, images, and other intellectual property appearing on our Services is owned, controlled or licensed by us and protected by copyright, trademark and other intellectual property law rights. All rights, title, and interest in and to Our Content remains with us at all times.</p>
<p>We grant you a limited license to access and use Our Content as provided under Section 6 below, and we reserve all other rights.</p>
<ol start="4">
<li><strong>INAPPROPRIATE CONTENT AND MISCONDUCT; REPORTING</strong></li>
</ol>
<p><strong><em>humor does not tolerate inappropriate content or behavior on our Services.</em></strong></p>
<p>We are committed to maintaining a positive and respectful humor community, and we do not tolerate any inappropriate content or misconduct, whether on or off of the Services. We encourage you to report any inappropriate Member Content or misconduct by other users. You can report a user directly by tapping the three dots in the top right of any profile and selecting “Report”. You may also email humor Customer Service at <a href="mailto:hello@humorapp.co?Subject=Hello!" target="_top">hello@humorapp.co</a>.</p>
<p>Member Content is subject to the terms and conditions of Sections 512(c) and/or 512(d) of the Digital Millennium Copyright Act 1998. To submit a complaint regarding Member Content that may constitute intellectual property infringement, see Section 12 (Digital Millennium Copyright Act) below.</p>
<ol start="5">
<li><strong>PRIVACY</strong></li>
</ol>
<p><strong><em>Privacy is important to us. We have a separate policy about it that you should read.</em></strong></p>
<p>For information about how humor and its affiliates collect, use, and share your personal data, please read our Privacy Policy. By using our Services, you agree that we may use your personal data in accordance with our Privacy Policy.</p>
<ol start="6">
<li><strong>RIGHTS YOU ARE GRANTED BY humor</strong></li>
</ol>
<p><strong><em>humor grants you the right to use and enjoy our Services, subject to these Terms.</em></strong></p>
<p>For as long as you comply with these Terms, humor grants you a personal, worldwide, royalty-free, non-assignable, non-exclusive, revocable, and non-sublicensable license to access and use our Services for purposes as intended by humor and permitted by these Terms and applicable laws.</p>
<ol start="7">
<li><strong>RIGHTS YOU GRANT humor</strong></li>
</ol>
<p><strong><em>You own all of the content you provide to humor, but you also grant us the right to use Your Content as provided in this Agreement.</em></strong></p>
<p>By creating an account, you grant to humor a worldwide, perpetual, transferable, sub-licensable, royalty-free right and license to host, store, use, copy, display, reproduce, adapt, edit, publish, translate, modify, and distribute Your Content, including any information you authorize us to access from Facebook or other third-party source (if applicable), in whole or in part, and in any format or medium currently known or developed in the future. humor’s license to Your Content shall be non-exclusive, except that humor’s license shall be exclusive with respect to derivative works created through use of our Services. For example, humor would have an exclusive license to screenshots of our Services that include Your Content.</p>
<p>In addition, so that humor can prevent the use of Your Content outside of our Services, you authorize humor to act on your behalf with respect to infringing uses of Your Content taken from our Services by other users or third parties. This expressly includes the authority, but not the obligation, to send notices pursuant to 17 U.S.C. § 512(c)(3) (i.e., DMCA Takedown Notices) on your behalf if Your Content is taken and used by third parties outside of our Services. humor is not obligated to take any action with regard to use of Your Content by other users or third parties. humor’s license to Your Content is subject to your rights under applicable law (for example, laws regarding personal data protection to the extent the content contains personal information as defined by those laws).</p>
<p>In consideration for humor allowing you to use our Services, you agree that we, our affiliates, and our third-party partners may place advertising on our Services. By submitting suggestions or feedback to humor regarding our Services, you agree that humor may use and share such feedback for any purpose without compensating you.</p>
<p>You agree that humor may access, preserve, and disclose your account information, including Your Content, if required to do so by law or upon a good faith belief that such access, preservation, or disclosure is reasonably necessary to: (i) comply with legal process; (ii) enforce these Terms; (iii) respond to claims that any content violates the rights of third parties; (iv) respond to your requests for customer service; or (v) protect the rights, property or personal safety of the Company or any other person.</p>
<ol start="8">
<li><strong>PURCHASES AND AUTOMATICALLY RENEWING SUBSCRIPTIONS</strong></li>
</ol>
<p><strong><em>You will have the opportunity to purchase products and services from humor. If you purchase a subscription, it will automatically renew—and you will be charged—until you cancel.</em></strong></p>
<p>humor may offer products and services for purchase through iTunes, Google Play or other external services authorized by humor (each, an “External Service,” and any purchases made thereon, an “External Service Purchase”). <span style="color:red;">If you purchase a subscription, it will automatically renew until you cancel, in accordance with the terms disclosed to you at the time of purchase, as further described below.</span> If you cancel your subscription, you will continue to have access to your subscription benefits until the end of your subscription period, at which point is will expire.</p>
<p>Because our Services may be utilized without a subscription, canceling your subscription does not remove your profile from our Services.  If you wish to fully terminate your membership, you must terminate your membership as set forth in Section 9.</p>
<p><strong>8a. EXTERNAL SERVICE PURCHASES AND SUBSCRIPTIONS</strong></p>
<p><strong><em>External Service Purchases, including subscriptions, may be processed through the External Service, in which case those purchases must be managed through your External Service Account.  Subscriptions automatically renew until you cancel.</em></strong></p>
<p>When making a purchase on the Service, you may have the option to pay through an External Service, such as with your Apple ID or Google account (“your External Service Account”), and your External Service Account will be charged for the purchase in accordance with the terms disclosed to you at the time of purchase and the general terms applicable to your External Service Account. Some External Services may charge you sales tax, depending on where you live, which may change from time to time.</p>
<p><span style="color:red"> If your External Service Purchase includes an automatically renewing subscription, your External Service Account will continue to be periodically charged for the subscription until you cancel. After your initial subscription commitment period, and again after any subsequent subscription period, the subscription will automatically continue for the price and time period you agreed to when subscribing.</span></p>
<p><span style="color:red">To cancel a subscription: If you do not want your subscription to renew automatically, or if you want to change or terminate your subscription, you must log in to your External Service Account and follow instructions to manage or cancel your subscription, even if you have otherwise deleted your account with us or if you have deleted the App from your device.  For example, if you subscribed using your Apple ID, cancellation is handled by Apple, not humor. To cancel a purchase made with your Apple ID, go to Settings &gt; iTunes &amp; App Stores &gt; [click on your Apple ID] &gt; View Apple ID &gt; Subscriptions, then find your humor subscription and follow the instructions to cancel. You can also request assistance at https://getsupport.apple.com. Similarly, if you subscribed on Google Play, cancellation is handled by Google.  To cancel a purchase made through Google Play, launch the Google Play app on your mobile device and go to Menu &gt; My Apps &gt; Subscriptions, then find your humor subscription and follow the instructions to cancel. You can also request assistance at https://play.google.com.  If you cancel a subscription, you may continue to use the cancelled service until the end of your then-current subscription term. The subscription will not be renewed when your then-current term expires.</span></p>
<p>If you initiate a chargeback or otherwise reverse a payment made with your External Service Account, humor may terminate your account immediately in its sole discretion. humor will retain all funds charged to your External Service Account until you cancel your subscription through your External Service Account. Certain users may be entitled to request a refund. See Section 8b below for more information.</p>
<p><strong>8b. REFUNDS</strong></p>
<p><strong><em>Generally, all purchases are nonrefundable. Special terms apply in Arizona, California, Connecticut, Illinois, Iowa, Minnesota, New York, North Carolina, Ohio, Rhode Island, Wisconsin, and the EU or European Economic Area.</em></strong></p>
<p>Generally, all purchases are final and nonrefundable, and there are no refunds or credits for partially used periods, except if the laws applicable in your jurisdiction provide for refunds.</p>
<p>For subscribers residing in the EU or European Economic Area:</p>
<p>In accordance with local law, you are entitled to a full refund during the 14 days after the subscription begins. Please note that this 14-day period commences when the subscription starts.</p>
<p>For subscribers residing in Arizona, California, Connecticut, Illinois, Iowa, Minnesota, New York, North Carolina, Ohio, Rhode Island, and Wisconsin:</p>
<p><strong><em>Your Right to Cancel—You may cancel your subscription, without penalty or obligation, at any time prior to midnight of the third business day following the date you subscribed.</em></strong> In the event that you die before the end of your subscription period, your estate shall be entitled to a refund of that portion of any payment you had made for your subscription which is allocable to the period after your death. In the event that you become disabled (such that you are unable to use our Services) before the end of your subscription period, you shall be entitled to a refund of that portion of any payment you had made for your subscription which is allocable to the period after your disability by providing the company notice in the same manner as you request a refund as described below.</p>
<p>If any of the above apply to you and you subscribed using your Apple ID, your refund requests are handled by Apple, not humor. To request a refund, please contact your External Service directly; for example using your Apple device, go to Settings &gt; iTunes &amp; App Stores &gt; [click on your Apple ID] &gt; View Apple ID &gt; Purchase History. Find the transaction and select “Report a Problem.” You can also request a refund at <a href="https://getsupport.apple.com">https://getsupport.apple.com</a>. For any other purchase, please contact humor Customer Service with your order number (see your confirmation email) by mailing or delivering a signed and dated notice which states that you, the buyer, are canceling this agreement, or words of similar effect. Please also include the email address or telephone number associated with your account along with your order number..</p>
<ol start="9">
<li><strong>ACCOUNT TERMINATION</strong></li>
</ol>
<p><strong><em>If you no longer wish to use our Services, or if we terminate your account for any reason, here’s what you need to know.</em></strong></p>
<p>You can delete your account at any time by logging into the App, going to the “Settings” tab, and selecting “Account” (the gear icon), and following the instructions to terminate your membership. <strong>However, you will need to cancel / manage any External Service Purchases through your External Service Account (e.g., iTunes, Google Play) to avoid additional billing.</strong></p>
<p>humor reserves the right to investigate and, if appropriate, suspend or terminate your account without a refund if you have violated these Terms, misused our Services, or behaved in a way that humor regards as inappropriate or unlawful, on or off our Services.  We reserve the right to make use of any personal, technological, legal, or other means available to enforce the Terms, at any time without liability and without the obligation to give you prior notice, including, but not limited to, preventing you from accessing the Services.</p>
<p>If your account is terminated by you or by humor for any reason, these Terms continue and remain enforceable between you and humor, and you will not be entitled to any refund for purchases made.  Your information will be maintained and deleted in accordance with our Privacy Policy.</p>
<ol start="10">
<li><strong>NO CRIMINAL BACKGROUND OR IDENTITY VERIFICATION CHECKS</strong></li>
</ol>
<p><em><strong>humor does not conduct criminal background or identity verification checks on its users. Use your best judgment when interacting with others and check out our</strong></em><em><strong>.</strong></em></p>
<p><strong>YOU UNDERSTAND THAT humor DOES NOT CONDUCT CRIMINAL BACKGROUND OR IDENTITY VERIFICATION CHECKS ON ITS USERS OR OTHERWISE INQUIRE INTO THE BACKGROUND OF ITS USERS.</strong> humor MAKES NO REPRESENTATIONS OR WARRANTIES AS TO THE CONDUCT, IDENTITY, INTENTIONS, LEGITIMACY, OR VERACITY OF USERS. humor RESERVES THE RIGHT TO CONDUCT—AND YOU AUTHORIZE humor TO CONDUCT—ANY CRIMINAL BACKGROUND CHECK OR OTHER SCREENINGS (SUCH AS SEX OFFENDER REGISTER SEARCHES) AT ANY TIME USING AVAILABLE PUBLIC RECORDS, AND YOU AGREE THAT ANY INFORMATION YOU PROVIDE MAY BE USED FOR THAT PURPOSE.  IF THE COMPANY DECIDES TO CONDUCT ANY SCREENING THROUGH A CONSUMER REPORTING AGENCY, YOU HEREBY AUTHORIZE THE COMPANY TO OBTAIN AND USE A CONSUMER REPORT ABOUT YOU TO DETERMINE YOUR ELIGIBILITY UNDER THESE TERMS.</p>
<p><strong>YOU ARE SOLELY RESPONSIBLE FOR YOUR INTERACTIONS WITH OTHER USERS.</strong> SEX OFFENDER SCREENINGS AND OTHER TOOLS DO NOT GUARANTEE YOUR SAFETY AND ARE NOT A SUBSTITUTE FOR FOLLOWING THE SAFETY TIPS AND OTHER SENSIBLE SAFETY PRECAUTIONS. ALWAYS USE YOUR BEST JUDGMENT AND TAKE APPROPRIATE SAFETY PRECAUTIONS WHEN COMMUNICATING WITH OR MEETING NEW PEOPLE.  COMMUNICATIONS RECEIVED THROUGH THE SERVICE, INCLUDING AUTOMATIC NOTIFICATIONS SENT BY humor, MAY RESULT FROM USERS ENGAGING WITH THE SERVICE FOR IMPROPER PURPOSES, INCLUDING FRAUD, ABUSE, HARASSMENT, OR OTHER SUCH IMPROPER BEHAVIOR.</p>
<p>Though humor strives to encourage a respectful user experience, it is not responsible for the conduct of any user on or off the Service. You agree to use caution in all interactions with other users, particularly if you decide to communicate off the Service or meet in person.</p>
<ol start="11">
<li><strong>DISCLAIMER</strong></li>
</ol>
<p><strong><em>humor’s Services are provided “as is” and we do not make, and cannot make, any representations about the content or features of our Services.</em></strong></p>
<p>humor PROVIDES OUR SERVICES ON AN “AS IS” AND “AS AVAILABLE” BASIS AND TO THE EXTENT PERMITTED BY APPLICABLE LAW, GRANTS NO WARRANTIES OF ANY KIND, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE WITH RESPECT TO OUR SERVICES (INCLUDING ALL CONTENT CONTAINED THEREIN), INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTIES OF SATISFACTORY QUALITY, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT. humor DOES NOT REPRESENT OR WARRANT THAT (A) OUR SERVICES WILL BE UNINTERRUPTED, SECURE, OR ERROR FREE, (B) ANY DEFECTS OR ERRORS IN OUR SERVICES WILL BE CORRECTED, OR (C) THAT ANY CONTENT OR INFORMATION YOU OBTAIN ON OR THROUGH OUR SERVICES WILL BE ACCURATE. FURTHERMORE, humor MAKES NO GUARANTEES AS TO THE NUMBER OF ACTIVE USERS AT ANY TIME; USERS’ ABILITY OR DESIRE TO COMMUNICATE WITH OR MEET YOU, OR THE ULTIMATE COMPATIBILITY WITH OR CONDUCT BY USERS YOU MEET THROUGH THE SERVICES.</p>
<p>humor TAKES NO RESPONSIBILITY FOR ANY CONTENT THAT YOU OR ANOTHER USER OR THIRD PARTY POSTS, SENDS, OR RECEIVES THROUGH OUR SERVICES NOR DOES humor TAKE ANY RESPONSIBILITY FOR THE IDENTITY, INTENTIONS, LEGITIMACY, OR VERACITY OF ANY USERS WITH WHOM YOU MAY COMMUNICATION THROUGH humor. ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF OUR SERVICES IS ACCESSED AT YOUR OWN DISCRETION AND RISK. humor IS NOT RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER HARDWARE, COMPUTER SOFTWARE, OR OTHER EQUIPMENT OR TECHNOLOGY INCLUDING, BUT WITHOUT LIMITATION, DAMAGE FROM ANY SECURITY BREACH OR FROM ANY VIRUS, BUGS, TAMPERING, FRAUD, ERROR, OMISSION, INTERRUPTION, DEFECT, DELAY IN OPERATION OR TRANSMISSION, COMPUTER LINE OR NETWORK FAILURE, OR ANY OTHER TECHNICAL OR OTHER MALFUNCTION.</p>
<ol start="12">
<li><strong>DIGITAL MILLENNIUM COPYRIGHT ACT</strong></li>
</ol>
<p><strong><em>We take copyright infringement very seriously. We ask you to help us to ensure we address it promptly and effectively.</em></strong></p>
<p>humor has adopted the following policy towards copyright infringement in accordance with the Digital Millennium Copyright Act (the “DMCA”). If you believe any Member Content or Our Content infringes upon your intellectual property rights, please submit a notification alleging such infringement (“DMCA Takedown Notice”) including the following:</p>
<ol>
<li>A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;</li>
<li>Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works;</li>
<li>Identification of the material claimed to be infringing or to be the subject of infringing activity and that is to be removed or access disabled and information reasonably sufficient to permit the service provider to locate the material;</li>
<li>Information reasonably sufficient to permit the service provider to contact you, such as an address, telephone number, and, if available, an electronic mail;</li>
<li>A statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and</li>
<li>A statement that, under penalty of perjury, the information in the notification is accurate and you are authorized to act on behalf of the owner of the exclusive right that is allegedly infringed.</li>
</ol>
<ol start="13">
<li><strong>LIMITATION OF LIABILITY.</strong></li>
</ol>
<p><strong><em>humor’s liability is limited to the maximum extent by applicable law.</em></strong></p>
<p>TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL humor, ITS AFFILIATES, EMPLOYEES, LICENSORS, OR SERVICE PROVIDERS BE LIABLE FOR ANY INDIRECT, CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING, WITHOUT LIMITATION, LOSS OF PROFITS, WHETHER INCURRED DIRECTLY OR INDIRECTLY, OR ANY LOSS OF DATA, USE, GOODWILL, OR OTHER INTANGIBLE LOSSES, RESULTING FROM: (I) YOUR ACCESS TO OR USE OF OR INABILITY TO ACCESS OR USE THE SERVICES, (II) THE CONDUCT OR CONTENT OF OTHER USERS OR THIRD PARTIES ON, THROUGH, OR FOLLOWING USE OF THE SERVICES; OR (III) UNAUTHORIZED ACCESS, USE, OR ALTERATION OF YOUR CONTENT, EVEN IF humor HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN NO EVENT WILL humor’S AGGREGATE LIABILITY TO YOU FOR ALL CLAIMS RELATING TO THE SERVICES EXCEED THE AMOUNT PAID, IF ANY, BY YOU TO humor FOR THE SERVICES WHILE YOU HAVE AN ACCOUNT.</p>
<p>THE LIMITATION OF LIABILITY PROVISIONS SET FORTH IN THIS SECTION 13 SHALL APPLY EVEN IF YOUR REMEDIES UNDER THIS AGREEMENT FAIL WITH RESPECT TO THEIR ESSENTIAL PURPOSE.</p>
<p>SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES, SO SOME OR ALL OF THE EXCLUSIONS AND LIMITATIONS IN THIS SECTION MAY NOT APPLY TO YOU.</p>
<ol start="14">
<li><strong>DISPUTE RESOLUTION</strong></li>
</ol>
<p><strong><em>In the unlikely event that we have a legal dispute, here is what you need to know.</em></strong></p>
<p>If you are dissatisfied with our Services for any reason, please contact humor Customer Service first so that we can try to resolve your concerns without the need of outside assistance. If you choose to pursue a claim against humor, these terms will apply.</p>
<p><strong>14a. ARBITRATION, CLASS-ACTION WAIVER, AND JURY WAIVER</strong></p>
<p><strong><em>If you pursue a legal claim against humor, you agree to arbitration (with limited exceptions).</em></strong></p>
<p>Except for users residing within the EU or European Economic Area, and where prohibited by law:</p>
<ol>
<li>xThe one exception to the exclusivity of arbitration is that either party has the right to bring an individual claim against the other in a small-claims court of competent jurisdiction, or, if filed in arbitration, the responding party may request that the dispute proceed in small claims court if the party’s claim is within the jurisdiction of the small claims court. If the responding party requests to proceed in small claims court before the appointment of the arbitrator, the arbitration shall be administratively closed, and if requested after the appointment of the arbitrator, the arbitrator shall determine if the dispute should be decided in arbitration or if the arbitration should be administratively closed and decided in small claims court. Whether you choose arbitration or small-claims court, you may not under any circumstances commence or maintain against the Company any class action, class arbitration, or other representative action or proceeding.</li>
<li>By using our Services in any manner, you agree to the above arbitration agreement. In doing so, YOU GIVE UP YOUR RIGHT TO GO TO COURT to assert or defend any claims between you and the Company (except for matters that may be taken to small-claims court). YOU ALSO GIVE UP YOUR RIGHT TO PARTICIPATE IN A CLASS ACTION OR OTHER CLASS PROCEEDING. Your rights will be determined by a NEUTRAL ARBITRATOR, NOT A JUDGE OR JURY, and the arbitrator shall determine all issues regarding the arbitrability of the dispute. You are entitled to a fair hearing before the arbitrator. The arbitrator can grant any relief that a court can, but you should note that arbitration proceedings are usually simpler and more streamlined than trials and other judicial proceedings. Decisions by the arbitrator are enforceable in court and may be overturned by a court only for very limited reasons.</li>
<li>Any proceeding to enforce this arbitration agreement, including any proceeding to confirm, modify, or vacate an arbitration award, may be commenced in any court of competent jurisdiction. In the event that this arbitration agreement is for any reason held to be unenforceable, any litigation against the Company (except for small-claims court actions) may be commenced only in the federal or state courts located in Dallas County, Texas. You hereby irrevocably consent to the jurisdiction of those courts for such purposes.</li>
<li>The online dispute settlement platform of the European Commission is available under <a href="http://ec.europa.eu/odr">http://ec.europa.eu/odr</a>. humor does not take part in dispute settlement procedures in front of a consumer arbitration entity for users residing in the EU or European Economic Area.</li>
</ol>
<p><strong>14b. GOVERNING LAW</strong></p>
<p><strong><em>Texas law and the Federal Arbitration Act will apply if there is a dispute (except in the EU and where prohibited by law).</em></strong></p>
<p>Except for users residing in the EU or European Economic Area or elsewhere where our arbitration agreement is prohibited by law, the laws of Texas, U.S.A., excluding Texas’s conflict of laws rules, will apply to any disputes arising out of or relating to this Agreement or our Services. Notwithstanding the foregoing, the Arbitration Agreement in Section 15a above shall be governed by the Federal Arbitration Act. For the avoidance of doubt, the choice of Texas governing law shall not supersede any mandatory consumer protection legislation in such jurisdictions.</p>
<p><strong>15c. VENUE</strong></p>
<p><strong><em>Any claims that are not submitted to arbitration for any reason must be litigated in Dallas County, Texas (except for claims brought in small claims court, and in the EU or where prohibited by law).</em></strong></p>
<p>Except for users residing in the EU or European Economic Area, who may bring claims in their country of residence in accordance with applicable law, and except for claims that may be properly brought in a small claims court of competent jurisdiction in the county or other jurisdiction in which you reside or in Dallas County, Texas, all claims arising out of or relating to this Agreement, to our Services, or to your relationship with humor that for whatever reason are not submitted to arbitration will be litigated exclusively in the federal or state courts of Dallas County, Texas, U.S.A. You and humor consent to the exercise of personal jurisdiction of courts in the State of Texas and waive any claim that such courts constitute an inconvenient forum.</p>
<ol start="15">
<li><strong>INDEMNITY BY YOU</strong></li>
</ol>
<p><strong><em>You agree to indemnify humor if a claim is made against humor due to your actions.</em></strong></p>
<p>You agree, to the extent permitted under applicable law, to indemnify, defend, and hold harmless humor, our affiliates, and their and our respective officers, directors, agents, and employees from and against any and all complaints, demands, claims, damages, losses, costs, liabilities, and expenses, including attorney’s fees, due to, arising out of, or relating in any way to your access to or use of our Services, Your Content, Your conduct toward other users, or your breach of this Agreement.</p>
<ol start="16">
<li><strong>ACCEPTANCE OF TERMS</strong></li>
</ol>
<p><strong><em>By using our Services, you accept the Terms of this Agreement.</em></strong></p>
<p>By using our Services, whether through a mobile device, mobile application, or computer, you agree to be bound by (i) these Terms, which we may amend from time to time, (ii) our Privacy Policy and (iii) any Additional Terms Upon Purchase. If you do not accept and agree to be bound by all of the terms of this Agreement, please do not use our Services.</p>
<p>The section headings and summaries contained herein are inserted for convenience only and shall not be considered in interpreting any term or provision hereof. All pronouns and any variations thereof shall be deemed to refer to the masculine, feminine, neuter, singular or plural as the identity of the entities or persons referred to any require. Any word both capitalized and uncapitalized will be deemed to have the same meaning.</p>
<ol start="17">
<li><strong>ENTIRE AGREEMENT</strong></li>
</ol>
<p><strong><em>This Agreement supersedes any previous agreements or representations.</em></strong></p>
<p>These Terms, with the Privacy Policy, Cookie Policy, and any Additional Terms Upon Purchase, contain the entire agreement between you and humor regarding the use of our Services. The Terms supersede all previous agreements, representations, and arrangements between us, written or oral. If any provision of these Terms is held invalid, illegal, or otherwise unenforceable, the remainder of the Terms shall continue in full force and effect. The failure of the Company to exercise or enforce any right or provision of these Terms shall not constitute a waiver of such right or provision. You agree that your humor account is non-transferable and all of your rights to your account and its content terminate upon your death, unless otherwise provided by law. Any rights and licenses granted hereunder, may not be transferred or assigned by you, but may be assigned by us without restriction. No agency, partnership, joint venture, fiduciary or other special relationship or employment is created as a result of these Terms, and you may not make any representations on behalf of or bind humor in any manner.</p>
<ol start="18">
<li><strong>SPECIAL STATE TERMS</strong></li>
</ol>
<p><strong><em>Special terms apply in Arizona, California, Connecticut, Illinois, Iowa, Minnesota, New York, North Carolina, Ohio, Rhode Island, Wisconsin</em></strong></p>
<p>For subscribers residing in New York:</p>
<p>•    The Services do not guarantee any number of “referrals”—rather, the functionality of the Services is such that the subscriber can view as many profiles as he/she would like;</p>
<p>•    Upon notice in writing and delivered to Match Group Legal, P.O. Box 25472, Dallas, Texas 75225, USA, subscribers may place their subscription on hold for up to one year;</p>
<p>•    How your information is used and how you may access your information is set forth in our Privacy Policy;</p>
<p>•    You may review the New York Dating Service Consumer Bill of Rights <a href="https://www.nysenate.gov/legislation/laws/GBS/394-C">here</a>;</p>
<p>For subscribers residing in North Carolina:</p>
<p>•    You may review the North Carolina Buyer’s Rights <a href="https://www.ncleg.gov/EnactedLegislation/Statutes/PDF/BySection/Chapter_66/GS_66-120.pdf">here</a>.</p>
<p>For subscribers residing in Illinois, New York, North Carolina, and Ohio :</p>
<p>•    Our Services are widely available in the United States—if you believe that you have moved outside a location where we provide the Services, please contact us in writing delivered to Match Group Legal, P.O. Box 25472, Dallas, Texas 75225, USA, and we will work with you to provide alternative services or a refund.</p>
<p>For subscribers residing in Arizona, California, Connecticut, Illinois, Iowa, Minnesota, New York, North Carolina, Ohio, Rhode Island, and Wisconsin:</p>
<p><strong>Your Right to Cancel—You may cancel your subscription, without penalty or obligation, at any time prior to midnight of the third business day following the date you subscribed</strong>. In the event that you die before the end of your subscription period, your estate shall be entitled to a refund of that portion of any payment you had made for your subscription which is allocable to the period after your death. In the event that you become disabled (such that you are unable to use our Services) before the end of your subscription period, you shall be entitled to a refund of that portion of any payment you had made for your subscription which is allocable to the period after your disability by providing the company notice in the same manner as you request a refund as described above in Section 8.</p>
</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading text-left mb-5">
                        <h1>Privacy Policy</h1>
                    </div>
<p>At Humor, your privacy is a top priority. Your privacy is at the core of the way we design and build the services and products you know and love, so that you can fully trust them and focus on building meaningful connections.</p>
<p>We appreciate that you put your trust in us when you provide us with your information and we do not take this lightly.</p>
<p>We do not compromise with your privacy. We design all of our products and services with your privacy in mind. We involve experts from various fields, including legal, security, engineering, product design and others to make sure that no decision is taken without respect for your privacy.</p>
<p>We strive to be transparent in the way we process your data. Because we use many of the same online services you do, we know that insufficient information and overly complicated language are common issues in privacy policies. We take the exact opposite approach: we have written our Privacy Policy and related documents in plain language. We actually want you to read our policies and understand our privacy practices!</p>
<p>We work hard to keep your information secure. We have teams dedicated to keeping your data safe and secure. We constantly update our security practices and invest in our security efforts to enhance the safety of your information.</p>
<p>* * *</p>
<p>PRIVACY POLICY</p>
<p>Welcome to Humor’s Privacy Policy. Thank you for taking the time to read it.</p>
<p>We appreciate that you trust us with your information and we intend to always keep that trust. This starts with making sure you understand the information we collect, why we collect it, how it is used and your choices regarding your information. This Policy describes our privacy practices in plain language, keeping legal and technical jargon to a minimum.</p>
<p>This Privacy Policy applies beginning 5/6/2020.</p>
<p>EFFECTIVE DATE: 5/6/2020</p>
<p>
Please see our California Privacy Statement to learn about California privacy rights.</p>
<p>
WHERE THIS PRIVACY POLICY APPLIES
This Privacy Policy applies to websites, apps, events and other services operated by Humor. For simplicity, we refer to all of these as our “services” in this Privacy Policy. To make it extra clear, we’ve added links to this Privacy Policy on all applicable services.</p>
<p>Some services may require their own unique privacy policy. If a particular service has its own privacy policy, then that policy -- not this Privacy Policy -- applies.</p>
<p>INFORMATION WE COLLECT
It goes without saying, we can’t help you develop meaningful connections without some information about you, such as basic profile details and the types of people you’d like to meet. We also collect information generated as you use our services, for example access logs, as well as information from third parties, like when you access our services through a social media account. If you want additional info, we go into more detail below.</p>
<p>Information you give us</p>
<p>You choose to give us certain information when using our services. This includes:</p>
<p>o When you create an account, you provide us with at least your login credentials, as well as some basic details necessary for the service to work, such as your gender and date of birth.</p>
<p>o When you complete your profile, you can share with us additional information, such as details on your personality, lifestyle, interests and other details about you, as well as content such as photos and videos. To add certain content, like pictures or videos, you may allow us to access your camera or photo album. Some of the information you choose to provide us may be considered “special” or “sensitive” in certain jurisdictions, for example your racial or ethnic origins, sexual orientation and religious beliefs. By choosing to provide this information, you consent to our processing of that information.</p>
<p>o When you participate in surveys, focus groups or market studies, you give us your insights into our products and services, responses to our questions and testimonials.</p>
<p>o When you choose to participate in our promotions, events or contests, we collect the information that you use to register or enter.</p>
<p>o If you contact our customer care team, we collect the information you give us during the interaction. Sometimes, we monitor or record these interactions for training purposes and to ensure a high quality of service.</p>
<p>o If you ask us to communicate with or otherwise process information of other people (for example, if you ask us to send an email on your behalf to one of your friends), we collect the information about others that you give us in order to complete your request.</p>
<p>o Of course, we also process your chats with other users as well as the content you publish, as part of the operation of the services.</p>
<p>Information we receive from others</p>
<p>In addition to the information you provide us directly, we receive information about you from others, including:</p>
<p>o Other Users</p>
<p>Other users may provide information about you as they use our services. For instance, we may collect information about you from other users if they contact us about you.</p>
<p>o Social Media</p>
<p>You may be able to use your social media account (such as Facebook Login) to create and log into your Humor account. This saves you from having to remember yet another user name and password and allows you to share some information from your social media account with us.</p>
<p>o Other Partners</p>
<p>We may receive info about you from our partners, for instance where Humor ads are published on a partner’s websites and platforms (in which case they may pass along details on a campaign’s success).</p>
<p>Information collected when you use our services</p>
<p>When you use our services, we collect information about which features you’ve used, how you’ve used them and the devices you use to access our services. See below for more details:</p>
<p>o Usage Information</p>
<p>We collect information about your activity on our services, for instance how you use them (e.g., date and time you logged in, features you’ve been using, searches or search parameters, clicks and pages which have been shown to you, referring webpage address, advertising that you click on) and how you interact with other users (e.g., users you connect and interact with, time and date of your exchanges, number of messages you send and receive).</p>
<p>o Device information</p>
<p>We collect information from and about the device(s) you use to access our services, including:</p>
<p>o hardware and software information such as IP address, device ID and type, device-specific and apps settings and characteristics, app crashes, advertising IDs (such as Google’s AAID and Apple's IDFA, both of which are randomly generated numbers that you can reset by going into your device’ settings), browser type, version and language, operating system, time zones, identifiers associated with cookies or other technologies that may uniquely identify your device or browser (e.g., IMEI/UDID and MAC address); and</p>
<p>o information on your wireless and mobile network connection, like your service provider and signal strength.</p>
<p>o Other information with your consent</p>
<p>If you give us permission, we will be able to collect your precise geolocation (latitude and longitude) in order to offer you features that make use of it. Such geolocation is collected through various means, depending on the service and device you’re using, including GPS, Bluetooth or Wi-Fi connections. If you decline permission for us to collect your geolocation, we will not collect it.</p>
<p>Similarly, if you consent, we may collect your photos and videos (for instance, if you want to publish a photo, video or streaming on the services).</p>
<p>COOKIES AND OTHER SIMILAR DATA COLLECTION TECHNOLOGIES
We use and may allow others to use cookies and similar technologies (e.g., web beacons, pixels) to recognize you and/or your device(s). You may read our Cookie Policy for more information on why we use them (such as authenticating you, remembering your preferences and settings, analyzing site traffic and trends, delivering and measuring the effectiveness of advertising campaigns, allowing you to use social features) and how you can better control their use, through your browser settings and other tools.</p>
<p>Some web browsers (including Safari, Internet Explorer, Firefox and Chrome) have a “Do Not Track” (“DNT”) feature that tells a website that a user does not want to have his or her online activity tracked.  If a website that responds to a DNT signal receives a DNT signal, the browser can block that website from collecting certain information about the browser’s user.  Not all browsers offer a DNT option and DNT signals are not yet uniform.  For this reason, many businesses, including Humor, do not currently respond to DNT signals.</p>
<p>HOW WE USE INFORMATION
The main reason we use your information is to deliver and improve our services. Additionally, we use your info to help keep you safe and to provide you with advertising that may be of interest to you. Read on for a more detailed explanation of the various reasons we use your information, together with practical examples.</p>
<p>To administer your account and provide our services to you</p>
<p>o Create and manage your account</p>
<p>o Provide you with customer support and respond to your requests</p>
<p>o Communicate with you about our services</p>
<p>To help you connect with other users</p>
<p>o Analyze your profile and usage behavior and that of other users to recommend meaningful connections. For more information on our profiling and automated decision-making, please see our FAQ</p>
<p>o Show users’ profiles to one another</p>
<p>To ensure a consistent experience across your devices</p>
<p>o Link the various devices you use so that you can enjoy a consistent experience of our services on all of them. We do this by linking devices and browser data, such as when you log into your account on different devices or by using partial or full IP address, browser version and similar data about your devices to help identify and link them</p>
<p>To serve you relevant offers and ads</p>
<p>o Administer sweepstakes, contests, discounts or other offers</p>
<p>o Develop, display and track content and advertising tailored to your interests on our services and other sites</p>
<p>o Communicate with you by email, phone, social media or mobile device about products or services that we think may interest you</p>
<p>To improve our services and develop new ones</p>
<p>o Administer focus groups and surveys</p>
<p>o Conduct research and analysis of users’ behavior to improve our services and content (for instance, we may decide to change the look and feel or even substantially modify a given feature based on users’ behavior)</p>
<p>o Develop new features and services (for example, we may decide to build a new interests-based feature further to requests received from users)</p>
<p>To prevent, detect and fight fraud or other illegal or unauthorized activities</p>
<p>o Address ongoing or alleged misbehavior on and off-platform</p>
<p>o Perform data analysis to better understand and design countermeasures against these activities</p>
<p>o Retain data related to fraudulent activities to prevent against recurrences</p>
<p>To ensure legal compliance</p>
<p>o Comply with legal requirements</p>
<p>o Assist law enforcement</p>
<p>o Enforce or exercise our rights, for example our Terms</p>
<p>To process your information as described above, we rely on the following legal bases:</p>
<p>o Provide our service to you: Most of the time, the reason we process your information is to perform the contract that you have with us. For instance, as you go about using our service to build meaningful connections, we use your information to maintain your account and your profile, to make it viewable to other users and recommend other users to you.</p>
<p>o Legitimate interests: We may use your information where we have legitimate interests to do so. For instance, we analyze users’ behavior on our services to continuously improve our offerings, we suggest offers we think might interest you, and we process information for administrative, fraud detection and other legal purposes.</p>
<p>o Consent: From time to time, we may ask for your consent to use your information for certain specific reasons. You may withdraw your consent at any time by contacting us at the address provided at the end of this Privacy Policy.</p>
<p>HOW WE SHARE INFORMATION
Since our goal is to help you make meaningful connections, the main sharing of users’ information is, of course, with other people, as per your privacy settings. We also share some users’ information with service providers and partners who assist us in operating the services, with other Match Group companies and, in some cases, legal authorities. Read on for more details about how your information is shared with others.</p>
<p>o With other people</p>
<p>We share your information with other people as per your privacy settings (and in the case of any sharing features available on Humor, the individuals or apps with whom you may choose to share your information with) when you voluntarily disclose information on the service (including your public profile). Please be careful with your information and make sure that the content you share is stuff that you’re comfortable being publicly viewable since neither you nor we can control what others do with your information once you share it.</p>
<p>If you choose to limit the audience for all or part of your profile or for certain content or information about you, then it will be visible according to your settings.</p>
<p>o With our service providers and partners</p>
<p>We use third parties to help us operate and improve our services. These third parties assist us with various tasks, including data hosting and maintenance, analytics, customer care, marketing, advertising, payment processing and security operations.</p>
<p>We may also share information with partners who distribute and assist us in advertising our services. For instance, we may share limited information on you in hashed, non-human readable form to advertising partners.</p>
<p>We follow a strict vetting process prior to engaging any service provider or working with any partner. All of our service providers and partners must agree to strict confidentiality obligations.</p>
<p>o With other Match Group businesses</p>
<p>Humor is part of the Match Group family of businesses which, as of the date of this Privacy Policy, includes websites and apps such as Tinder, OkCupid, Plenty of Fish, Match, Meetic, BlackPeopleMeet, LoveScout24, OurTime, Pairs, ParPerfeito, and Twoo (for more details, click here).</p>
<p>We share your information with other Match Group companies:</p>
<p>- For them to assist us in processing your information, as service providers, upon our instructions and on our behalf. Assistance provided by other Match Group companies may include technical processing operations, such as data hosting and maintenance, customer care, marketing and targeted advertising, finance and accounting assistance, better understanding how our service is used and users’ behavior to improve our service, securing our data and systems and fighting against spam, abuse, fraud, infringement and other wrongdoings.</p>
<p>- In order to improve your chances at building significant connections with others: we may make you visible on other Match Group services or allow you to benefit from cross-platform functionalities. We will of course inform you of any such instance and comply with applicable law by, where relevant, allowing you to agree or to refuse. Examples of this may include the creation of a new service within Match Group, addressing a specific demographic that we feel would be interesting to you based on your demography or search criteria.</p>
<p>We may also share information with other Match Group companies for legitimate business purposes such as corporate audit, analysis and consolidated reporting as well as compliance with applicable laws. We may also share user information with other Match Group companies to remove users who violate our terms of service, or have been reported for criminal activity and/or bad behavior. In some instances, we may remove that user from all platforms.</p>
<p>o For corporate transactions</p>
<p>We may transfer your information if we are involved, whether in whole or in part, in a merger, sale, acquisition, divestiture, restructuring, reorganization, dissolution, bankruptcy or other change of ownership or control.</p>
<p>o When required by law</p>
<p>We may disclose your information if reasonably necessary: (i) to comply with a legal process, such as a court order, subpoena or search warrant, government / law enforcement investigation or other legal requirements; (ii) to assist in the prevention or detection of crime (subject in each case to applicable law); or (iii) to protect the safety of any person.</p>
<p>o To enforce legal rights</p>
<p>We may also share information: (i) if disclosure would mitigate our liability in an actual or threatened lawsuit; (ii) as necessary to protect our legal rights and legal rights of our users, business partners or other interested parties; (iii) to enforce our agreements with you; and (iv) to investigate, prevent, or take other action regarding illegal activity, suspected fraud or other wrongdoing.</p>
<p>o With your consent or at your request</p>
<p>We may ask for your consent to share your information with third parties. In any such case, we will make it clear why we want to share the information.</p>
<p>We may use and share non-personal information (meaning information that, by itself, does not identify who you are such as device information, general demographics, general behavioral data, geolocation in de-identified form), as well as personal information in hashed, non-human readable form, under any of the above circumstances. We may also share this information with other Match Group companies, acting as our service providers, and other third parties (notably advertisers) to develop and deliver targeted advertising on our services and on websites or applications of third parties, and to analyze and report on advertising you see. We may combine this information with additional non-personal information or personal information in hashed, non-human readable form collected from other sources. More information on our use of cookies and similar technologies can be found in our Cookie Policy.</p>
<p>CROSS-BORDER DATA TRANSFERS
Sharing of information laid out in Section 6 sometimes involves cross-border data transfers, for instance to the United States of America and other jurisdictions. As an example, where the service allows for users to be located in the European Economic Area (“EEA”), their personal information is transferred to countries outside of the EEA. We use standard contract clauses approved by the European Commission or other suitable safeguard to permit data transfers from the EEA to other countries. Standard contractual clauses are commitments between companies transferring personal data, binding them to protect the privacy and security of your data.</p>
<p>YOUR RIGHTS
We want you to be in control of your information, so we have provided you with the following tools:</p>
<p>o Access / Update tools in the service. Tools and account settings that help you to access, rectify or delete information that you provided to us and that’s associated with your account directly within the service. If you have any question on those tools and settings, please contact our customer care team for help here.</p>
<p>o Device permissions. Mobile platforms have permission systems for specific types of device data and notifications, such as phone book and location services as well as push notifications. You can change your settings on your device to either consent or oppose the collection of the corresponding information or the display of the corresponding notifications. Of course, if you do that, certain services may lose full functionality.</p>
<p>o Deletion. You can delete your account by using the corresponding functionality directly on the service.</p>
<p>We want you to be aware of your privacy rights. Here are a few key points to remember:</p>
<p>o Reviewing your information. Applicable privacy laws may give you the right to review the personal information we keep about you (depending on the jurisdiction, this may be called right of access, right of portability or variations of those terms). You can request a copy of your personal information by putting in such a request here.</p>
<p>o Updating your information. If you believe that the information we hold about you is inaccurate or that we are no longer entitled to use it and want to request its rectification, deletion or object to its processing, please contact us here.</p>
<p>For your protection and the protection of all of our users, we may ask you to provide proof of identity before we can answer the above requests.</p>
<p>Keep in mind, we may reject requests for certain reasons, including if the request is unlawful or if it may infringe on trade secrets or intellectual property or the privacy of another user. If you wish to receive information relating to another user, such as a copy of any messages you received from him or her through our service, the other user will have to contact our Privacy Officer to provide their written consent before the information is released.</p>
<p>Also, we may not be able to accommodate certain requests to object to the processing of personal information, notably where such requests would not allow us to provide our service to you anymore. For instance, we cannot provide our service if we do not have your date of birth.</p>
<p>o Uninstall. You can stop all information collection by an app by uninstalling it using the standard uninstall process for your device. If you uninstall the app from your mobile device, the unique identifier associated with your device will continue to be stored. If you re-install the application on the same mobile device, we will be able to re-associate this identifier to your previous transactions and activities.</p>
<p>o Accountability. In certain countries, including in the European Union, you have a right to lodge a complaint with the appropriate data protection authority if you have concerns about how we process your personal information. The data protection authority you can lodge a complaint with notably may be that of your habitual residence, where you work or where we are established.</p>
<p>RESIDENTS OF CALIFORNIA
If you are a California resident, you can request a notice disclosing the categories of personal information about you that we have shared with third parties for their direct marketing purposes during the preceding calendar year. To request this notice, please submit your request here. Please allow 30 days for a response. For your protection and the protection of all of our users, we may ask you to provide proof of identity before we can answer such a request.</p>
<p>HOW WE PROTECT YOUR INFORMATION
We work hard to protect you from unauthorized access to or alteration, disclosure or destruction of your personal information. As with all technology companies, although we take steps to secure your information, we do not promise, and you should not expect, that your personal information will always remain secure.</p>
<p>We regularly monitor our systems for possible vulnerabilities and attacks and regularly review our information collection, storage and processing practices to update our physical, technical and organizational security measures.</p>
<p>We may suspend your use of all or part of the services without notice if we suspect or detect any breach of security. If you believe that your account or information is no longer secure, please notify us immediately here.</p>
<p>HOW LONG WE RETAIN YOUR INFORMATION
We keep your personal information only as long as we need it for legitimate business purposes (as laid out in Section 5 and as permitted by applicable law.</p>
<p>In practice, we delete or anonymize your information upon deletion of your account, unless:</p>
<p>we must keep it to comply with applicable law;
we must keep it to evidence our compliance with applicable law;
there is an outstanding or potential issue, claim or dispute requiring us to keep the relevant information until it is resolved; or
the information is kept for our legitimate business interests, such as fraud prevention and enhancing users' safety and security. For example, information may need to be kept to prevent a user who was banned for unsafe behavior or security incidents from opening a new account.
Keep in mind that even though our systems are designed to carry out data deletion processes according to the above guidelines, we cannot promise that all data will be deleted within a specific timeframe due to technical constraints.</p>
<p>CHILDREN’S PRIVACY
Our services are restricted to users who are 18 years of age or older. We do not permit users under the age of 18 on our platform and we do not knowingly collect personal information from anyone under the age of 18. If you suspect that a user is under the age of 18, please use the reporting mechanism available through the service.</p>
<p>PRIVACY POLICY CHANGES
Because we’re always looking for new and innovative ways to help you build meaningful connections, this policy may change over time. We will notify you before any material changes take effect so that you have time to review the changes.</p>
<p>HOW TO CONTACT US
If you have questions about this Privacy Policy, here’s how you can reach us: hello@humorapp.co</p>
                    </div>
                </div>
            </div>
            <!--clients logo end-->
        </div>
    </section>
    <!--client section start-->
</div>
<!--body content wrap end-->

<!--footer section start-->
<footer class="footer-section">
    <!--footer top start-->
    <div class="footer-top gradient-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-12">
                    <div class="footer-nav-wrap text-white mb-0 mb-md-4 mb-lg-0">
                        <a class="d-block" href="#"><img src="images/logo-white.png" alt="footer logo" width="150" class="img-fluid mb-1"></a>
                        <p>Intrinsicly matrix high standards in niches whereas intermandated niche markets. Objectively harness competitive resources.</p>
                        <ul class="list-unstyled social-list mb-0">
                            <li class="list-inline-item"><a href="#" class="rounded"><span class="ti-facebook white-bg color-2 shadow rounded-circle"></span></a></li>
                            <li class="list-inline-item"><a href="#" class="rounded"><span class="ti-twitter white-bg color-2 shadow rounded-circle"></span></a></li>
                            <li class="list-inline-item"><a href="#" class="rounded"><span class="ti-linkedin white-bg color-2 shadow rounded-circle"></span></a></li>
                            <li class="list-inline-item"><a href="#" class="rounded"><span class="ti-dribbble white-bg color-2 shadow rounded-circle"></span></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 col-md-4 col-12">
                    <div class="footer-nav-wrap text-white">
                        <h5 class="text-white">Usefull Links</h5>
                        <ul class="list-unstyled footer-nav-list mt-3">
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Privacy Protection</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Safe Payments</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Terms of Services</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Documentation</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 col-md-4 col-12">
                    <div class="footer-nav-wrap text-white">
                        <h5 class="text-white">Company</h5>
                        <ul class="list-unstyled footer-nav-list mt-3">
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> About us Section</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Pricing Section</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Features Section</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> FAQ Section</a></li>
                            <li><a href="#" class="text-foot"><span class="ti-angle-double-right"></span> Contact Section</a></li>
                        </ul>
                    </div>
                </div>



                <div class="col-lg-4 col-md-4 col-12">
                    <div class="footer-nav-wrap text-white">
                        <h5 class="text-light footer-head">Newsletter</h5>
                        <p>Subscribe our newsletter to get our update. We don't send span email to you.</p>
                        <form action="#" class="newsletter-form mt-3">
                            <div class="input-group">
                                <input type="email" class="form-control" id="email" placeholder="Enter your email" required="">
                                <div class="input-group-append">
                                    <button class="btn solid-btn subscribe-btn btn-hover" type="submit">
                                        Subscribe
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--footer top end-->

    <!--footer copyright start-->
    <div class="footer-bottom gray-light-bg py-3">
        <div class="container">
            <div class="row text-center justify-content-center">
                <div class="col-md-6 col-lg-5"><p class="copyright-text pb-0 mb-0">Copyrights © 2020.</p>
                </div>
            </div>
        </div>
    </div>
    <!--footer copyright end-->
</footer>

<!--footer section end-->

<!--bottom to top button start-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="ti-angle-up"></span>
</button>
<!--bottom to top button end-->


<!--jQuery-->
<script src="scripts/jquery-3.4.1.min.js"></script>
<!--Popper js-->
<script src="scripts/popper.min.js"></script>
<!--Bootstrap js-->
<script src="scripts/bootstrap.min.js"></script>
<!--Magnific popup js-->
<script src="scripts/jquery.magnific-popup.min.js"></script>
<!--jquery easing js-->
<script src="scripts/jquery.easing.min.js"></script>
<!--wow js-->
<script src="scripts/wow.min.js"></script>
<!--owl carousel js-->
<script src="scripts/owl.carousel.min.js"></script>
<!--countdown js-->
<script src="scripts/jquery.countdown.min.js"></script>
<!--custom js-->
<script src="scripts/scripts.js"></script>
<script src="/assets/libs/countdown/jquery-countdown.min.js"></script>

        <!-- Countdown js -->
        <script src="/assets/libs/countdown/coming-soon.init.js"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163662190-1');
</script>
</body>
</html>
<?php

Route::get('/messages', 'HomeController@index')->name('home');
Route::post('/message/list/{convo}', 'ChatController@chatLog');
Route::post('/message/send', 'ChatController@sendChat');
Route::post('/message/menu/{action}', 'ChatController@chatMenu');
Route::post('/message/notifications', 'ChatController@getNotifications');
<?php

Route::prefix('profile')->group(function () {
	Route::get('/', 'ProfileController@readProfile')->name('read-profile');

	Route::post('/add/onboarding', 'ProfileController@addOnboarding')->name('add-onboarding');
	Route::post('/edit', 'ProfileController@editProfile')->name('edit-profile');
	//Route::post('/chat/{user}', 'ProfileController@chatProfile');
	Route::post('/avatar', 'ProfileController@readAvatar');
	Route::post('/card', 'ProfileController@profileCard');

	Route::post('/photo/upload', 'ProfileController@uploadPhoto');
	Route::post('/photo/sequence', 'ProfileController@sequencePhoto');
	Route::post('/photo/delete', 'ProfileController@deletePhoto');
});
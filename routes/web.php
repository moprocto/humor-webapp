<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index');
Route::get('/policy', function(){
	return view('policy');
})->name('policy');

Route::get('/support', function(){
	return view('support');
})->name('support');

Route::get('/user/{id}', 'PublicController@readUser');

Route::get('/memes', 'QuizController@browseImages');
Route::get('/memes/{id}', 'QuizController@readMeme');

Route::get('/quiz', 'PublicController@browseQuiz');

Route::get('/quiz-results/{top?}/{second?}', 'QuizController@quizResults')->name('quiz-results');
Route::post('/quiz-results', 'QuizController@addQuiz')->name('quiz-submit');

Route::get('/quizzes', 'QuizController@browseQuizzes');

Auth::routes();

Route::get('/home', 'DiscoverController@browse')->name('discover');
Route::post('/feed', 'DiscoverController@browseFeed')->name('discover-feed');
Route::get('/discover/feed/{uid}', '/iOSDiscoverController@browseFeed')->name('discover-feed');

Route::get('/likes', 'LikeController@index');

Route::get('/fb', 'PublicController@detectFB');

Route::get('/webhooks/{source?}', 'WebhooksController@index');
Route::post('/webhooks/{source}', 'WebhooksController@index');

Route::post('register/user', 'FirebaseController@register')->name('firebase-register');
Route::post('login/user', 'FirebaseController@login')->name('firebase-login');
Route::get('reset/password', 'FirebaseController@getReset')->name('firebase-reset-get');
Route::post('reset/password', 'FirebaseController@sendReset')->name('firebase-reset-post');
Route::post('reset/password/confirm', 'FirebaseController@resetPassword')->name('firebase-password-reset');

Route::get('logout', 'FirebaseController@logout')->name('firebase-logout');

Route::post('/post', 'PostController@addPost')->name('add-post');
Route::post('/post/verify/song', 'PostController@verifySong');
Route::post('/post/delete', 'PostController@deletePost')->name('delete-post');
Route::post('/read/like', 'PostController@readLike');
Route::post('/add/like', 'PostController@addLike');
Route::post('/add/unmatch', 'PostController@addUnmatch');


// /lab routes

include('lab.php');

// /message routes

include('messages.php');

// /profile routes

include('profile.php');

include('apiv1.php');
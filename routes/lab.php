<?php

Route::prefix('lab')->group(function () {
	Route::get('/', 'LabController@index')->name('lab');
	Route::post('/score/meme', 'LabController@scoreMeme');
});
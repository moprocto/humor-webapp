<?php

Route::prefix('apiv1')->group(function () {
	Route::prefix('discover')->group(function () {
		Route::get('/browse/{uid}', 'APIController@browseFeed');
		Route::get('/icebreaker/read/{current}/{request}', 'PostController@getIcebreaker');
		Route::get('/song/{width}/{url}', 'APIController@getSong');

		Route::post('/post/like', 'APIController@addLike');
		Route::post('/post/icebreaker', 'APIController@addIcebreaker');
		Route::post('/post/media', 'APIController@addMedia');

		Route::post('/post/delete', 'APIController@deletePost');
		Route::post('/post/report', 'APIController@reportPost');
	});
	Route::prefix('conversations')->group(function () {
		Route::get('/browse/{uid}', 'APIController@browseConversations');
		Route::get('/messages/{user_id}/{conversation_id}', 'APIController@browseMessages');
		Route::get('/read/{conversation_id}/{user_id}', 'APIController@readConversation');
		Route::post('/messages/add', 'APIController@addMessage');
		Route::post('/messages/media', 'APIController@addMediaMessage');
		Route::post('/typing', 'APIController@editConversationTyping');
	});
	Route::prefix('profile')->group(function () {
		Route::get('/read/{uid}/{user_id?}', 'APIController@readProfile');
		Route::get('/posts/{uid}/{user_id?}', 'APIController@readPosts');
		Route::get('/humor/{uid}/{user_id}/{callback}', 'APIController@compareHumor');
		Route::post('/edit', 'APIController@editProfile');
		Route::post('/add/photo', 'APIController@addPhoto');
		Route::post('/add', 'APIController@addProfile');
		Route::post('/edit/photo/sequence', 'APIController@editPhotoSequence');
		Route::post('/delete/photo', 'APIController@deletePhoto');
		Route::post('/token', 'APIController@saveToken');
		Route::post('/report', 'APIController@reportProfile');
		Route::post('/unmatch', 'APIController@unmatchProfile');

		Route::post('/verification', 'APIController@addVerification');
		Route::post('/verification/submit', 'APIController@editVerification');
	});
	Route::prefix('photos')->group(function () {
		Route::get('/read/{uid}', 'APIController@readPhotos');
	});
	Route::prefix('quiz')->group(function () {
		Route::get('/draw/{uid}', 'APIController@getQuizCards');
		Route::get('/result/{uid}', 'APIController@getHumor');
		Route::post('/score', 'APIController@scoreCard');
	});
});
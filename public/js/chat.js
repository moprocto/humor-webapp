$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $("#CSRF").attr('content')
      }
    });

    getChatMenu("new");


    var heartbeatInterval = 2000;
    var pacer;

    var swiper = new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
      },
    });

  heartbeat();



  function heartbeat(){

      clearInterval(pacer);

      var result = getChatWindow("append");

      updateHeartbeat();

      if(result == "reset"){
          heartbeatInterval = 2000;
      }

      pacer = setInterval(heartbeat, heartbeatInterval); 
  }

  function updateHeartbeat(){
    if(heartbeatInterval < 15000){
      heartbeatInterval = heartbeatInterval + heartbeatInterval * 0.1;
    }
    
  }

  
    $("body").on("click", "#send-message", function(){
        var content = $("#message-body").val();
        var toUser = $(this).attr("data-to");
        var convo = $("#chat-window").attr("data-convo");
        var d = Math.floor(Date.now() / 1000);
        $("#message-body").val("");

        if(content != ""){
          
          $("#chat-window").append('<li class="clearfix odd"><div class="chat-avatar" data-time="' + d + '"></div><div class="conversation-text"><div class="ctext-wrap"><p>' + content + '</p></div></div></li>');
          
          $.post("/message/send/",{
            to: toUser,
            content: content,
            conversation: convo
          })
            .done(function(data) {});
        }
    });

    

    $("body").on("click", ".match-row", function(){
      var convo = $(this).attr("data-convo");
      var toUser = $(this).attr("data-to");
      var userAvatar = $(this).attr("data-avatar");
      var displayName = $(this).attr("data-displayName");

      $(".chat-window-avatar").hide();
      $(".text-displayName").html('<div class="spinner-border text-blue spinner-border-sm" role="status" ></div>');
      $("#chat-window").html('<div class="d-flex justify-content-center mt-3"><div class="spinner-border text-blue" role="status"></div></div>');
      $("#profile-card").html('<div class="d-flex justify-content-center" style="padding:200px 0;"><div class="spinner-border text-blue" role="status"></div></div>');
      $.post("/message/list/" + convo, {type: "new"})
        .done(function(data) {
          $("#chat-window").attr("data-convo", convo);
          $("#chat-window").html(data);
          $("#send-message").attr("data-to", toUser);
          getProfileInfo(toUser, userAvatar, displayName);
          
          var width = $(window).width();

          if(width < 900){
            $(".content-page").show();
          }

          adjustScroll();
          
      });
        heartbeatInterval = 2000;
    });

    $("body").on("click", "#chat-user-header", function(){
      	$("#chat-window").empty();
      	$(".content-page").hide();
		$("#chat-screen-tab").addClass("active");
		$("#chat-screen").addClass("active");
		$("#profile-screen-tab").removeClass("active");
		$("#profile-screen").removeClass("active");
    });

    function getChatMenu(action){
      var stamp = $(".match-row").first().attr("data-stamp");


      $.post("/message/menu/" + action,{
        stamp: stamp
      })
      .done(function(data) {
        if(action == "refresh"){
          $("#matches").prepend(data);
        } else {
          $("#matches").html(data);
        }
        getChatWindow("new");
        showChatInterface();
      });

    }

    function getProfileInfo(user, avatar, displayName){
      $("#chat-user img.chat-window-avatar").show();
      $("#chat-user img.chat-window-avatar").attr("src", avatar);
      $("#chat-user .text-displayName").text(displayName);
      var c = $("#chat-window").attr('data-convo');
      $.post("/profile/card", {
        user: user,
        avatar: avatar,
        displayName: displayName,
        convo: c
      })
        .done(function(data) {
          $("#profile-card").html(data);
          var swiper = new Swiper('.swiper-container', {
            pagination: {
              el: '.swiper-pagination',
              dynamicBullets: true,
            },
          });
          
      });
    }

    function getChatWindow(type){

      var result = "none";

      
      
      $(".match-row").each(function(i){
        var toUser = $(this).attr("data-to");
        var convo = $(this).attr("data-convo");
        var userAvatar = $(this).attr("data-avatar");
        var displayName = $(this).attr("data-displayName");
        var heartbeat = parseInt($("#send-heartbeat").val());
        
        
        if(i == 0){
          
          if($(this).hasClass("active")){
            $("#send-message").attr('data-to', toUser);
          }

          $.ajax({
            type: 'POST',
            url: "/message/list/" + convo,
            data: {type: type},
            success: function(data) {
                if(type == "new"){
                  $("#chat-window").html(data);
                  

                  $("#chat-window").attr('data-convo', convo);
                  
                }
                else {
                  data = $.parseJSON( data );
                  // check to see if last item is unique
                  var dataTime = parseInt($("#chat-window li:last-child").find(".chat-avatar").attr("data-time"));
                  var upperBound = dataTime + 2;

                  console.log(data.time);
                  console.log(upperBound);

                  if($("#chat-window").attr('data-convo') == data.convo && data.time >= upperBound){
                    $("#chat-window").append(data.html);
                    console.log("print reset if new");
                    result = "reset";
                    
                  }
                }
                
                
                
          },
            async:false
          });

          if(type == "new"){
            
            getProfileInfo(toUser, userAvatar, displayName);
            

          }
          if(heartbeatInterval < 2600){
            adjustScroll();
          }
          
            
        }


      
      });

      return result;
    }

    function showChatInterface(){
        if ( $('.match-row').length >= 1 ) {
        $('#main-chat-window').show();
        $('#profile-card').show();
      } else {
        $("#matches").html('No messages, yet! Visit <a href="/home">Discover</a> to connect with others');
        $(".content").html('<div class="card hidden-md-up d-md-none mt-4"><div class="card-body"><p>No messages, yet! Visit <a href="/home">Discover</a> to start connecting with users</p></div></div>');
      } 

      $("#send-message").attr("data-to", $(".match-row:first").attr("data-to"));
    }



function adjustScroll(){
  $(function() {
    console.log(heartbeatInterval);
    $('#chat-scroll').scrollTop($('#chat-scroll').height() + 9999999); 
  });
}
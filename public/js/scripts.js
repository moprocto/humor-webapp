function getIcebreaker(){var a=$("#icebreaker_id").val();$.ajax({url:"/api/icebreaker/"+a,async:!0}).done(function(a){$("#icebreaker").html(a)})}function getAvatar(a){$.post("/profile/avatar/",{user:a}).done(function(a){$(".user-avatar").each(function(){""!=a?$(this).attr("src",a):$(this).attr("src","/images/logo.png")})})}function pollChatNotifcations(){$.ajax({type:"POST",url:"/message/notifications",dataType:"json",success:function(a){if(""!=a)
	$.toast({
	heading:a.heading,
	text: '<p>' + a.text + '</p>',
	loader:!0,
	loaderBg:"#6332f6",
	allowToastClose:!0,
	position:"top-center"}
	)},async:!0})}

$.ajaxSetup({headers:{"X-CSRF-TOKEN":$("#CSRF").attr("content")}});


$("body").on("click",".jq-toast-heading",function(){window.location.href="/messages"});

$("body").on("click",".unmatch",function(){
	var a = $(this).attr("data-avatar");
	var u = $(this).attr("data-user");
	var c = $(this).attr("data-convo");
	Swal.fire({
	    html: '<img src="' + a + '" width="100"><br> <h5>Sorry things didn\'t work out =/</h5> <br> <textarea id="unmatch-reason" class="form-control" placeholder="Submit an anonymous tip (optional) to us if this person acted inappropriately."></textarea>' ,
	    showCloseButton: !0,
	    confirmButtonClass: "btn btn-danger mt-2",
	    confirmButtonText: '<i class="mdi mdi-heart-broken"></i> Unmatch',
	    showLoaderOnConfirm: !0,
	    preConfirm: function(i) {
	        return new Promise(function(i) {
	        	var r = $("#unmatch-reason").val();
	            $.post("/add/unmatch/", {
	                user: u,
	                reason: r,
	                convo: c
	            }).done(function(a) {
	                window.location.href = "/messages";
	            })
	        })
	    }
	});
});
$.ajaxSetup({
  headers: {'X-CSRF-TOKEN': $("#CSRF").attr('content')}
});

$(".lazy").lazyload();

function scoreMeme(target, cap){
    $("#meme-window").html('<div class="d-flex justify-content-center" style="padding:110px 0;"><div class="spinner-border text-blue" role="status"></div></div>');
            
    var meme_id = target.attr("data-meme");
    var score = target.attr("data-score");

    $.post("/lab/score/meme", {meme: meme_id,score: score}).done(function(data) {
        data = $.parseJSON( data );
        title = data[0]["prompt"];

        if(title === null || title == "" || title == " "){
            title = "";
        } else {
            title = '<h3 class="text-center" style="font-size:16px;">' + title + '</h3>';
        }

        setTimeout(function(){
            if(data[1] < cap){
                $(".score-meme").each(function(){
                    $(this).attr("data-meme", data[0]["id"]);
                    $(this).attr('checked', false);
                    $(this).removeClass('selected', false);

                });
                $("#meme-window").html(title + '<img src="/images/' + data[0]["file"] + '" style="width:100%; margin: 0 auto; max-height:380px;" class="quiz-image">');
            }
            else {
                // user is ready!
                $("#quiz-buttons").html("");
                $("#meme-window").html('<div class="col-md-12 text-center"><span class="rounded-circle btn btn-success text-center" style="width: 7rem; height: 7rem;"><i class="mdi mdi-check-bold" style="font-size: 4rem;"></i></span></div>');
                $("#quiz-form-title").html("Your responses have been sent to the lab for analysis");
                $("#quiz-form-title").parent().removeClass("mb-150");
            }
        }, 1000);
    });
}
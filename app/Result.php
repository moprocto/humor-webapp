<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = [
        'user_id','meme_id','score'
    ];

    function profile(){
    	return $this->belongsTo('\App\Profile');
    }

    function meme(){
    	return $this->hasOne('\App\Meme');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unmatch extends Model
{
    protected $fillable = [
        'user_id', 'receiver_id','reason', 'conversation_id'
    ];

    public static function verify($me, $them){
		$unmatches = \App\Unmatch::where('user_id', $me)->orWhere('receiver_id', $me)->get();

		if($unmatches->isNotEmpty()){
			// there are conversations
			
			$unmatchExists = $unmatches->where('user_id', $them);
			if($unmatchExists->isNotEmpty()){
					return true;
			} else {
				$unmatchExists = $unmatches->where('receiver_id', $them); 
				if($unmatchExists->isNotEmpty()){
						return true;
				}
			}
			
			return false;
			
		} 
		return false;
    }
}

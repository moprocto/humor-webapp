<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = [
        'user_id', 'post_id'
    ];

    function post(){
    	return $this->belongsTo('\App\Post');
    }

    function profile(){
    	return $this->belongsTo('\App\Profile', 'user_id', 'user_id');
    }

    function message(){
    	return $this->belongsTo('\App\Message','id' ,'like_id');
    }
}

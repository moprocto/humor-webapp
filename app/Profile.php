<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $fillable = [
        'user_id','avatar','bio','birthday','displayName','drinking','drugs','educationLevel','gender','jobTitle','lookingFor','marijuana','onboarded','religion','school','showMe','smoking','timezone','zipcode', 'fcm_token', 'email_code', 'emailVerified'
    ];

    function user(){
    	return $this->belongsTo('\App\User');
    }

    function likes(){
        return $this->hasMany('\App\Like');
    }

    function results(){
        return $this->hasMany('\App\Result', 'user_id', 'user_id');
    }

    function posts(){
        return $this->hasMany('\App\Post', 'user_id', 'user_id');
    }

    function photos(){
        return \App\Photo::where('user_id', $this->user_id)->orderBy('sequence', 'ASC')->get();
    }

    function conversations(){
        return \App\Message::conversations($this->user_id);
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'user_id', 'icebreaker', 'answer', 'mediaLink', 'songURL', 'type'
    ];

    function likes(){
        return $this->hasMany('\App\Like')->orderBy('created_at', 'DESC');
    }

    public static function read($database, $post_id){
        return $database->getReference("posts/$post_id")->getValue();
    }

    public static function icebreaker($database, $icebreaker_id){
        return $database->getReference('icebreakers/'  . $icebreaker_id)->getValue();
    }

    function profile(){
        return $this->belongsTo('\App\User');
    }
}

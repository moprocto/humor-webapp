<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use App\Photo as Photos;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photo extends Model
{
	use SoftDeletes;

	protected $fillable = [
        'user_id','mediaLink','sequence'
    ];

    function profile(){
        return $this->belongsTo('\App\Profile');
    }

	public static function read($database, $photoID){
		$user = Session::get('user');
		return $database->getReference('/photos/' . '/' . $user->uid . '/' . $photoID)->getValue();
	}
}
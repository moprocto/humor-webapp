<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class Firebase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('active') == true && Session::get('user') !== null){
            return $next($request);
        }
        return redirect('/login');
    }
}

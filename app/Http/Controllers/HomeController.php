<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use App\User as Users;
use App\Message as Messages;
use Carbon;
use Session;
use Redirect;

$user = Session::get('user');

//date_default_timezone_set();


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('firebase');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index(Request $request)
    {
        $user = Session::get('user');

        $profile = Users::profile($user->uid);
        
        if(!$profile->onboarded){
            $memes = json_decode(app(\App\Http\Controllers\LabController::class)->getMeme());
            return view('onboarding', ["user" => $user, "profile" => $profile, "memes" => $memes]);
        }

        $data = [
            "page" => "home",
            "user" => $user,
            "profile" => $profile,
            "i" => 0
        ];

        return view('home', $data);
    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User as Users;
use App\Result as Results;
use App\Meme as Memes;
use Session;
use DB;

class LabController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('firebase');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(){
        $user = Session::get('user');
        $profile = Users::profile($user->uid);
        $posts = $profile->posts;
        $results = $profile->results;
        $memes = json_decode($this->getMeme());

        if(!$profile->onboarded){
            return view('onboarding', ["user" => $user, "profile" => $profile, "memes" => $memes]);
        }

        $data = [
            "page" => "lab",
            "user" => $user,
            "profile" => $profile,
            "posts" => $posts,
            "results" => $results,
            "memes" => $memes
        ];

        return view('lab', $data);
    }

    public function scoreMeme(Request $request){
        $user = Session::get('user');

        $validator = Validator::make($request->all(), [
            'meme' => 'required|max:255',
            'score' => 'required|max:1',
        ]);

        if (!$validator->fails()) {
            Results::create([
                "user_id" => $user->uid,
                "meme_id" => $request->meme,
                "score" => $request->score
            ]);
        }

        $memes = $this->getMeme();

        return $memes;
    }

    public function getMeme(){
        $user = Session::get('user');
        $profile = Users::profile($user->uid);
        $results = $profile->results;
        
        $memes = DB::table('Meme')                 
            ->select('id','file', 'type', 'prompt', 'active')
            ->whereNotIn('id', DB::table('results')->where('user_id', $user->uid)->pluck('meme_id')->toArray())
            ->where('active',1)
            ->inRandomOrder()
            ->first();
            
        return json_encode([$memes, $results->count()]);
    }

    public function getHumor($user){
        $profile = Users::profile($user);
        $results = $profile->results;

        $sexual = $dark = $witty = $weird = $physical = $contextual = 0;

        foreach($memes as $meme){
            $var = "item_$meme->_id";

            switch ($meme->type) {
                case 'dark':
                    $dark += $request->{$var};
                    break;
                case 'witty':
                    $witty += $request->{$var};
                    break;
                case 'weird':
                    $weird += $request->{$var};
                    break;
                case 'physical':
                    $physical += $request->{$var};
                    break;
                case 'contextual':
                    $contextual += $request->{$var};
                    break;
                case 'sexual':
                    $sexual += $request->{$var};
                    break;
                default:
                    break;
            }
        }
    }
}

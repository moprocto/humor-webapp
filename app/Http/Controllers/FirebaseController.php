<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use App\Profile as Profiles;
use Session;
use Redirect;
use Throwable;
use Exception;
use Mail;
use Illuminate\Support\Facades\Validator;

class FirebaseController extends Controller
{
    public function register(Request $request){
    	$auth = $this->auth()->createAuth();

    	// validation

    	$validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|min:8|confirmed',
        ]);

    	if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

		try{
			$userProperties = [
			    'email' => $request->email,
			    'emailVerified' => false,
			    'password' => $request->password,
			    'displayName' => $request->name,
			    'photoUrl' => 'http://www.example.com/12345678/photo.png',
			    'disabled' => false,
			];

			$createdUser = $auth->createUser($userProperties);

			// create a profile

			$database = $this->auth()->createDatabase();

			$request->merge(['user_id' => $createdUser->uid, 'displayName' => $request->name, 'avatar' => "/images/logo.png"]);

			Profiles::create($request->all());

			Session::put('user', $createdUser);
			Session::put('active', true);

			return Redirect::to('/home');
		}
		catch(Exception $e){
			return Redirect::back()->withErrors(["This email is already in use"]);
		}



    }

    public function login(Request $request){
    	$auth = $this->auth()->createAuth();

		try{
			$signInResult = $auth->signInWithEmailAndPassword($request->email, $request->password);
			if(isset($signInResult)){
				$user = $auth->getUserByEmail($request->email);
                Session::flush();
                Session::regenerate();


				Session::put('user', $user);
				Session::put('active', true);

				return Redirect::to('/home');
			}
		}
		catch(Exception $e){
			return Redirect::back()->withErrors(["Incorrect email or password"]);
		}

        return Redirect::back()->withErrors(["There was an error signing you in. Please try again!"]);
    }

    public function logout(Request $request){
    	Session::flush();
    	Session::regenerate();

    	return Redirect::to('/');
    }

    public function auth(){
    	$firebase = (new Factory)
			->withServiceAccount(app_path() . env('FIREBASE_CREDENTIALS'))
			->withDatabaseUri(env('FIREBASE_DATABASE'));

		return $firebase;
    }

    public function getReset(){

    	$data = [
    		"page" => "reset"
    	];

    	return view('auth.passwords.email', $data);
    }

    public function sendReset(Request $request){
    	$validator = Validator::make($request->all(), ['email' => 'required|max:255']);

    	if ($validator->fails()) {
    		return Redirect::back()->withErrors($validator)->withInput();
        }

        $auth = $this->auth()->createAuth();

    	try{
    		$user = $auth->getUserByEmail($request->email);

    		if(isset($user)){
	    		$token = generateRandomString(10);
				Session::put('reset_token', $token);
				Session::put('reset_email', $user->email);

                Mail::send(['html' => 'emails.password-reset'], ["token" => $token], function($message) use ($user){
                $message->to($user->email, $user->displayName)
                ->subject("Your email verification code is...");
                    $message->from("hello@humorapp.co",'humor');
                    $message->sender("hello@humorapp.co",'humor');
                });

    		}
    	}
    	catch(Exception $e){

    	}

    	$data = [
    		"page" => "confirm"
    	];

    	return view('auth.passwords.email', $data);
    }

    public function resetPassword(Request $request){
    	$auth = $this->auth()->createAuth();
        $token = Session::get('reset_token');
        $email = Session::get('reset_email');

        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'password' => 'required|min:8|confirmed',
        ]);

        if ($validator->fails() || $request->token != $token) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        try{
            $user = $auth->getUserByEmail($email);

            if(isset($user)){
                $updatedUser = $auth->changeUserPassword($user->uid, $request->password);
                
                Session::flush();
                Session::regenerate();
                
                Session::put('active', true);
                Session::put('user', $user);

                Session::flash("success", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "Your password has been updated!"
                ]);

                return Redirect::to('/profile');
            }
        }
        catch(Exception $e){
            return Redirect::back()->withErrors(["email" => "There was an error updating your email."]);
        }

        return Redirect::back()->withErrors(["email" => "There was an error updating your email."]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as Users;
use App\Conversation as Conversations;
use App\Message as Messages;
use App\Post as Posts;
use App\Profile as Profiles;
use Session;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification as FCM_Notification;
use Kreait\Firebase\Messaging\ApnsConfig as ApnsConfig;

class ChatController extends Controller
{
    public function __construct()
    {
        $this->middleware('firebase');
    }

    public function chatLog(Request $request){
        $user = Session::get('user');
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        $database = $init->createDatabase();

        $messages = "";

        if($request->type == "new"){
            $chats = Messages::where('conversation_id', $request->convo)->orderBy('startedAt', 'ASC')->get();

            foreach ($chats as $chat) {
                $chatSide = "";

                if($chat->user_id == $user->uid){
                    $them = $request->from;
                    $chatSide = "odd"; // I wrote it
                } else {
                    $them = $chat->receiver_id;
                    $chatSide = ""; // they wrote it
                }

                if(isset($chat->subject)){
                    if($chat->user_id == $user->uid){ $d = ""; } else {  $d = "odd"; }
                    
                    $messages .= '<li class="chat-item clearfix ' . $d . '">
                                    <div class="chat-avatar" data-time="' . $chat->startedAt . '">
                                    </div>
                                    <div class="conversation-text">
                                        <div class="ctext-wrap">';

                                        $messages .= '<p>' . $chat["subject"] . '...<br><strong>' . $chat["subheader"] . ' 
                                            </strong></p>
                                        </div>
                                    </div>
                                </li>';

                }

                if(isset($chat->media)){
                    if($chat->user_id == $user->uid){ $d = ""; } else {  $d = "odd"; }
                    
                    $messages .= '<li class="chat-item clearfix ' . $d . '">
                                    <div class="chat-avatar" data-time="' . $chat->startedAt . '">
                                    </div>
                                    <div class="conversation-text">
                                        <div class="ctext-wrap">';

                                        $messages .= '<img src="' . $chat["media"] . '" data-featherlight="' . $chat["media"] . '" style="width:100%; max-width:330px;">
                                        </div>
                                    </div>
                                </li>';

                }

                if(isset($chat->songURL)){
                    if($chat->user_id == $user->uid){ $d = ""; } else {  $d = "odd"; }
                    
                    $messages .= '<li class="chat-item clearfix ' . $d . '">
                                    <div class="chat-avatar" data-time="' . $chat->startedAt . '">
                                    </div>
                                    <div class="conversation-text">
                                        <div class="ctext-wrap">';

                                        $messages .= '<iframe src="' .  $chat->songURL . '" height="280" frameborder="0" allowtransparency="true" allow="encrypted-media" style="width:100%;"></iframe>
                                        </div>
                                    </div>
                                </li>';

                }

                if($chat->user_id == $user->uid){
                    $them = $request->from;
                    $chatSide = "odd"; // I wrote it
                } else {
                    $them = $chat->receiver_id;
                    $chatSide = ""; // they wrote it
                }
                
                $messages .= '<li class="chat-item clearfix ' . $chatSide . '">
                                    <div class="chat-avatar" data-time="' . $chat->startedAt . '">
                                    </div>
                                    <div class="conversation-text">
                                        <div class="ctext-wrap">';

                $messages .= '                           <p>
                                                ' . $chat->message . ' 
                                            </p>
                                        </div>
                                    </div>
                                </li>';
                
            }

            

            return $messages;
        }

        elseif($request->type == "append"){
            // only get the latest message in the conversation

            $chat = Messages::where('conversation_id', $request->convo)->orderBy('startedAt', 'DESC')->first();

            
            
            $chatSide = "";

            if($chat->user_id == $user->uid){
                $chatSide = "odd";
            }

            if(isset($chat->subject)){
                    if($chat->user_id == $user->uid){ $d = ""; } else {  $d = "odd"; }
                    
                    $messages .= '<li class="chat-item clearfix ' . $d . '">
                                    <div class="chat-avatar" data-time="' . $chat->startedAt . '">
                                    </div>
                                    <div class="conversation-text">
                                        <div class="ctext-wrap">';

                                        $messages .= '<p>' . $chat["subject"] . '...<br><strong>' . $chat["subheader"] . ' 
                                            </strong></p>
                                        </div>
                                    </div>
                                </li>';

                }

                if(isset($chat->media)){
                    if($chat->user_id == $user->uid){ $d = ""; } else {  $d = "odd"; }
                    
                    $messages .= '<li class="chat-item clearfix ' . $d . '">
                                    <div class="chat-avatar" data-time="' . $chat->startedAt . '">
                                    </div>
                                    <div class="conversation-text">
                                        <div class="ctext-wrap">';

                                        $messages .= '<img src="' . $chat->media . '" data-featherlight="' . $chat["media"] . '" style="width:100%; max-width:330px;">
                                        </div>
                                    </div>
                                </li>';

                }

                if(isset($chat->songURL)){
                    if($chat->user_id == $user->uid){ $d = ""; } else {  $d = "odd"; }
                    
                    $messages .= '<li class="chat-item clearfix ' . $d . '">
                                    <div class="chat-avatar" data-time="' . $chat->startedAt . '">
                                    </div>
                                    <div class="conversation-text">
                                        <div class="ctext-wrap">';

                                        $messages .= '<iframe src="' .  $chat["songURL"] . '" height="280" frameborder="0" allowtransparency="true" allow="encrypted-media" style="width:100%;"></iframe>
                                        </div>
                                    </div>
                                </li>';

                }

                if($chat->user_id == $user->uid){
                    $them = $request->from;
                    $chatSide = "odd"; // I wrote it
                } else {
                    $them = $chat->receiver_id;
                    $chatSide = ""; // they wrote it
                    $chat->update(["read", true]);
                }

            
            $messages .= '<li class="clearfix ' . $chatSide . '">
                                <div class="chat-avatar" data-time="' . $chat->startedAt . '">
                                </div>
                                <div class="conversation-text">
                                    <div class="ctext-wrap">';


            $messages .= '                           <p>
                                            ' . $chat->message . ' 
                                        </p>
                                    </div>
                                </div>
                            </li>';

                
                
                return json_encode(["time" => $chat->startedAt, "html" => $messages, "convo" => $chat["conversation_id"] ]);
        }
    }

    public function sendChat(Request $request){
        $user = Session::get('user');
        $profile = Users::profile($user->uid);
        $date = getRealTime($profile["timezone"]);

        if(!empty($request->content)){

            Messages::create([
                'user_id' => $user->uid,
                'receiver_id' => $request->to,
                'message' => $request->content,
                'conversation_id' => $request->conversation,
                'viewed' => false,
                'notified' => false,
                'startedAt' => $date
            ]);

            // update the conversation details

            $conversation = Conversations::find($request->conversation);

            $conversation->update([
                "lastMessage" => $request->content,
                "startedAt" => $date
            ]);

        }

        $message = '<li class="clearfix odd">
                            <div class="chat-avatar" data-time="' . $date . '">
                                <i>Now</i>
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap"><p>
                                        ' . $request->content . ' 
                                    </p>
                                </div>
                            </div>
                        </li>';

        // if the other user has notifications enabled. send 

        $theirProfile = Profiles::where('user_id', $request->to)->first();

        if(!is_null($theirProfile->fcm_token)){
                $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
                $auth = $init->createAuth();
                $messaging = $init->createMessaging();

                $message = CloudMessage::withTarget('token', $theirProfile->fcm_token);

                $config = ApnsConfig::fromArray([
                    'headers' => [
                        'apns-priority' => '10',
                    ],
                    'payload' => [
                        'aps' => [
                            'sound' => 'default',
                            'alert' => [
                                'title' => "$profile->displayName",
                                'body' => $request->content,
                            ],
                            'badge' => 0,
                        ],
                    ],
                ]);

                $message = $message->withApnsConfig($config);

               
                $messaging->send($message);
            }

        return $message;
    }

    public function chatMenu(){
        $user = Session::get('user');
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        $database = $init->createDatabase();
        $conversations = Messages::conversations($user->uid);

        $profile = Users::profile($user->uid);

        $menu = '<div class="nav flex-column nav-pills nav-pills-tab" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="padding:0">';

        $i = 0;


        foreach($conversations as $key => $convo){

            if($convo["user_id"] == $user->uid){
                $convoUser = Users::profile($convo["receiver_id"]);
            } else {
                $convoUser = Users::profile($convo["user_id"]);
            }

            if(empty($userAvatar)){
                $userAvatar = "/images/logo.png";
            }

            $active = "";
            if($i == 0){
                //$active = "active";
            }
            $menu .= '
                    <a class="nav-link ' . $active .  ' mb-1 match-row" data-convo="' . $convo->id . '" data-to="' . $convoUser["user_id"] . '" id="' . $convoUser["user_id"] . '" data-toggle="pill" href="#v-pills-home' . $i .  '" role="tab" aria-controls="v-pills-home' . $i .  '" data-avatar="' . $convoUser["avatar"] . '" data-displayName="' . $convoUser["displayName"] . '">
                    <div class="media">
                        <div class="position-relative mr-2">
                            <img src="' . $convoUser["avatar"] . '" class="rounded-circle avatar-sm" alt="user-pic">
                        </div>
                        <div class="media-body">
                            <h6 class="mt-0 mb-1 font-14">' . $convoUser["displayName"] . '
                            <span class="float-right text-muted" style="font-size: 10px;">' . calculateChatAge($convo["startedAt"], $profile["timezone"]) . '</span>
                            </h6>
                            <div class="font-13 text-muted">
                                <p class="mb-0 text-truncate" style="max-width:165px;">' . $convo["lastMessage"] . '</p>
                            </div>
                        </div>

                    </div>
                    </a>               
                ';
            $i++;
        }

        $menu .= "</div>";

            return $menu;
    }

    public function getNotifications(Request $request){
        $user = Session::get('user');
        $messages = Messages::dialog($user->uid);
        $notifications = [];

        foreach ($messages as $message) {
            if(isset($message->viewed) && !$message->viewed && !$message->notified){
                $notifications[] = $message;
                $message->update(["viewed" => true, "notified" => true]);
            }
        }
        $notes = sizeof($notifications);

        if($notes > 1){
            return json_encode(["heading" => "New Messages", "text" => "You have $notes new messages"]);
        }
        elseif($notes == 1){
            $message = $notifications[0];
            $profile = Users::profile($message->user_id);

            if(isset($profile->avatar)){
                $avatar = $profile->avatar;
            } else {
                $avatar = "/images/logo.png";
            }
            $text = substr($message->message, 0, 40);

            return json_encode(["heading" => "<img src='" . $avatar . "' class='avatar-sm rounded-circle'> " . $profile->displayName, "text" => $text]);
        }

        return null;
    }
}

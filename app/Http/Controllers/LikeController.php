<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as Users;
use App\Like as Likes;
use Session;

class LikeController extends Controller
{
    public function index(){
    	$myLikes = [];
    	$user = Session::get('user');
    	$profile = Users::profile($user->uid);

    	$posts = Users::posts($user->uid);

    	foreach($posts as $post){
    		$likes = $post->likes;

    		foreach($likes as $like){
    			array_push($myLikes, $like);
    		}
    	}

    	$data = [
    		"likes" => $myLikes,
    		"page" => "likes",
    		"profile" => $profile,
    		"user" => $user
    	];

    	return view('likes', $data);
    }
}

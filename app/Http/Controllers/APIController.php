<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as Users;
use App\Message as Messages;
use App\Post as Posts;
use App\Profile as Profiles;
use App\Photo as Photos;
use App\Unmatch as Unmatches;
use App\Conversation as Conversations;
use App\Like as Likes;
use App\Icebreaker as Icebreakers;
use App\Report as Reports;
use App\Result as Results;
use App\Meme as Memes;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification as FCM_Notification;
use Kreait\Firebase\Messaging\ApnsConfig as ApnsConfig;

use Carbon;
use Session;
use Redirect;
use View;
use Storage;
use DB;
use Mail;

class APIController extends Controller
{
    public function browseFeed($uid){
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        $cards = array();
        $unmatched = false;

        try{
            $user = $auth->getUser($uid);
        }
        catch(Exception $e){
            // abandon app. user not authorized

            return "error";
        }
        
        $posts = Posts::orderBy('created_at', 'DESC')->get();

        foreach($posts as $key => $post){
            $userID = $post["user_id"]; 
            if($userID != $uid){
                $unmatched = Unmatches::verify($uid, $userID);
            }
            
            $owner = 0;
            if($userID == $uid){
                $owner = 1;
            }

            if(!$unmatched){
                $humorCompare = "none";
                $profile = Users::profile($userID);
                $avatar = $profile["avatar"];
                $likes = $post->likes->count();
                $liked = $post->likes->where('user_id', $uid)->count();
                $quizResults = Results::where('user_id', $profile->user_id)->count();

                if($quizResults >= 20){
                    $humorCompare = $this->compareHumor($uid, $userID, "strength"); // this is an expensive operation
                }

                $postVars = [
                    'id' => $post->id,
                    'owner' => $owner,
                    'likes' => $likes,
                    'author_id' => $userID,
                    'displayName' => $profile["displayName"],
                    'avatar' => $avatar,
                    'type' => $post["type"],
                    'icebreaker' => "",
                    'answer' => "",
                    'mediaLink' => "",
                    'songURL' => "",
                    'liked' => $liked,
                    'humor' => $humorCompare
                ];

                if($post["type"] < 3){
                    $postVars["icebreaker"] = $post["icebreaker"];
                    $postVars["answer"] = $post["answer"];
                }
                if($post["type"] == 3){
                    $postVars["mediaLink"] = $post["mediaLink"];
                }
                if($post["type"] == 4){
                    $postVars["songURL"] = str_replace("/", "***", $post["songURL"]);
                }
                if($post["type"] < 4){
                    // no song support for now
                    array_push($cards, $postVars);
                }
                
            }
        }
        return json_encode($cards);
    }

    public function browseConversations($uid){
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        $cards = array();
        $unmatched = false;
        $conversations = [];

        try{
            $auth->getUser($uid);
        }
        catch(Exception $e){
            return "error";
        }

        $convos = Messages::conversations($uid);

        $profile = Users::profile($uid);

        foreach($convos as $key => $convo){

            if($convo["user_id"] == $uid){
                $convoUser = Users::profile($convo["receiver_id"]);
            } else {
                $convoUser = Users::profile($convo["user_id"]);
            }

            $lastMessage = mb_substr($convo["lastMessage"], 0, 50);
            if(strlen($lastMessage) == 50){
                $lastMessage = $lastMessage . "...";
            }

            $unreadMessages = Messages::where('conversation_id', $convo["id"])->where('viewed', false)->where('user_id', '!=', $uid)->count();

            $unread = false;

            if($unreadMessages > 0){
                $unread = true;
            }

            $convoVars = [
                "id" => $convo->id,
                "startedAt" => calculateChatAge($convo["startedAt"], $profile["timezone"]),
                "avatar" => $convoUser["avatar"],
                "lastMessage" =>  $lastMessage,
                "displayName" => $convoUser["displayName"],
                "user_id" => $uid,
                "receiver_id" => $convoUser["user_id"],
                "unread" => $unread,
                "userTyping" => $convo->userTyping,
                "receiverTyping" => $convo->receiverTyping
            ];

            array_push($conversations, $convoVars);

        }

        return json_encode($conversations);
    }

    public function readConversation($conversation_id, $user_id){
        $convo = Conversations::where('id', $conversation_id)->first();

        $profile = Users::profile($user_id);

        $convoVars = [
                "id" => $convo->id,
                "startedAt" => calculateChatAge($convo["startedAt"], $profile["timezone"]),
                "avatar" => "blank",
                "lastMessage" =>  "blank",
                "displayName" => "blank",
                "user_id" => $convo->user_id,
                "receiver_id" => $convo->receiver_id,
                "unread" => true,
                "userTyping" => $convo->userTyping,
                "receiverTyping" => $convo->receiverTyping
            ];

        return json_encode($convoVars);
    }

    public function editConversationTyping(Request $request){
        $response = json_decode($request->getContent());
        $profile = Users::profile($response->user_id);
        $author = true;

        $conversation = Conversations::find($response->conversation_id);

        if($response->user_id == $conversation->receiver_id){
            $author = false;
        }

        if($author){
            $conversation->update([
                "userTyping" => $response->status
            ]);
        } else {
            $conversation->update([
                "receiverTyping" => $response->status
            ]);
        }
    }

    public function browseMessages($user_id, $conversation_id){
        $messages = Messages::where('conversation_id', $conversation_id)->orderBy('startedAt', 'DESC')->get();
        $chats = [];

        foreach($messages as $message){

            $direction = 0; // 0 = inbound

            if($message->user_id == $user_id){
                $direction = 1; // 1 = outbound
            }

            if(isset($message->subject)){
                if($message->user_id == $user_id){ $direction = 0; } else {  $direction = 1; }
            }

            $chatVars = [
                "id"  => $message->id,
                "user_id"  => $message->user_id,
                "receiver_id"  => $message->receiver_id,
                "like_id"  => $message->like_id,
                "subject"  => $message->subject,
                "subheader"  => $message->subheader,
                "message"  => $message->message,
                "startedAt"  => $message->startedAt,
                "songURL"  => $message->songURL,
                "media"  => $message->media,
                "viewed"  => $message->viewed,
                "notified"  => $message->notified,
                "direction" => $direction
            ];

            if($message->user_id == $user_id || $message->receiver_id == $user_id){
                // for safe measure, only return if the user is actually part of the conversation
                array_push($chats, $chatVars);

                // mark the content as read

                if($direction == 0){
                    $message->update(
                        [
                            "viewed" =>  true,
                            "notified" => true
                        ]
                    );
                }
            }
        }

        return json_encode($chats);
    }

    public function readProfile($uid, $user_id){
        $profile = Users::profile($uid);
        $age = 0;
        $profile->makeHidden(['created_at','updated_at']);
        $quizResults = Results::where('user_id', $profile->user_id)->count();

        if(!is_null($profile->birthday)){
            $age = getAge($profile->birthday);
        }

        $profile = collect($profile)->merge(["quizResultCount" => $quizResults, "age" => $age]);

        // matched with user?

        if(Messages::verifyConversationAPP($user_id, $uid, "verify") === false){
            // matched
            $profileResult = collect($profile)->merge(['matched' => 0]);    
        } else {
            $profileResult = collect($profile)->merge(['matched' => 1]);    
        }

        return json_encode($profileResult);
    }

    public function readPhotos($uid){
        $photos = Users::profile($uid)->photos();
        $photos->makeHidden(['created_at','updated_at', 'deleted_at']);
        return json_encode($photos);
    }

    public function readPosts($uid, $user_id){

        // get all the posts of the user that I'm looking at

        $posts = Posts::where('user_id', $user_id)->orderBy('created_at', 'DESC')->get();
        $cards = [];
        // organize this post to app object

        foreach($posts as $key => $post){
            $userID = $post["user_id"]; 
            
            $owner = 0;

            if($userID == $uid){
                $owner = 1;
            }

                $profile = Users::profile($userID);
                $avatar = $profile["avatar"];
                $likes = $post->likes->count();
                $liked = $post->likes->where('user_id', $uid)->count();


                $postVars = [
                    'id' => $post->id,
                    'owner' => $owner,
                    'likes' => $likes,
                    'author_id' => $userID,
                    'displayName' => $profile["displayName"],
                    'avatar' => $avatar,
                    'type' => $post["type"],
                    'icebreaker' => "",
                    'answer' => "",
                    'mediaLink' => "",
                    'songURL' => "",
                    'liked' => $liked,
                    'humor' => "none"
                ];

                if($post["type"] < 3){
                    $postVars["icebreaker"] = $post["icebreaker"];
                    $postVars["answer"] = $post["answer"];
                }
                if($post["type"] == 3){
                    $postVars["mediaLink"] = $post["mediaLink"];
                }
                if($post["type"] == 4){
                    $postVars["songURL"] = str_replace("/", "***", $post["songURL"]);
                }
                if($post["type"] < 4){
                    // no song support for now
                    array_push($cards, $postVars);
                }
        }
        return json_encode($cards);
    }

    public function addMessage(Request $request){

        $request = json_decode($request->getContent());
        
        $profile = Users::profile($request->user_id);
        $date = getRealTime($profile["timezone"]);

        $conversation = Conversations::where('id',$request->conversation_id)->first();

        $them = $conversation->receiver_id;

        if($request->user_id == $them){
            $them = $conversation->user_id;
        }

        if(!empty($request->message)){

            Messages::create([
                'user_id' => $request->user_id,
                'receiver_id' => $them,
                'message' => $request->message,
                'conversation_id' => $request->conversation_id,
                'viewed' => false,
                'notified' => false,
                'startedAt' => $date
            ]);

            // update the conversation details

            $conversation->update([
                "lastMessage" => $request->message,
                "startedAt" => $date
            ]);

            // send push notification

            $theirProfile = Profiles::where('user_id', $them)->first();

            if(!is_null($theirProfile->fcm_token)){
                $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
                $auth = $init->createAuth();
                $messaging = $init->createMessaging();

                $message = CloudMessage::withTarget('token', $theirProfile->fcm_token);

                $config = ApnsConfig::fromArray([
                    'headers' => [
                        'apns-priority' => '10',
                    ],
                    'payload' => [
                        'aps' => [
                            'sound' => 'default',
                            'alert' => [
                                'title' => "$profile->displayName",
                                'body' => $request->message,
                            ],
                            'badge' => 0,
                        ],
                    ],
                ]);

                $message = $message->withApnsConfig($config);

               
                $messaging->send($message);
            }

        }
    }

    public function addMediaMessage(Request $request){

        $request = json_decode($request->getContent());
        
        $profile = Users::profile($request->user_id);
        $date = getRealTime($profile["timezone"]);

        $conversation = Conversations::where('id',$request->conversation_id)->first();

        $them = $conversation->receiver_id;

        if($request->user_id == $them){
            $them = $conversation->user_id;
        }

        $message = $request->message;

        Messages::create([
            'user_id' => $request->user_id,
            'receiver_id' => $them,
            'message' => $message,
            'conversation_id' => $request->conversation_id,
            'media' => $request->mediaLink,
            'viewed' => false,
            'notified' => false,
            'startedAt' => $date
        ]);

        if(empty($message) || $message == NULL || $message == ""){
            $txtmessage = "Sent a Photo";
        }

        // update the conversation details

        $conversation->update([
            "lastMessage" => $txtmessage,
            "startedAt" => $date
        ]);

        $theirProfile = Profiles::where('user_id', $them)->first();

            if(!is_null($theirProfile->fcm_token)){
                $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
                $auth = $init->createAuth();
                $messaging = $init->createMessaging();

                $message = CloudMessage::withTarget('token', $theirProfile->fcm_token);

                $config = ApnsConfig::fromArray([
                    'headers' => [
                        'apns-priority' => '10',
                    ],
                    'payload' => [
                        'aps' => [
                            'sound' => 'default',
                            'alert' => [
                                'title' => "$profile->displayName",
                                'body' => $txtmessage,
                            ],
                            'badge' => 0,
                        ],
                    ],
                ]);

                $message = $message->withApnsConfig($config);

               
                $messaging->send($message);
            }
    }

    

    public function editProfile(Request $request){
        $request = json_decode($request->getContent());
        
        $profile = Users::profile($request->user_id);

        $birthday = str_replace("Optional(", "", $request->birthday);
        $birthday = str_replace(")", "", $birthday);
        $birthday = str_replace('"', "", $birthday);
        $birthday = substr($birthday, 0, 10);

        $profile->update([
            "bio" => $request->bio,
            "school" => $request->school,
            "jobTitle" => $request->jobTitle,
            "birthday" => $birthday,
            "displayName" => $request->displayName,
            "gender" => $request->gender,
            "lookingFor" => $request->lookingFor,
            "showMe" => $request->showMe,
            "onboarded" => 1
        ]);
    }

    public function saveToken(Request $request){
        $request = json_decode($request->getContent());
        
        $profile = Users::profile($request->user_id);

        $profile->update([
            "fcm_token" => $request->token
        ]);
    }

    public function addLike(Request $request){
        $request = json_decode($request->getContent());
        $profile = Users::profile($request->user_id);
        $date = getRealTime($profile["timezone"]);

        $post = Posts::find($request->post_id);

        $liked = Likes::where('user_id', $profile->user_id)->where('post_id', $post->id)->first();

        if(!isset($liked)){
            $liked = Likes::create([
                'user_id' => $profile->user_id,
                'post_id' => $post->id
            ]);
        }

        if(!empty($request->message) && $request->message != "Type something..."){
            if(Messages::verifyConversationAPP($profile->user_id, $post->user_id, "verify") === false){
                // create a new conversation
                $convo = Conversations::create([
                        'user_id' => $profile->user_id,
                        'receiver_id' => $post->user_id,
                        'lastMessage' => $request->message,
                        'startedAt' => $date
                    ])->id;

                
            } else{
                // grabs conversation ID and adds new message

                $convo = Messages::getConvoIDAPP($profile->user_id, $post->user_id, "read");
            }

            if(!empty($post["icebreaker"])){
                Messages::create([

                        'user_id' => $profile->user_id,
                        'receiver_id' => $post->user_id,
                        'subject' => $post["icebreaker"],
                        'like_id' => $liked->id,
                        'subheader' => $post["answer"],
                        'message' => $request->message,
                        'conversation_id' => $convo,
                        'startedAt' => $date
                ]);
            } 
            if(!empty($post["mediaLink"])){
                Messages::create([
                        'user_id' => $profile->user_id,
                        'receiver_id' => $post->user_id,
                        'like_id' => $liked->id,
                        'message' => $request->message,
                        'media' => $post->mediaLink,
                        'conversation_id' => $convo,
                        'startedAt' => $date
                ]);
            }

            $conversation = Conversations::find($convo);

            $conversation->update([
                "lastMessage" => $request->message,
                "startedAt" => $date
            ]);

            $theirProfile = Profiles::where('user_id', $post->user_id)->first();

            if(!is_null($theirProfile->fcm_token)){
                $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
                $auth = $init->createAuth();
                $messaging = $init->createMessaging();

                $message = CloudMessage::withTarget('token', $theirProfile->fcm_token);

                $config = ApnsConfig::fromArray([
                    'headers' => [
                        'apns-priority' => '10',
                    ],
                    'payload' => [
                        'aps' => [
                            'sound' => 'default',
                            'alert' => [
                                'title' => "$profile->displayName liked your post!",
                                'body' => $request->message,
                            ],
                            'badge' => 0,
                        ],
                    ],
                ]);

                $message = $message->withApnsConfig($config);

               
                $messaging->send($message);
            }
        }

    }

    public function addIcebreaker(Request $request){
        $response = json_decode($request->getContent());
        $profile = Users::profile($response->user_id);
        
        $date = getRealTime($profile["timezone"]);

        if(isset($response->answer)){

            $validator = Validator::make($request->all(), [
                'answer' => 'required|max:255'
            ]);

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $icebreaker = Icebreakers::find($response->icebreaker_id);
            
            Posts::create([
                "user_id" => $response->user_id,
                "type" => 1,
                "icebreaker" => $icebreaker->prompt,
                "answer" => $response->answer
            ]);
        }
    }

    public function addMedia(Request $request){
        $response = json_decode($request->getContent());
        $profile = Users::profile($response->user_id);

        Posts::create([
            "user_id" => $profile->user_id,
            "type" => 3,
            "mediaLink" => $response->mediaLink
        ]);
    }

    public function addPhoto(Request $request){
        $response = json_decode($request->getContent());
        $profile = Users::profile($response->user_id);
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $storage = $init->createStorage();
        $defaultBucket = $storage->getBucket();

        // intercept and crop

        $filePath = app(\App\Http\Controllers\PostController::class)->shrinkImageAPP($response->mediaLink, true);

        $uploadResult = $defaultBucket->upload(              
            file_get_contents($filePath), [
                'name' => rand(999,9999999).time().".jpg",
                'uploadType'=> "media",
                'predefinedAcl' => 'publicRead'
            ]
        );

        Photos::create([
            "user_id" => $profile->user_id,
            "mediaLink" => $uploadResult->info()["mediaLink"],
            "sequence" => 2
        ]);

        if(Photos::where('user_id', $profile->user_id)->count() <= 1){
            $profile->update([
                "avatar" =>  $uploadResult->info()["mediaLink"]
            ]);
        }
    }

    public function editPhotoSequence(Request $request){
        $response = json_decode($request->getContent());
        $profile = Users::profile($response->user_id);

        $photo = Photos::where('user_id', $profile->user_id)->where('id', $response->photo_id)->first();

        $photo->update([
            "sequence" => $response->sequence
        ]);

        if($response->sequence == 0){
            $profile->update([
                "avatar" => $photo->mediaLink
            ]);
        }
    }

    public function deletePhoto(Request $request){
        $response = json_decode($request->getContent());
        $profile = Users::profile($response->user_id);

        $photo = Photos::where('user_id', $profile->user_id)->where('id', $response->photo_id)->first();

        $photosCount = Photos::where('user_id', $profile->user_id)->count();
        if($photosCount > 1){
            $photo->delete();
        }
        return "error";
    }

    public function addProfile(Request $request){
        $response = json_decode($request->getContent());
        Profiles::create([
            "user_id" => $response->user_id,
            "displayName" => "humor",
            "avatar" => "https://humorapp.co/images/logo.png"
        ]);
    }

    public function deletePost(Request $request){
        $response = json_decode($request->getContent());

        $post = Posts::where('id', $response->post_id)->where('user_id', $response->user_id)->first();

        $post->delete();
    }

    public function reportPost(Request $request){
        $response = json_decode($request->getContent());

        $post = Posts::where('id', $response->post_id)->first();

        Reports::create([
            "post_id" => $post->id,
            "user_id" => $response->user_id,
            "violator_id" => $post->user_id,
            "reason" => "post"
        ]);

    }

    public function reportProfile(Request $request){
        $response = json_decode($request->getContent());

        Reports::create([
            "user_id" => $response->user_id,
            "violator_id" => $response->violator_id,
            "reason" => $response->reason
        ]);

        $reason = "reported user for: " . $response->reason;

        $this->unmatchProfile($response->user_id, $response->violator_id, $reason);

    }

    public function unmatchProfile($user_id = NULL, $profile_id = NULL, $reason = "", Request $request){
        $response = json_decode($request->getContent());

        if(empty($reason)){
            $reason = $response->reason;
        }
        if(empty($user_id)){
            $user_id = $response->user_id;
            $profile_id = $response->profile_id;
        }

        $convo = Messages::getConvoIDAPP($user_id, $profile_id, "read");



        if(isset($convo)){
            $convo = Conversations::find($convo);
            $convo->delete();
            // delete the conversation

            Unmatches::create([
                'user_id' => $profile_id,
                'receiver_id' => $user_id,
                'conversation_id' => $convo->id,
                'reason' => $reason
            ]);
        }
    }

    public function getSong($width, $song){

        $song = str_replace("***", "/", $song);
        $width = (int) $width - 80; // device width minus padding

        echo '<!DOCTYPE html>
                <html lang="en">
                    <head>
                        <meta charset="utf-8" />
                        <title>humor</title>
                        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
                    </head><body><iframe src="' . $song . '" height="280" frameborder="0" allowtransparency="true" allow="encrypted-media" style="width:' . $width . 'px;"></iframe></body></html>';
    }



    public function scoreCard(Request $request){
        $response = json_decode($request->getContent());
        $profile = Users::profile($response->user_id);

        Results::create([
            "user_id" => $profile->user_id,
            "meme_id" => $response->meme_id,
            "score" => $response->score
        ]);

    }

    public function getQuizCards($user_id){
        $profile = Users::profile($user_id);
        $results = $profile->results;
        $cards = [];
        
        $memes = DB::table('Meme')                 
            ->select('id','file','prompt')
            ->limit(25)
            ->whereNotIn('id', DB::table('results')->where('user_id', $profile->user_id)->pluck('meme_id')->toArray())
            ->where('active',1)
            ->inRandomOrder()
            ->get()
            ->toArray();

        for($i = 1; $i < sizeof($memes); $i++){
            $meme = collect($memes[$i])->merge(["position" => $i])->toArray();
            array_push($cards, $meme);
        }

        return json_encode($cards);
    }

    public function getHumor($user_id){
        $profile = Users::profile($user_id);

        $results = $profile->results;

        $sexual = $dark = $witty = $weird = $physical = $contextual = 0;

        foreach($results as $result){
            
            $meme = Memes::find($result->meme_id);

            switch ($meme->type) {
                case 'dark':
                    $dark += $result->score;
                    break;
                case 'witty':
                    $witty += $result->score;
                    break;
                case 'weird':
                    $weird += $result->score;
                    break;
                case 'physical':
                    $physical += $result->score;
                    break;
                case 'contextual':
                    $contextual += $result->score;
                    break;
                case 'sexual':
                    $sexual += $result->score;
                    break;
                default:
                    break;
            }
        }

        $results = ["Dark" => $dark, "Witty" => $witty, "Weird" => $weird, "Physical" => $physical, "Contextual" => $contextual, "Sexual" => $sexual];

        arsort($results);

        $keys = array_keys($results);

        $finalResult = [
            "first" => ["category" => $keys[0], "score" => $results[$keys[0]]],
            "second" => ["category" => $keys[1], "score" => $results[$keys[1]]],
            "third" => ["category" => $keys[2], "score" => $results[$keys[2]]],
            "fourth" => ["category" => $keys[3], "score" => $results[$keys[3]]],
            "fifth" => ["category" => $keys[4], "score" => $results[$keys[4]]],
            "sixth" => ["category" => $keys[5], "score" => $results[$keys[5]]]
        ];
        
        return json_encode($finalResult);
    }

    public function compareHumor($me, $them, $callback = "strength"){
        $myHumor = json_decode($this->getHumor($me), true);
        $theirHumor = json_decode($this->getHumor($them), true);

        $match = "none";

        if($myHumor["first"]["category"] == $theirHumor["first"]["category"] || $myHumor["second"]["category"] == $theirHumor["second"]["category"]){
            $match = "strong";
        }

        if($myHumor["first"]["category"] == $theirHumor["first"]["category"] && $myHumor["second"]["category"] == $theirHumor["second"]["category"]){
            $match = "exact";
        }

        if($callback == "strength"){
            return $match;
        } else {
            return json_encode(["me" => $myHumor, "them" => $theirHumor]);
        }
    }

    public function addVerification(Request $request){
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        $response = json_decode($request->getContent());
        $profile = Users::profile($response->user_id);

        try{
            $user = $auth->getUser($response->user_id);
        }
        catch(Exception $e){
            // abandon app. user not authorized

            return "error";
        }

        if($profile->emailVerified == 0){

            $code = rand(1000,9999);

            $profile->update([
                "email_code" => $code
            ]);

            Mail::send(['html' => 'emails.verification'], ["token" => $code], function($message) use ($user){
                
                $message->to($user->email, $user->displayName)
                ->subject("Your email verification code is...");
                    $message->from("hello@humorapp.co",'humor');
                    $message->sender("hello@humorapp.co",'humor');
            });
        }
        
    }

    public function editVerification(Request $request){
        $response = json_decode($request->getContent());
        $profile = Users::profile($response->user_id);

        if($response->code == $profile->email_code){
            $profile->update([
                "email_code" => NULL,
                "emailVerified" => 1
            ]);
        }
    }
}

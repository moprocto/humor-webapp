<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use \CloudConvert\Laravel\Facades\CloudConvert;
use \CloudConvert\Models\Job;
use \CloudConvert\Models\Task;



class StorageController extends Controller
{
    public function upload($file, $bucket){
    	/*
    	$uploadResult = Storage::disk($bucket)->put(
            'videos',
            $file->getRealPath()
        );
        */

    	// $uploadResult = $file->store('images', $bucket);

        $job = (new Job())
    ->addTask(new Task('import/upload','upload-my-file'))
    ->addTask(
        (new Task('convert', 'convert-my-file'))
            ->set('input', 'import-my-file')
            ->set('output_format', 'gif')
    )
    ->addTask(
        (new Task('export/url', 'export-my-file'))
            ->set('input', 'convert-my-file')
    );

    $cloudconvert->jobs()->create($job);

$uploadTask = $job->getTasks()->name('upload-my-file')[0];

$inputStream = fopen($file, 'r');

CloudConvert::tasks()->upload($uploadTask, $inputStream);

foreach ($job->getExportUrls() as $file) {

    $source = $cloudconvert->getHttpTransport()->download($file->url)->detach();
    $dest = fopen(Storage::path('out/' . $file->filename), 'w');
    
    stream_copy_to_stream($source, $dest);

}

        return $source;
    }
}

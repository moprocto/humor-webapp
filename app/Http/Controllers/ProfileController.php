<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Storage;
use App\User as Users;
use App\Profile as Profiles;
use App\Photo as Photos;
use App\Post as Posts;
use App\Icebreaker as Icebreakers;
use App\Conversation as Conversations;
use App\Unmatch as Unmatches;
use Carbon;
use Session;
use Redirect;
use View;

class ProfileController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('firebase');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function addOnboarding(Request $request){
    	$user = Session::get('user');
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        $database = $init->createDatabase();
        $profile = Users::profile($user->uid);

		$request->merge(['user_id' => $user->uid, 'onboarded' => true]);

		$profile->update($request->all());

		// upload user photos

		$storage = $init->createStorage();
		$defaultBucket = $storage->getBucket();

		if($request->file("photos") !== null){
			for($i = 0; $i < 6; $i++){
				$file = $request->file("photos");
				
				if(array_key_exists($i, $file) && $file[$i] !== null){
					$filename = rand(999,9999999) . "-" . cleanFileName($file[$i]->getClientOriginalName());
					$extension = strtolower($request->file("photos")[$i]->getClientOriginalExtension());

					$filePath = app(\App\Http\Controllers\PostController::class)->shrinkImage($request->file("photos")[$i], true);

					$uploadResult = $defaultBucket->upload(
						
				        file_get_contents($filePath), [
				            'name' => $filename,
				            'uploadType'=> "media",
				            'predefinedAcl' => 'publicRead',
				            "metadata" => ["contentType"=> 'image/' . $extension],
				        ]
				    );

                    $uploadedPhoto = Photos::create([
                        "user_id" => $user->uid,
                        "mediaLink" => $uploadResult->info()["mediaLink"],
                        "sequence" => $i
                    ]);

				   if($i == 0){
						// finally, we need to make the first photo the profile avatar
						$profile->update([
							"avatar" => $uploadResult->info()["mediaLink"]
						]);
					}

                    unlink($filePath); // delete the thumbnail
				}
			}
		}

		// add a post if the icebreaker was answered

		if(isset($request->answer) && !empty($request->answer)){

			$icebreaker = Icebreakers::find($request->icebreaker_id);

            Posts::create([
                'user_id' => $user->uid,
                'type' => 2, // icebreaker onboarded
                'icebreaker' => $icebreaker["prompt"],
                'answer' => stringClean($request->answer)
            ]);
		}

		Session::flash("success", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "Welcome to humor! Post something to get the conversation going"
                ]);

		return Redirect::to('/home');
    }

    public function readProfile(){
    	$user = Session::get('user');
        
    	$profile = Users::profile($user->uid);
    	$photos = Users::photos($user->uid);


    	$data = [
    		"user" => $user,
    		"profile" => $profile,
    		"photos" => $photos,
    		"photoIDs" => NULL,
    		"page" => "profile"
    	]; 	

    	return view('users.index', $data);
    }

    public function editProfile(Request $request){
    	$user = Session::get('user');
        $profile = Users::profile($user->uid);
        $request->merge(['user_id' => $user->uid, 'onboarded' => true]);
		$profile->update($request->all());

		Session::flash("success", [
                "alert" => "success",
                "header" => "Success",
                "body" => "Your profile has been updated!"
            ]);

		return Redirect::back();

    }

    public function profileCard(Request $request){
        $profile = Users::profile($request->user);
        $photos = Users::photos($request->user);

        $data = [
            "user" => Session::get('user'),
            "profile" => $profile,
            "photos" => $photos,
            "convo" => $request->convo,
            "details" => "",
            "jobTitle" => "",
            "lookingFor" => "",
            "bio" => ""
        ];
        
        $html = (string)View::make('users.card',  $data)->render();

        return $html;
        
    }

    public function readAvatar(Request $request){
    	$init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        $database = $init->createDatabase();

        return Users::avatar($database, $request->user);
    }

    public function uploadPhoto(Request $request){
    	$user = Session::get('user');
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        $database = $init->createDatabase();
    	$storage = $init->createStorage();
		$defaultBucket = $storage->getBucket();
		$profile = Users::profile($user->uid);
		$date = getRealTime($profile["timezone"]);

    	$file = $request->file("image");

    	$image_parts = explode(";base64,", $request->image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);


        $uploadResult = $defaultBucket->upload(
				
	        $image_base64, [
	            'name' => generateRandomString(20) . ".jpg",
	            'uploadType'=> "media",
	            'predefinedAcl' => 'publicRead',
	            "metadata" => ["contentType"=> 'image/jpg'],
	        ]
	    );

		$mediaLink = $uploadResult->info()["mediaLink"];

        $uploadedPhoto = Photos::create([
                        "user_id" => $user->uid,
                        "mediaLink" => $uploadResult->info()["mediaLink"],
                        "sequence" => 2
                    ]);

		if($request->toDiscover == 1){
			// the user also wants to upload to the discover page
			Posts::create(['user_id' => $user->uid,'type' => 3,'mediaLink' => $mediaLink]);
		}


		return json_encode(["link" => $mediaLink, "id" => $uploadedPhoto->id]);

    }

    public function sequencePhoto(Request $request){
    	$user = Session::get('user');
        $photo = Photos::where('id', $request->photo)->where('user_id', $user->uid)->first();
        $photo->update(["sequence" => $request->sequence]);

		if($request->sequence == 0){
			$profile = Users::profile($user->uid);
			$profile->update([
				"avatar" => $photo["mediaLink"]
			]);
		}
    } 

    public function deletePhoto(Request $request){
    	$user = Session::get('user');
        $photo = Photos::where('id', $request->photo)->where('user_id', $user->uid)->first();
        $photosCount = Photos::where('user_id', $user->uid)->count();
        if($photosCount > 1){
            $photo->delete();
        }
        return "error";
    } 

}

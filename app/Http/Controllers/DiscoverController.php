<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use App\User as Users;
use App\Message as Messages;
use App\Post as Posts;
use App\Profile as Profiles;
use App\Unmatch as Unmatches;
use Carbon;
use Session;
use Redirect;
use View;
use Storage;
use DB;

class DiscoverController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('firebase');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function browse(){

        $user = Session::get('user');
        $profile = Users::profile($user->uid);

        if(!$profile->onboarded){
            $memes = json_decode(app(\App\Http\Controllers\LabController::class)->getMeme());
            return view('onboarding', ["user" => $user, "profile" => $profile, "memes" => $memes]);
        }

        $data = [
            "page" => "discover",
            "profile" => $profile,
            "user" => $user
        ];

        return view('discover', $data);
    }

    public function browseFeed($source = NULL){
        $user = Session::get('user');
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        $database = $init->createDatabase();
        $cards = array();
        $unmatched = false;
        
        $posts = Posts::orderBy('created_at', 'DESC')->get();

        foreach($posts as $key => $post){
            $userID = $post["user_id"]; 

            if($userID != $user->uid){
                $unmatched = Unmatches::verify($user->uid, $userID);
            }

            if(!$unmatched){
                
                $profile = Users::profile($userID);
                $avatar = $profile["avatar"];

                // get like info

                $likeButton = $this->readLike($post, $avatar);

                $postVars = [
                    'key' => $post->id,
                    'userID' => $userID,
                    'displayName' => $profile["displayName"],
                    'avatar' => $avatar,
                    'user' => $user,
                    'post' => $post,
                    'likeButton' => $likeButton
                ];

                if($post["type"] < 3){
                    $view = (string)View::make('layouts.posts.discover-text',  $postVars)->render();
                }
                if($post["type"] == 3){
                    $view = (string)View::make('layouts.posts.discover-media',  $postVars)->render();
                }
                if($post["type"] == 4){
                    $view = (string)View::make('layouts.posts.discover-spotify',  $postVars)->render();
                }
                if($source == "ios"){
                    $cards[$key] = $postVars;
                } else{
                    $cards[$key] = $view;
                }
                
            }
        }
        return json_encode($cards);

    }

    public function readLike($post, $avatar){
        $liked = false;
        $user = Session::get('user');

        $likeClass = "mdi-heart-outline";
        $color = "text-danger";
        $sendLike = "send-like";
        $isLiked = $icebreakerAnswer = $media = "";

        if($post["user_id"] == $user->uid){
            $likeClass = "mdi-heart";
            $color = $sendLike = "text-dark";
        }
    
        $likes = $post->likes;

        foreach ($likes as $like) {
            if($like["user_id"] == $user->uid){
                $liked = true;
                $likeClass = "mdi-heart";
                $isLiked = "liked";
                break;
            }
        }

        $likeCount = $likes->count();
        if($post["type"] < 3){
            $icebreakerAnswer = "<span style='font-size:16px; padding:10px; font-weight:bold;'>" . $post["icebreaker"] . "...</span></h2><h2>\""; 

            $icebreakerAnswer .= 'data-answer="' . $post["answer"] . '"';
        }
        elseif($post["type"] == 3) {

            $media = $post["mediaLink"];
        }
        elseif($post["type"] == 4) {

            $media = $post["songURL"];
        }

        $likeButton = '<a href="javascript:void(0);" class="btn btn-link ' . $sendLike . ' p-0 waves-effect waves-light ' . $isLiked . '" data-post="'. $post["id"] . '" data-type="' . $post["type"] . '" data-avatar="' . $avatar . '" data-title="' . $icebreakerAnswer . '" data-media="' . $media . '" data-user="' . $post["user_id"] . '"><i class="mdi ' . $likeClass . ' likeCount ' . $color . '" data-post="" style="font-size: 24px;"> </i></a> <span class="likeCounter">' . $likeCount . '</span>';

        return $likeButton;

    }
}



<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Storage as FBStorage;
use App\User as Users;
use App\Profile as Profiles;
use App\Conversation as Conversations;
use App\Unmatch as Unmatches;
use App\Message as Messages;
use App\Post as Posts;
use App\Like as Likes;
use App\Icebreaker as Icebreakers;
use Carbon;
use Session;
use Redirect;
use DateTime;
use Collection;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification as FCM_Notification;
use Kreait\Firebase\Messaging\ApnsConfig as ApnsConfig;

class PostController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

	public function addPost(Request $request){
		$user = Session::get('user');
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
		$profile = Users::profile($user->uid);
		
        $date = getRealTime($profile["timezone"]);

        $request->merge(['user_id' => $user->uid]);

		if(isset($request->icebreaker)){

			$validator = Validator::make($request->all(), [
	            'answer' => 'required|max:255'
	        ]);

	    	if ($validator->fails()) {
	            return Redirect::back()->withErrors($validator)->withInput();
	        }

			$icebreaker = Icebreakers::find($request->icebreaker_id);
			$request->merge(['type' => 1, 'icebreaker' => $icebreaker->prompt]);
			Posts::create($request->all());
		}

		if(isset($request->postmedia)) {

			$validator = Validator::make($request->all(), [
	            'photo' => 'required|mimes:jpeg,jpg,png,gif'
	        ]);

	    	if ($validator->fails()) {
	            return Redirect::back()->withErrors($validator)->withInput();
	        }

			if($request->file("photo") != null){
				$type = 3;

				// upload user photos

				$file = $request->file("photo");
				
				$filename = rand(999,9999999) . "-" . cleanFileName($file->getClientOriginalName());
				$extension = strtolower($file->getClientOriginalExtension());

				if($extension == "mp4" || $extension == "MP4"){
					$type = 5;
				}

				// clear all images

				if($type == 3){
					$file = $this->shrinkImage($file, false);
					$storage = $init->createStorage();
					$defaultBucket = $storage->getBucket();
					$uploadResult = $defaultBucket->upload(
				        file_get_contents($file), [
				            'name' => $filename,
				            'uploadType'=> "media",
				            'predefinedAcl' => 'publicRead',
				            "metadata" => ["contentType"=> 'image/' . $extension],
				        ]
			    	);

			    	$mediaLink = $uploadResult->info()["mediaLink"];

			    	unlink($file);

				}
				elseif($type == 4){
			    	
			    	$uploadResult = $file->store('images', "s3");

			    	$mediaLink = env("AWS_ACCESS_URL") . $uploadResult;
				}

				// we need to replace the photo url in sequence with the new image

				$request->merge(['mediaLink' => $mediaLink, 'type' => $type]);

				Posts::create($request->all());
			}

		}

		if(isset($request->postsong)){
			$validator = Validator::make($request->all(), [
	            'song' => 'required|max:255'
	        ]);
			/*

				desktop: https://open.spotify.com/embed/track/5FEXPoPnzueFJQCPRIrC3c
				mobile: https://open.spotify.com/track/5FEXPoPnzueFJQCPRIrC3c?si=ZeqwhSK5QcOHUJqtXb6TpA
			*/
	        //
	        // the share format from the spotify app is weird

	        $song = $request->song;
	        $song = str_replace("https://open.spotify.com/track/", "https://open.spotify.com/embed/track/", $song);

	        
	        $contains = $this->verifySong($request, $song);


	    	if ($validator->fails() || !$contains) {
	            return Redirect::back()->withErrors($validator)->withInput();
	        }

	        $request->merge(['songURL' => $song, 'type' => 4]);
			
			Posts::create($request->all());
		}
		
		return Redirect::back();
	}

	public function getIcebreaker($current, $request = "html"){

        $option = Icebreakers::inRandomOrder()->where('id', '!=', $current)->limit(1)->first();

        if($request == "html"){
        	return '
        <div class="collapse pt-3 show">
           		<h5 class="card-title mb-1">' . $option->prompt . '</h5>
              <input type="hidden" name="icebreaker_id" id="icebreaker_id" value="' . $option->id . '">
             <textarea rows="3" class="form-control" name="answer" style="background: transparent; border:none; min-height: 40px;" placeholder="Your Answer..." spellcheck="false"></textarea>
        </div>';
        }

        return json_encode($option);
        

	}

	public function addLike(Request $request){
		
		$user = Session::get('user');
        $profile = Users::profile($user->uid);
        
        $date = getRealTime($profile["timezone"]);

        $liked = Likes::where('user_id', $profile->user_id)->where('post_id', $request->post)->first();

        if(!isset($liked)){
            $liked = Likes::create([
                'user_id' => $profile->user_id,
                'post_id' => $post->id
            ]);
        }

        if(!empty($request->message)){

        	// check to see if a conversation with this person exists

        	if(Messages::verifyConversation($request->user, "verify") === false){
        		// create a new conversation
        		$convo = Conversations::create([
		        		'user_id' => $user->uid,
		        		'receiver_id' => $request->user,
		        		'lastMessage' => $request->message,
		        		'startedAt' => $date
		        	])->id;

        		
        	} else{
        		// grabs conversation ID and adds new message

        		$convo = Messages::getConvoID($request->user, "read");
        	}

        	$post = Posts::find($request->post);

        	if(!empty($post["icebreaker"])){
        		Messages::create([
		        		'user_id' => $user->uid,
		        		'receiver_id' => $request->user,
		        		'subject' => $post["icebreaker"],
		        		'like_id' => $liked->id,
		        		'subheader' => $post["answer"],
		        		'message' => $request->message,
		        		'media' => $request->media,
		        		'conversation_id' => $convo,
		        		'startedAt' => $date,
		        		'type' => $request->type
		        ]);
        	} 
        	if(!empty($request->media)){
        		Messages::create([
		        		'user_id' => $user->uid,
		        		'receiver_id' => $request->user,
		        		'like_id' => $liked->id,
		        		'message' => $request->message,
		        		'media' => $request->media,
		        		'conversation_id' => $convo,
		        		'startedAt' => $date,
		        		'type' => $request->type
		        ]);
        	}
        	if(!empty($request->songURL)){
        		Messages::create([
		        		'user_id' => $user->uid,
		        		'receiver_id' => $request->user,
		        		'like_id' => $liked->id,
		        		'message' => $request->message,
		        		'songURL' => $request->songURL,
		        		'conversation_id' => $convo,
		        		'startedAt' => $date,
		        		'type' => $request->type
		        ]);
        	}

        	// update conversation

        	$conversation = Conversations::find($convo);

            $conversation->update([
                "lastMessage" => $request->message,
                "startedAt" => $date
            ]);

            $theirProfile = Profiles::where('user_id', $request->user)->first();

            if(!is_null($theirProfile->fcm_token)){
	            $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
	            $auth = $init->createAuth();
	            $messaging = $init->createMessaging();

	            $message = CloudMessage::withTarget('token', $theirProfile->fcm_token);

				$config = ApnsConfig::fromArray([
				    'headers' => [
				        'apns-priority' => '10',
				    ],
				    'payload' => [
				        'aps' => [
				        	'sound' => 'default',
				            'alert' => [
				                'title' => "$profile->displayName liked your post!",
				                'body' => $request->message,
				            ],
				            'badge' => 0,
				        ],
				    ],
				]);

				$message = $message->withApnsConfig($config);

	           
	            $messaging->send($message);
	        }
        }
	}

	public function deletePost(Request $request){
		$user = Session::get('user');
        $post = Posts::where('user_id', $user->uid)->where('id', $request->post)->first();
        $post->delete();
	}

	public function shrinkImage($OGimage, $crop = false){
		$image = Image::make($OGimage)->orientate();
		if($crop){
			$image->fit(520);
		} else{
			$image->resize(520, null, function ($constraint) {
			    $constraint->aspectRatio();
			});
		}
		

		$extension = $OGimage->getClientOriginalExtension();
	
		$thumbnailPath = public_path() .'/thumbnail/';
		$fileName = $thumbnailPath.time().$OGimage->getClientOriginalName();
		$image->save($fileName);
	
        return $fileName;
	}

	public function shrinkImageAPP($OGimage, $crop = false){
		$extension = pathinfo($OGimage)["extension"];

		$image = Image::make($OGimage)->orientate();
		if($crop){
			$image->fit(520);
		} else{
			$image->resize(520, null, function ($constraint) {
			    $constraint->aspectRatio();
			});
		}
	
		$thumbnailPath = public_path() .'/thumbnail/';
		$fileName = $thumbnailPath.time();
		$image->save($fileName);
	
        return $fileName;
	}

	public function verifySong(Request $request, $songURL = null){
		if(!is_null($request->songURL)){
			$songURL = $request->songURL;
		}

		if(!is_null($songURL)){
			// from another function
			$songURL = str_replace("https://open.spotify.com/track/", "https://open.spotify.com/embed/track/", $songURL);

	        $contains = Str::contains($songURL, 'https://open.spotify.com/embed/track/');
	        $validatorCustom = substr($songURL, 0, 37);

	        if($validatorCustom != "https://open.spotify.com/embed/track/"){
	        	$contains = false;
	        }
	        if(strlen($songURL) <= 58){
	        	$contains = false;
	        }

	        // the song matches the format needed for the iframe

			return $contains;
		}

		return false;
	}

    public function addUnmatch(Request $request){
        $user = Session::get('user');
        $conversation = Conversations::where('id', $request->convo)->where('user_id', $user->uid)->orWhere('receiver_id', $user->uid)->first();

        $conversation->delete();

        // delete the conversation

        Unmatches::create([
            'user_id' => $user->uid,
            'receiver_id' => $request->user,
            'conversation_id' => $request->convo,
            'reason' => $request->reason
        ]);
    }
}
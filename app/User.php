<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Session;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function read($uid){
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        return $auth->getUser($uid);
    }

    public static function posts($user_id){
         return \App\Post::where('user_id', $user_id)->get();
    }

    public static function profile($user_id){
        return \App\Profile::where('user_id', $user_id)->first();
    }

    public static function photos($user_id){
        return \App\Photo::where('user_id', $user_id)->orderBy('sequence', 'ASC')->get();
    }

    public static function matches($uid){
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        $database = $init->createDatabase();
        return $database->getReference('matches/' . $uid)->getValue();
    }

}

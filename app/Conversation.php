<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conversation extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'user_id', 'receiver_id','lastMessage', 'startedAt', 'userTyping', 'receiverTyping'
    ];
}

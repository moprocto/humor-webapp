<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use App\User as Users;
use App\Message as Messages;
use Carbon;
use Session;
use Redirect;
use View;
use Storage;

class FetchDiscover extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Discover:Fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will retrieve system posts and save into json for reading';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 

        $user = Session::get('user');
        $init = app(\App\Http\Controllers\FirebaseController::class)->auth();
        $auth = $init->createAuth();
        $database = $init->createDatabase();
        $cards = array();
        
        $posts = $database->getReference('posts')->orderByChild('startedAt')->getValue();

        $time = array_column($posts, 'startedAt');

        array_multisort($time, SORT_DESC, $posts);

        $posts = json_encode($posts);

        Storage::disk('discover')->put('discover.json', $posts);
        
        
    }
}

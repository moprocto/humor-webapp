<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use App\Message as Messages;
use App\Conversation as Conversations;

class Message extends Model
{
    protected $fillable = [
        'conversation_id','user_id','receiver_id','like_id','subject','subheader','message','startedAt','songURL','media','viewed','notified'
    ];

    public static function getConvoID($database, $them){
	    return Messages::verifyConversation($database, $them, "read");
	}

	public static function conversations($user_id){
		return  Conversations::where('user_id', $user_id)->orWhere('receiver_id', $user_id)->orderBy('startedAt', 'DESC')->get();
	}

	public static function mine(){
		$user = Session::get('user');
		return  Messages::where('user_id', $user->uid)->orWhere('receiver_id', $user->uid)->orderBy('startedAt', 'ASC')->get();
	}

	function like(){
    	return $this->hasOne('\App\Like');
    }


	public static function dialog($user_id){
		return  Messages::where('receiver_id', $user_id)->orderBy('startedAt', 'ASC')->get();
	}

	public static function verifyConversation($them, $request){
		$user = Session::get('user');
		$conversations = Messages::conversations($user->uid);

		if($conversations->isNotEmpty()){
			// there are conversations
			
			$convoExists = $conversations->where('user_id', $them); //->orWhere('receiver_id', $them)->count();
			if($convoExists->isNotEmpty()){
				if($request == "verify"){
					return true;
				}
				return $convoExists[0]->id;
			} else {
				$convoExists = $conversations->where('receiver_id', $them); 
				if($convoExists->isNotEmpty()){
					if($request == "verify"){
						return true;
					}
					return $convoExists[0]->id;
				}
			}
			
			return false;
			
		} 
		return false;
	}

	/* TEMPORARY */

	public static function verifyConversationAPP($user_id, $them, $request = "verify"){
		$conversations = Messages::conversations($user_id);

		if($conversations->isNotEmpty()){
			// there are conversations
			
			$convoExists = $conversations->where('user_id', $them); //->orWhere('receiver_id', $them)->count();
			

			if($convoExists->isNotEmpty()){
				if($request == "verify"){
					return true;
				}

				return $convoExists->first()->toArray()["id"];
			} else {
				$convoExists = $conversations->where('receiver_id', $them); 

				if($convoExists->isNotEmpty()){
					if($request == "verify"){
						return true;
					}
					$key = array_keys($convoExists->toArray())[0];
					return $convoExists->toArray()[$key]["id"];
				}
			}
			
			return false;
			
		} 
		return false;
	}

	public static function getConvoIDAPP($user_id, $them){
	    return Messages::verifyConversationAPP($user_id, $them, "read");
	}

	/* END TEMOPORARY */

	public static function read($database, $message_id){
		return $database->getReference("/messages/$message_id")->getValue();
	}

	public static function markAsRead($database, $message_id){
		$updates = ["messages/$message_id/read" => true];
        $database->getReference()->update($updates);

        $updates = ["messages/$message_id/notified" => true];
        $database->getReference()->update($updates);
	}

	public static function markAsNotified($database, $message_id){
        $updates = ["messages/$message_id/notified" => true];
        $database->getReference()->update($updates);
	}
}



